# VMware NSX-T

Vendor: VMware
Homepage: https://www.vmware.com/

Product: NSX-T
Product Page: https://www.vmware.com/products/cloud-infrastructure/nsx

## Introduction
We classify VMware NSX-T into the Data Center and Network Services domains as it provides a virtualization and security platform to simplify management and automation of network resources.

## Why Integrate
The VMware NSX-T adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware NSX-T. With this adapter you have the ability to perform operations on items such as:

- Fabric
- Policy
- Services

## Additional Product Documentation
[VMware NSX-T API Doc](https://developer.broadcom.com/xapis/nsx-t-data-center-rest-api/latest/)