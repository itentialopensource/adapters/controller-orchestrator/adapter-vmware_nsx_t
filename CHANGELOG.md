
## 0.9.4 [10-15-2024]

* Changes made at 2024.10.14_21:35PM

See merge request itentialopensource/adapters/adapter-vmware_nsx_t!24

---

## 0.9.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-vmware_nsx_t!22

---

## 0.9.2 [08-15-2024]

* Changes made at 2024.08.14_19:55PM

See merge request itentialopensource/adapters/adapter-vmware_nsx_t!21

---

## 0.9.1 [08-07-2024]

* Changes made at 2024.08.06_22:03PM

See merge request itentialopensource/adapters/adapter-vmware_nsx_t!20

---

## 0.9.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!19

---

## 0.8.3 [03-28-2024]

* Changes made at 2024.03.28_13:48PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!18

---

## 0.8.2 [03-11-2024]

* Changes made at 2024.03.11_16:02PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!17

---

## 0.8.1 [02-28-2024]

* Changes made at 2024.02.28_11:33AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!16

---

## 0.8.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!15

---

## 0.7.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!8

---

## 0.6.4 [07-13-2021]

- Changes to sampleProperties and healthcheck

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!7

---

## 0.6.3 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!6

---

## 0.6.2 [07-09-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!5

---

## 0.6.1 [01-16-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!4

---

## 0.6.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!3

---

## 0.5.0 [09-16-2019]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!2

---
## 0.4.0 [07-30-2019] & 0.3.0 [07-18-2019] & 0.2.0 [07-16-2019]

- Migrate, categorize and artifacts

See merge request itentialopensource/adapters/controller-orchestrator/adapter-vmware_nsx_t!1

---

## 0.1.1 [04-23-2019]

- Initial Commit

See commit 16da1e6

---
