## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for VMware NSX-T. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for VMware NSX-T.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the VMware NSX-T. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">putHpmfeaturesfeatureStackName(featurestackname, FeatureStackCollectionConfiguration, callback)</td>
    <td style="padding:15px">Update health performance monitoring configuration for feature stack</td>
    <td style="padding:15px">{base_path}/{version}/hpm/features/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHpmfeaturesfeatureStackName(featurestackname, callback)</td>
    <td style="padding:15px">Read health performance monitoring configuration for feature stack</td>
    <td style="padding:15px">{base_path}/{version}/hpm/features/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHpmfeaturesfeatureStackNameactionresetCollectionFrequency(featurestackname, action, callback)</td>
    <td style="padding:15px">Reset the data collection frequency configuration setting to the default values</td>
    <td style="padding:15px">{base_path}/{version}/hpm/features/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putHpmglobalConfig(GlobalCollectionConfiguration, callback)</td>
    <td style="padding:15px">Set the global configuration for aggregation service related data collection</td>
    <td style="padding:15px">{base_path}/{version}/hpm/global-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHpmglobalConfig(callback)</td>
    <td style="padding:15px">Read global health performance monitoring configuration</td>
    <td style="padding:15px">{base_path}/{version}/hpm/global-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHpmfeatures(callback)</td>
    <td style="padding:15px">List all health performance monitoring feature stacks</td>
    <td style="padding:15px">{base_path}/{version}/hpm/features?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitcheslswitchIdvtepTable(lswitchid, cursor, includedFields, pageSize, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get virtual tunnel endpoint table for logical switch of the given
ID (lswitch-id)
</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}/vtep-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchingProfilesswitchingProfileIdsummary(switchingprofileid, callback)</td>
    <td style="padding:15px">Get Counts of Ports and Switches Using This Switching Profile</td>
    <td style="padding:15px">{base_path}/{version}/switching-profiles/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalSwitches(LogicalSwitch, callback)</td>
    <td style="padding:15px">Create a Logical Switch</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitches(cursor, diagnostic, includedFields, pageSize, sortAscending, sortBy, switchingProfileId, transportType, transportZoneId, callback)</td>
    <td style="padding:15px">List all Logical Switches</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalPorts(LogicalPort, callback)</td>
    <td style="padding:15px">Create a Logical Port</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPorts(attachmentId, attachmentType, bridgeClusterId, containerPortsOnly, cursor, diagnostic, includedFields, logicalSwitchId, pageSize, parentVifId, sortAscending, sortBy, switchingProfileId, transportNodeId, transportZoneId, callback)</td>
    <td style="padding:15px">List All Logical Ports</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPortslportIdstate(lportid, callback)</td>
    <td style="padding:15px">Get realized state & location of a logical port</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitcheslswitchIdsummary(lswitchid, callback)</td>
    <td style="padding:15px">Get Logical Switch runtime status info for a given logical switch</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitcheslswitchIdmacTableformatcsv(lswitchid, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get MAC Table for Logical Switch of the Given ID (lswitch-id)</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}/mac-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitcheslswitchIdstate(lswitchid, callback)</td>
    <td style="padding:15px">Get the realized state associated with provided logical switch id</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitcheslswitchIdstatistics(lswitchid, source, callback)</td>
    <td style="padding:15px">Get Statistics for Logical Switch of the Given ID (lswitch-id)</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitchesstatus(cursor, diagnostic, includedFields, pageSize, sortAscending, sortBy, source, switchingProfileId, transportType, transportZoneId, callback)</td>
    <td style="padding:15px">Get Status Summary of All Logical Switches in the System</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSwitchingProfilesswitchingProfileId(switchingprofileid, BaseSwitchingProfile, callback)</td>
    <td style="padding:15px">Update a Switching Profile</td>
    <td style="padding:15px">{base_path}/{version}/switching-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchingProfilesswitchingProfileId(switchingprofileid, callback)</td>
    <td style="padding:15px">Get Switching Profile by ID</td>
    <td style="padding:15px">{base_path}/{version}/switching-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSwitchingProfilesswitchingProfileId(switchingprofileid, unbind, callback)</td>
    <td style="padding:15px">Delete a Switching Profile</td>
    <td style="padding:15px">{base_path}/{version}/switching-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSwitchingProfiles(BaseSwitchingProfile, callback)</td>
    <td style="padding:15px">Create a Switching Profile</td>
    <td style="padding:15px">{base_path}/{version}/switching-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchingProfiles(cursor, includeSystemOwned, includedFields, pageSize, sortAscending, sortBy, switchingProfileType, callback)</td>
    <td style="padding:15px">List Switching Profiles</td>
    <td style="padding:15px">{base_path}/{version}/switching-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPortslportIdstatus(lportid, source, callback)</td>
    <td style="padding:15px">Get Operational Status for Logical Port of a Given Port ID (lport-id)</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPortslportIdmacTableformatcsv(lportid, cursor, includedFields, pageSize, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get MAC table of a logical port with a given port id (lport-id)</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}/mac-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPortslportIdmacTable(lportid, cursor, includedFields, pageSize, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get MAC table of a logical port with a given port id (lport-id)</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}/mac-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitcheslswitchIdvtepTableformatcsv(lswitchid, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get virtual tunnel endpoint table for logical switch of the given
ID (lswitch-id)
</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}/vtep-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitcheslswitchIdmacTable(lswitchid, cursor, includedFields, pageSize, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get MAC Table for Logical Switch of the Given ID (lswitch-id)</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}/mac-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPortsstatus(attachmentId, attachmentType, bridgeClusterId, containerPortsOnly, cursor, diagnostic, includedFields, logicalSwitchId, pageSize, parentVifId, sortAscending, sortBy, source, switchingProfileId, transportNodeId, transportZoneId, callback)</td>
    <td style="padding:15px">Get Operational Status Summary of All Logical Ports in the System</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPortslportIdstatistics(lportid, source, callback)</td>
    <td style="padding:15px">Get Statistics for Logical Port of a Given Port ID (lport-id)</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalSwitcheslswitchId(lswitchid, LogicalSwitch, callback)</td>
    <td style="padding:15px">Update a Logical Switch</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalSwitcheslswitchId(lswitchid, cascade, detach, callback)</td>
    <td style="padding:15px">Delete a Logical Switch</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitcheslswitchId(lswitchid, callback)</td>
    <td style="padding:15px">Get Logical Switch associated with the provided id (lswitch-id)</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalSwitchesstate(status, callback)</td>
    <td style="padding:15px">List logical switches by realized state</td>
    <td style="padding:15px">{base_path}/{version}/logical-switches/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalPortslportId(lportid, LogicalPort, callback)</td>
    <td style="padding:15px">Update a Logical Port</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPortslportId(lportid, callback)</td>
    <td style="padding:15px">Get Information About a Logical Port</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalPortslportId(lportid, detach, callback)</td>
    <td style="padding:15px">Delete a Logical Port</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoverysessionssessionIdinstalledApps(sessionid, appProfileId, cursor, includedFields, pageSize, sortAscending, sortBy, vmId, callback)</td>
    <td style="padding:15px">Returns the details of the installed apps for the app profile ID in that session</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}/installed-apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppDiscoverysessionssessionIdreClassify(sessionid, SessionReclassificationParameter, callback)</td>
    <td style="padding:15px">Re-classify a completed application discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}/re-classify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppDiscoverysessionssessionIdreportappInfoAndVmformatcsv(sessionid, ReportAppResultsForVmsRequestParameters, callback)</td>
    <td style="padding:15px">Export app discovery results in CSV format</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}/report/app-info-and-vm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoverysessionssessionIdnsGroups(sessionid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">ns-groups in this application discovery session</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}/ns-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoverysessionssessionIdsummary(sessionid, callback)</td>
    <td style="padding:15px">Returns the summary of the application discovery session</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppDiscoveryappProfiles(AppProfile, callback)</td>
    <td style="padding:15px">Adds a new app profile</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/app-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoveryappProfiles(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Returns list of app profile IDs created</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/app-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoverysessionssessionIdreportappProfileAndAppInfoformatcsv(sessionid, callback)</td>
    <td style="padding:15px">Export app profiles in CSV format for a given sessiom</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}/report/app-profile-and-app-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppDiscoverysessionssessionId(sessionid, callback)</td>
    <td style="padding:15px">Cancel and delete the application discovery session</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoverysessionssessionId(sessionid, callback)</td>
    <td style="padding:15px">Returns the status of the application discovery session and other details</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoverysessionssessionIdappProfiles(sessionid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">application profiles in this application discovery session</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}/app-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoverysessionssessionIdnsGroupsnsGroupIdmembers(sessionid, nsgroupid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">vms in the ns-group of the application discovery session</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions/{pathv1}/ns-groups/{pathv2}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAppDiscoveryappProfilesappProfileId(appprofileid, AppProfile, callback)</td>
    <td style="padding:15px">Update AppProfile</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/app-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoveryappProfilesappProfileId(appprofileid, callback)</td>
    <td style="padding:15px">Returns detail of the app profile</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/app-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppDiscoveryappProfilesappProfileId(appprofileid, force, callback)</td>
    <td style="padding:15px">Delete App Profile</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/app-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppDiscoverysessions(StartAppDiscoverySessionParameters, callback)</td>
    <td style="padding:15px">Starts the discovery of application discovery session</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppDiscoverysessions(cursor, groupId, includedFields, pageSize, sortAscending, sortBy, status, callback)</td>
    <td style="padding:15px">Returns the list of the application discovery sessions available</td>
    <td style="padding:15px">{base_path}/{version}/app-discovery/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMacSetsmacSetIdmembersmacAddress(macsetid, macaddress, callback)</td>
    <td style="padding:15px">Remove a MAC address from given MACSet</td>
    <td style="padding:15px">{base_path}/{version}/mac-sets/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsServicesnsServiceId(nsserviceid, NSService, callback)</td>
    <td style="padding:15px">Update NSService</td>
    <td style="padding:15px">{base_path}/{version}/ns-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsServicesnsServiceId(nsserviceid, force, callback)</td>
    <td style="padding:15px">Delete NSService</td>
    <td style="padding:15px">{base_path}/{version}/ns-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsServicesnsServiceId(nsserviceid, callback)</td>
    <td style="padding:15px">Read NSService</td>
    <td style="padding:15px">{base_path}/{version}/ns-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMacSetsmacSetIdmembers(macsetid, MACAddressElement, callback)</td>
    <td style="padding:15px">Add a MAC address to a MACSet</td>
    <td style="padding:15px">{base_path}/{version}/mac-sets/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMacSetsmacSetIdmembers(macsetid, callback)</td>
    <td style="padding:15px">Get all MACAddresses in a MACSet</td>
    <td style="padding:15px">{base_path}/{version}/mac-sets/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupsnsgroupIdserviceAssociations(nsgroupid, cursor, fetchParentgroupAssociations, includedFields, pageSize, serviceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get services to which the given nsgroup belongs to
</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}/service-associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupsnsGroupIdeffectiveIpAddressMembers(nsgroupid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get Effective IPAddress translated from the NSGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}/effective-ip-address-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpSets(IPSet, callback)</td>
    <td style="padding:15px">Create IPSet</td>
    <td style="padding:15px">{base_path}/{version}/ip-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpSets(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List IPSets</td>
    <td style="padding:15px">{base_path}/{version}/ip-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupsnsGroupIdeffectiveLogicalSwitchMembers(nsgroupid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get Effective switch members translated from the NSGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}/effective-logical-switch-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupsnsGroupIdmemberTypes(nsgroupid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get member types from NSGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}/member-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsGroupsnsGroupId(nsgroupid, NSGroup, callback)</td>
    <td style="padding:15px">Update NSGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsGroupsnsGroupId(nsgroupid, NSGroupExpressionList, action, callback)</td>
    <td style="padding:15px">Add NSGroup expression</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupsnsGroupId(nsgroupid, populateReferences, callback)</td>
    <td style="padding:15px">Read NSGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsGroupsnsGroupId(nsgroupid, force, callback)</td>
    <td style="padding:15px">Delete NSGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupsnsGroupIdeffectiveVirtualMachineMembers(nsgroupid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get Effective Virtual Machine members of the specified NSGroup.</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}/effective-virtual-machine-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNsServiceGroupsnsServiceGroupId(nsservicegroupid, NSServiceGroup, callback)</td>
    <td style="padding:15px">Update NSServiceGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-service-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsServiceGroupsnsServiceGroupId(nsservicegroupid, force, callback)</td>
    <td style="padding:15px">Delete NSServiceGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-service-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsServiceGroupsnsServiceGroupId(nsservicegroupid, callback)</td>
    <td style="padding:15px">Read NSServiceGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-service-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpSetsipSetId(ipsetid, IPSet, callback)</td>
    <td style="padding:15px">Update IPSet</td>
    <td style="padding:15px">{base_path}/{version}/ip-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpSetsipSetId(ipsetid, force, callback)</td>
    <td style="padding:15px">Delete IPSet</td>
    <td style="padding:15px">{base_path}/{version}/ip-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpSetsipSetId(ipsetid, callback)</td>
    <td style="padding:15px">Read IPSet</td>
    <td style="padding:15px">{base_path}/{version}/ip-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMacSetsmacSetId(macsetid, MACSet, callback)</td>
    <td style="padding:15px">Update MACSet</td>
    <td style="padding:15px">{base_path}/{version}/mac-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMacSetsmacSetId(macsetid, force, callback)</td>
    <td style="padding:15px">Delete MACSet</td>
    <td style="padding:15px">{base_path}/{version}/mac-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMacSetsmacSetId(macsetid, callback)</td>
    <td style="padding:15px">Read MACSet</td>
    <td style="padding:15px">{base_path}/{version}/mac-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsServices(NSService, callback)</td>
    <td style="padding:15px">Create NSService</td>
    <td style="padding:15px">{base_path}/{version}/ns-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsServices(cursor, defaultService, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List all NSServices</td>
    <td style="padding:15px">{base_path}/{version}/ns-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupsunassociatedVirtualMachines(cursor, displayName, externalId, hostId, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get the list of all the virtual machines that are not a part of any existing NSGroup.</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/unassociated-virtual-machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMacSets(MACSet, callback)</td>
    <td style="padding:15px">Create MACSet</td>
    <td style="padding:15px">{base_path}/{version}/mac-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMacSets(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List MACSets</td>
    <td style="padding:15px">{base_path}/{version}/mac-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupsnsGroupIdeffectiveLogicalPortMembers(nsgroupid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get Effective Logical Ports translated from the NSgroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups/{pathv1}/effective-logical-port-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsServiceGroups(NSServiceGroup, callback)</td>
    <td style="padding:15px">Create NSServiceGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-service-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsServiceGroups(cursor, defaultService, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List all NSServiceGroups</td>
    <td style="padding:15px">{base_path}/{version}/ns-service-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNsGroups(NSGroup, callback)</td>
    <td style="padding:15px">Create NSGroup</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroups(cursor, includedFields, memberTypes, pageSize, populateReferences, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List NSGroups</td>
    <td style="padding:15px">{base_path}/{version}/ns-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpfixObsPointsswitchGlobal(IpfixObsPointConfig, callback)</td>
    <td style="padding:15px">Update global switch IPFIX export configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix-obs-points/switch-global?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpfixObsPointsswitchGlobal(callback)</td>
    <td style="padding:15px">Read global switch IPFIX export configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix-obs-points/switch-global?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTraceflowstraceflowId(traceflowid, callback)</td>
    <td style="padding:15px">Delete the Traceflow round</td>
    <td style="padding:15px">{base_path}/{version}/traceflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTraceflowstraceflowId(traceflowid, callback)</td>
    <td style="padding:15px">Get the Traceflow round status and result summary</td>
    <td style="padding:15px">{base_path}/{version}/traceflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZoneszoneIdtransportNodeStatus(zoneid, cursor, includedFields, pageSize, sortAscending, sortBy, source, status, callback)</td>
    <td style="padding:15px">Read status of transport nodes in a transport zone</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/{pathv1}/transport-node-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTraceflows(TraceflowRequest, callback)</td>
    <td style="padding:15px">Initiate a Traceflow Operation on the Specified Port</td>
    <td style="padding:15px">{base_path}/{version}/traceflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTraceflows(cursor, includedFields, lportId, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List all Traceflow rounds</td>
    <td style="padding:15px">{base_path}/{version}/traceflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodelogslogName(logname, callback)</td>
    <td style="padding:15px">Read node log properties</td>
    <td style="padding:15px">{base_path}/{version}/node/logs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodelogslogNamedata(logname, callback)</td>
    <td style="padding:15px">Read node log contents</td>
    <td style="padding:15px">{base_path}/{version}/node/logs/{pathv1}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodesnodeIdpnicBondStatus(nodeid, callback)</td>
    <td style="padding:15px">Get high-level summary of a transport node</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}/pnic-bond-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodelogs(callback)</td>
    <td style="padding:15px">List available node logs</td>
    <td style="padding:15px">{base_path}/{version}/node/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodesstatus(callback)</td>
    <td style="padding:15px">Get high-level summary of all transport nodes. The service layer does not support source = realtime or cached.</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodesnodeIdremoteTransportNodeStatus(nodeid, bfdDiagnosticCode, cursor, includedFields, pageSize, sortAscending, sortBy, source, tunnelStatus, callback)</td>
    <td style="padding:15px">Read status of all transport nodes with tunnel connections to transport node
</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}/remote-transport-node-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodesnodeIdstatus(nodeid, source, callback)</td>
    <td style="padding:15px">Read status of a transport node</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMirrorSessions(PortMirroringSession, callback)</td>
    <td style="padding:15px">Create a mirror session</td>
    <td style="padding:15px">{base_path}/{version}/mirror-sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMirrorSessions(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List all mirror sessions</td>
    <td style="padding:15px">{base_path}/{version}/mirror-sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMirrorSessionsmirrorSessionId(mirrorsessionid, PortMirroringSession, callback)</td>
    <td style="padding:15px">Update the mirror session</td>
    <td style="padding:15px">{base_path}/{version}/mirror-sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMirrorSessionsmirrorSessionId(mirrorsessionid, callback)</td>
    <td style="padding:15px">Get the mirror session</td>
    <td style="padding:15px">{base_path}/{version}/mirror-sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMirrorSessionsmirrorSessionId(mirrorsessionid, callback)</td>
    <td style="padding:15px">Delete the mirror session</td>
    <td style="padding:15px">{base_path}/{version}/mirror-sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalPortslportIdforwardingPath(lportid, peerPortId, callback)</td>
    <td style="padding:15px">Get networking entities between two logical ports with VIF attachment</td>
    <td style="padding:15px">{base_path}/{version}/logical-ports/{pathv1}/forwarding-path?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTraceflowstraceflowIdobservations(traceflowid, componentName, componentType, cursor, includedFields, pageSize, resourceType, sortAscending, sortBy, transportNodeName, callback)</td>
    <td style="padding:15px">Get observations for the Traceflow round</td>
    <td style="padding:15px">{base_path}/{version}/traceflows/{pathv1}/observations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZonesstatus(callback)</td>
    <td style="padding:15px">Get high-level summary of a transport zone. The service layer does not support source = realtime or cached.</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpfixObsPoints(callback)</td>
    <td style="padding:15px">Get the list of IPFIX observation points</td>
    <td style="padding:15px">{base_path}/{version}/ipfix-obs-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZoneszoneIdstatus(zoneid, source, callback)</td>
    <td style="padding:15px">Get high-level summary of a transport zone</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZoneszoneIdtransportNodeStatusReport(zoneid, source, status, callback)</td>
    <td style="padding:15px">Creates a status report of transport nodes in a transport zone</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/{pathv1}/transport-node-status-report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZonestransportNodeStatusReport(source, status, callback)</td>
    <td style="padding:15px">Creates a status report of transport nodes of all the transport zones</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/transport-node-status-report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMirrorSessionsmirrorSessionIdactionverify(mirrorsessionid, callback)</td>
    <td style="padding:15px">Verify whether the mirror session is still valid</td>
    <td style="padding:15px">{base_path}/{version}/mirror-sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZonestransportNodeStatus(cursor, includedFields, pageSize, sortAscending, sortBy, source, status, callback)</td>
    <td style="padding:15px">Read status of all the transport nodes</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/transport-node-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterslogicalRouterIdroutingstaticRoutes(logicalrouterid, StaticRoute, callback)</td>
    <td style="padding:15px">Add Static Routes on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingstaticRoutes(logicalrouterid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Paginated List of Static Routes</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouters(LogicalRouter, callback)</td>
    <td style="padding:15px">Create a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouters(cursor, includedFields, pageSize, routerType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Logical Routers</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingforwardingTable(logicalrouterid, cursor, includedFields, networkPrefix, pageSize, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get FIB table on a specified node for a logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/forwarding-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterslogicalRouterIdroutingrouteMaps(logicalrouterid, RouteMap, callback)</td>
    <td style="padding:15px">Add RouteMap on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/route-maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingrouteMaps(logicalrouterid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Paginated List of RouteMaps</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/route-maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdnatrulesruleIdstatistics(logicalrouterid, ruleid, source, callback)</td>
    <td style="padding:15px">Get the statistics of a specified logical router NAT Rule</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/nat/rules/{pathv2}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingrouteTableformatcsv(logicalrouterid, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get route table on a node for a logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/route-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId(logicalrouterid, bfdpeerid, StaticHopBfdPeer, callback)</td>
    <td style="padding:15px">Update a static route BFD peer</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes/bfd-peers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId(logicalrouterid, bfdpeerid, callback)</td>
    <td style="padding:15px">Read a static route BFD peer</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes/bfd-peers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId(logicalrouterid, bfdpeerid, force, callback)</td>
    <td style="padding:15px">Delete a specified static route BFD peer cofigured on a specified logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes/bfd-peers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdstatus(logicalrouterid, source, callback)</td>
    <td style="padding:15px">Get the status for the Logical Router of the given id</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterslogicalRouterIdroutingipPrefixLists(logicalrouterid, IPPrefixList, callback)</td>
    <td style="padding:15px">Add IPPrefixList on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/ip-prefix-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingipPrefixLists(logicalrouterid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Paginated List of IPPrefixLists</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/ip-prefix-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingredistribution(logicalrouterid, RedistributionConfig, callback)</td>
    <td style="padding:15px">Update the Redistribution Configuration on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/redistribution?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingredistribution(logicalrouterid, callback)</td>
    <td style="padding:15px">Read the Redistribution Configuration on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/redistribution?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcprelaysrelayId(relayid, DhcpRelayService, callback)</td>
    <td style="padding:15px">Update a DHCP Relay Service</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relays/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcprelaysrelayId(relayid, callback)</td>
    <td style="padding:15px">Read a DHCP Relay Service</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relays/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcprelaysrelayId(relayid, callback)</td>
    <td style="padding:15px">Delete a DHCP Relay Service</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relays/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingstaticRoutesid(logicalrouterid, id, StaticRoute, callback)</td>
    <td style="padding:15px">Update a specific Static Route Rule on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingstaticRoutesid(logicalrouterid, id, callback)</td>
    <td style="padding:15px">Get a specific Static Route on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalRouterslogicalRouterIdroutingstaticRoutesid(logicalrouterid, id, callback)</td>
    <td style="padding:15px">Delete a specific Static Route on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcprelayProfilesrelayProfileId(relayprofileid, DhcpRelayProfile, callback)</td>
    <td style="padding:15px">Update a DHCP Relay Profile</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relay-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcprelayProfilesrelayProfileId(relayprofileid, callback)</td>
    <td style="padding:15px">Delete a DHCP Relay Profile</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relay-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcprelayProfilesrelayProfileId(relayprofileid, callback)</td>
    <td style="padding:15px">Read a DHCP Relay Profile</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relay-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterslogicalRouterIdactionreprocess(logicalrouterid, callback)</td>
    <td style="padding:15px">Reprocesses a logical router configuration and publish updates to controller</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIddebugInfoformattext(logicalrouterid, callback)</td>
    <td style="padding:15px">Read the debug information for the logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/debug-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingbgpneighborsstatus(logicalrouterid, cursor, includedFields, pageSize, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get the status of all the BGP neighbors for the Logical Router of the given id</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp/neighbors/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingredistributionrules(logicalrouterid, RedistributionRuleList, callback)</td>
    <td style="padding:15px">Update All the Redistribution Rules on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/redistribution/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingredistributionrules(logicalrouterid, callback)</td>
    <td style="padding:15px">Read All the Redistribution Rules on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/redistribution/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServices(LogicalService, callback)</td>
    <td style="padding:15px">Create a Logical Router Service</td>
    <td style="padding:15px">{base_path}/{version}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(cursor, includedFields, pageSize, resourceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List all Logical Router Services</td>
    <td style="padding:15px">{base_path}/{version}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcprelayProfiles(DhcpRelayProfile, callback)</td>
    <td style="padding:15px">Create a DHCP Relay Profile</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relay-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcprelayProfiles(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List All DHCP Relay Profiles</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relay-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingrouteTable(logicalrouterid, cursor, includedFields, pageSize, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get route table on a given node for a logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/route-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterslogicalRouterIdroutingbgpneighbors(logicalrouterid, BgpNeighbor, callback)</td>
    <td style="padding:15px">Add a new BGP Neighbor on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingbgpneighbors(logicalrouterid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Paginated list of BGP Neighbors on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingforwardingTableformatcsv(logicalrouterid, networkPrefix, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get FIB table on a specified node for a logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/forwarding-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingroutingTable(logicalrouterid, cursor, includedFields, networkPrefix, pageSize, routeSource, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get RIB table on a specified node for a logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/routing-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterPortslogicalRouterPortIdarpTable(logicalrouterportid, cursor, includedFields, pageSize, sortAscending, sortBy, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get the ARP table for the Logical Router Port of the given id</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports/{pathv1}/arp-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterPortslogicalRouterPortIdstatisticssummary(logicalrouterportid, source, callback)</td>
    <td style="padding:15px">Get the statistics summary of a specified logical router port</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports/{pathv1}/statistics/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServiceProfiles(ServiceProfile, callback)</td>
    <td style="padding:15px">Create a Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/service-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceProfiles(cursor, includedFields, pageSize, resourceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List All Service Profiles</td>
    <td style="padding:15px">{base_path}/{version}/service-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterPorts(LogicalRouterPort, callback)</td>
    <td style="padding:15px">Create a Logical Router Port</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterPorts(cursor, includedFields, logicalRouterId, logicalSwitchId, pageSize, resourceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Logical Router Ports</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdnatrulesstatistics(logicalrouterid, source, callback)</td>
    <td style="padding:15px">Get the statistics of all rules of the logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/nat/rules/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodesnodeIdstatisticsnatRules(nodeid, source, callback)</td>
    <td style="padding:15px">Get statistics for all logical router NAT rules on a transport node</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}/statistics/nat-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers(logicalrouterid, StaticHopBfdPeer, callback)</td>
    <td style="padding:15px">Create a static hop BFD peer</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes/bfd-peers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers(logicalrouterid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List static routes BFD Peers</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/static-routes/bfd-peers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterId(logicalrouterid, LogicalRouter, callback)</td>
    <td style="padding:15px">Update a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterId(logicalrouterid, callback)</td>
    <td style="padding:15px">Read Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalRouterslogicalRouterId(logicalrouterid, force, callback)</td>
    <td style="padding:15px">Delete a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingadvertisementrules(logicalrouterid, AdvertiseRuleList, callback)</td>
    <td style="padding:15px">Update the Advertisement Rules on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/advertisement/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingadvertisementrules(logicalrouterid, callback)</td>
    <td style="padding:15px">Read the Advertisement Rules on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/advertisement/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingroutingTableformatcsv(logicalrouterid, networkPrefix, routeSource, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get RIB table on a specified node for a logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/routing-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcprelays(DhcpRelayService, callback)</td>
    <td style="padding:15px">Create a DHCP Relay Service</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcprelays(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List all DHCP Relay Services</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/relays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingipPrefixListsid(logicalrouterid, id, IPPrefixList, callback)</td>
    <td style="padding:15px">Update a specific IPPrefixList on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/ip-prefix-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingipPrefixListsid(logicalrouterid, id, callback)</td>
    <td style="padding:15px">Get a specific IPPrefixList on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/ip-prefix-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalRouterslogicalRouterIdroutingipPrefixListsid(logicalrouterid, id, callback)</td>
    <td style="padding:15px">Delete a specific IPPrefixList on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/ip-prefix-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterPortslogicalRouterPortIdstatistics(logicalrouterportid, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get the statistics of a specified logical router port on all or a specified node</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports/{pathv1}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingrouteMapsid(logicalrouterid, id, RouteMap, callback)</td>
    <td style="padding:15px">Update a specific RouteMap on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/route-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingrouteMapsid(logicalrouterid, id, callback)</td>
    <td style="padding:15px">Get a specific RouteMap on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/route-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalRouterslogicalRouterIdroutingrouteMapsid(logicalrouterid, id, callback)</td>
    <td style="padding:15px">Delete a specific RouteMap on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/route-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingbgp(logicalrouterid, BgpConfig, callback)</td>
    <td style="padding:15px">Update the BGP Configuration on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingbgp(logicalrouterid, callback)</td>
    <td style="padding:15px">Read the BGP Configuration on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdrouting(logicalrouterid, RoutingConfig, callback)</td>
    <td style="padding:15px">Update the Routing Configuration</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdrouting(logicalrouterid, callback)</td>
    <td style="padding:15px">Read the Routing Configuration</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingadvertisement(logicalrouterid, AdvertisementConfig, callback)</td>
    <td style="padding:15px">Update the Advertisement Configuration on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingadvertisement(logicalrouterid, callback)</td>
    <td style="padding:15px">Read the Advertisement Configuration on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/advertisement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesserviceId(serviceid, LogicalService, callback)</td>
    <td style="padding:15px">Update a Logical Router Service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesserviceId(serviceid, callback)</td>
    <td style="padding:15px">Read a Logical Router Service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicesserviceId(serviceid, callback)</td>
    <td style="padding:15px">Delete a Logical Router Service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingbgpneighborsid(logicalrouterid, id, BgpNeighbor, callback)</td>
    <td style="padding:15px">Update a specific BGP Neighbor on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp/neighbors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterslogicalRouterIdroutingbgpneighborsid(logicalrouterid, id, action, callback)</td>
    <td style="padding:15px">Unset/Delete password property on specific BGP Neighbor on Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp/neighbors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingbgpneighborsid(logicalrouterid, id, callback)</td>
    <td style="padding:15px">Read a specific BGP Neighbor on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp/neighbors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalRouterslogicalRouterIdroutingbgpneighborsid(logicalrouterid, id, callback)</td>
    <td style="padding:15px">Delete a specific BGP Neighbor on a Logical Router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bgp/neighbors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdroutingbfdConfig(logicalrouterid, BfdConfig, callback)</td>
    <td style="padding:15px">Update the BFD Configuration for BFD peers for routing</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bfd-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdroutingbfdConfig(logicalrouterid, callback)</td>
    <td style="padding:15px">Read the Routing BFD Configuration</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/routing/bfd-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServiceProfilesserviceProfileId(serviceprofileid, ServiceProfile, callback)</td>
    <td style="padding:15px">Update a Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/service-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceProfilesserviceProfileId(serviceprofileid, callback)</td>
    <td style="padding:15px">Read a Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/service-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceProfilesserviceProfileId(serviceprofileid, callback)</td>
    <td style="padding:15px">Delete a Service Profile</td>
    <td style="padding:15px">{base_path}/{version}/service-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterPortslogicalRouterPortIdarpTableformatcsv(logicalrouterportid, source, transportNodeId, callback)</td>
    <td style="padding:15px">Get the ARP table for the Logical Router Port of the given id</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports/{pathv1}/arp-table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterPortslogicalRouterPortId(logicalrouterportid, LogicalRouterPort, callback)</td>
    <td style="padding:15px">Update a Logical Router Port</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterPortslogicalRouterPortId(logicalrouterportid, callback)</td>
    <td style="padding:15px">Read Logical Router Port</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalRouterPortslogicalRouterPortId(logicalrouterportid, force, callback)</td>
    <td style="padding:15px">Delete a Logical Router Port</td>
    <td style="padding:15px">{base_path}/{version}/logical-router-ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLogicalRouterslogicalRouterIdnatrulesruleId(logicalrouterid, ruleid, NatRule, callback)</td>
    <td style="padding:15px">Update a specific NAT rule from a given logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/nat/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdnatrulesruleId(logicalrouterid, ruleid, callback)</td>
    <td style="padding:15px">Get a specific NAT rule from a given logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/nat/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLogicalRouterslogicalRouterIdnatrulesruleId(logicalrouterid, ruleid, callback)</td>
    <td style="padding:15px">Delete a specific NAT rule from a logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/nat/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogicalRouterslogicalRouterIdnatrules(logicalrouterid, NatRule, callback)</td>
    <td style="padding:15px">Add a NAT rule in a specific logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/nat/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalRouterslogicalRouterIdnatrules(logicalrouterid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List NAT rules of the logical router</td>
    <td style="padding:15px">{base_path}/{version}/logical-routers/{pathv1}/nat/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusternodesnodeIdnetworkinterfaces(nodeid, source, callback)</td>
    <td style="padding:15px">List the specified node's Network Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes/{pathv1}/network/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessyslogexporters(NodeSyslogExporterProperties, callback)</td>
    <td style="padding:15px">Add node syslog exporter</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog/exporters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessyslogexporters(callback)</td>
    <td style="padding:15px">List node syslog exporters</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog/exporters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicescmInventoryactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the manager service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/cm-inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodenetwork(callback)</td>
    <td style="padding:15px">Read network configuration properties</td>
    <td style="padding:15px">{base_path}/{version}/node/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusternodesnodeIdstatus(nodeid, source, callback)</td>
    <td style="padding:15px">Read Cluster Node Status</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesmgmtPlaneBusactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the Rabbit MQ service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/mgmt-plane-bus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodenetworknameServers(NodeNameServersProperties, callback)</td>
    <td style="padding:15px">Update the NSX Manager's Name Servers</td>
    <td style="padding:15px">{base_path}/{version}/node/network/name-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodenetworknameServers(callback)</td>
    <td style="padding:15px">Read the NSX Manager's Name Servers</td>
    <td style="padding:15px">{base_path}/{version}/node/network/name-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusternodes(AddClusterNodeSpec, action, callback)</td>
    <td style="padding:15px">Add a Node to the Cluster</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusternodes(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Cluster Node Configurations</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodefileStorefileNameactioncopyToRemoteFile(filename, CopyToRemoteFileProperties, callback)</td>
    <td style="padding:15px">Copy file in the file store to a remote file store</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodefileStorefileNamedata(filename, callback)</td>
    <td style="padding:15px">Replace file contents</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store/{pathv1}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodefileStorefileNamedata(filename, callback)</td>
    <td style="padding:15px">Read file contents</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store/{pathv1}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesnsxUpgradeAgentactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX upgrade agent service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-upgrade-agent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessearchstatus(callback)</td>
    <td style="padding:15px">Read NSX Search service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/search/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesntpactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NTP service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesmanageractionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeservicessnmp(NodeSnmpServiceProperties, callback)</td>
    <td style="padding:15px">Update SNMP service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessnmp(callback)</td>
    <td style="padding:15px">Read SNMP service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterrestorestatus(callback)</td>
    <td style="padding:15px">Query Restore Request Status</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagement(callback)</td>
    <td style="padding:15px">Return the Properties of a Trust Manager</td>
    <td style="padding:15px">{base_path}/{version}/trust-management?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterbackupshistory(callback)</td>
    <td style="padding:15px">Get backup history</td>
    <td style="padding:15px">{base_path}/{version}/cluster/backups/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesliagentactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the liagent service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/liagent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeservicesntp(NodeNtpServiceProperties, callback)</td>
    <td style="padding:15px">Update NTP service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesntp(callback)</td>
    <td style="padding:15px">Read NTP service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeserviceshttpactionstop(callback)</td>
    <td style="padding:15px">Stop the http service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/http?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesnsxUpgradeAgentactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX upgrade agent service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-upgrade-agent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessshactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the ssh service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesliagent(callback)</td>
    <td style="padding:15px">Read liagent service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/liagent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTrustManagementcrlsactionimport(CrlObjectData, callback)</td>
    <td style="padding:15px">Add a New Certificate Revocation List</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/crls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterrestorebackuptimestamps(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List timestamps of all available Cluster Backups.</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore/backuptimestamps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesmgmtPlaneBusactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the Rabbit MQ service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/mgmt-plane-bus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAdministrationsupportBundlesactioncollect(SupportBundleRequest, overrideAsyncResponse, callback)</td>
    <td style="padding:15px">Collect support bundles from registered cluster and fabric nodes</td>
    <td style="padding:15px">{base_path}/{version}/administration/support-bundles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessnmpactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the SNMP service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusterrestoreactionretry(callback)</td>
    <td style="padding:15px">Retry any failed restore operation</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesinstallUpgradeactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX install-upgrade service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/install-upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeserviceshttpactionstart(callback)</td>
    <td style="padding:15px">Start the http service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/http?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeactionrestart(callback)</td>
    <td style="padding:15px">Restart or shutdown node</td>
    <td style="padding:15px">{base_path}/{version}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodenetworkinterfacesinterfaceId(interfaceid, NodeNetworkInterfaceProperties, callback)</td>
    <td style="padding:15px">Update the NSX Manager's Network Interface</td>
    <td style="padding:15px">{base_path}/{version}/node/network/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodenetworkinterfacesinterfaceId(interfaceid, callback)</td>
    <td style="padding:15px">Read the NSX Manager's Network Interface</td>
    <td style="padding:15px">{base_path}/{version}/node/network/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeservicesssh(NodeSshServiceProperties, callback)</td>
    <td style="padding:15px">Update ssh service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesssh(callback)</td>
    <td style="padding:15px">Read ssh service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNoderabbitmqManagementPort(callback)</td>
    <td style="padding:15px">Set RabbitMQ management port</td>
    <td style="padding:15px">{base_path}/{version}/node/rabbitmq-management-port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNoderabbitmqManagementPort(callback)</td>
    <td style="padding:15px">Delete RabbitMQ management port</td>
    <td style="padding:15px">{base_path}/{version}/node/rabbitmq-management-port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNoderabbitmqManagementPort(callback)</td>
    <td style="padding:15px">Check if RabbitMQ management port is enabled or not</td>
    <td style="padding:15px">{base_path}/{version}/node/rabbitmq-management-port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodetaskstaskId(taskid, callback)</td>
    <td style="padding:15px">Delete task</td>
    <td style="padding:15px">{base_path}/{version}/node/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodetaskstaskId(taskid, suppressRedirect, callback)</td>
    <td style="padding:15px">Read task properties</td>
    <td style="padding:15px">{base_path}/{version}/node/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusteractionsummarizeInventoryToRemote(callback)</td>
    <td style="padding:15px">Request one-time inventory summary.</td>
    <td style="padding:15px">{base_path}/{version}/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodetaskstaskIdresponse(taskid, callback)</td>
    <td style="padding:15px">Read asynchronous task response</td>
    <td style="padding:15px">{base_path}/{version}/node/tasks/{pathv1}/response?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesmgmtPlaneBusstatus(callback)</td>
    <td style="padding:15px">Read Rabbit MQ service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/mgmt-plane-bus/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusteractionbackupToRemote(callback)</td>
    <td style="padding:15px">Request one-time backup</td>
    <td style="padding:15px">{base_path}/{version}/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterbackupsstatus(callback)</td>
    <td style="padding:15px">Get backup status</td>
    <td style="padding:15px">{base_path}/{version}/cluster/backups/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTrustManagementcsrscsrIdactionimport(csrid, TrustObjectData, callback)</td>
    <td style="padding:15px">Import a Certificate Associated with an Approved CSR</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/csrs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesntpactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NTP service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterstatus(source, callback)</td>
    <td style="padding:15px">Read Cluster Status</td>
    <td style="padding:15px">{base_path}/{version}/cluster/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesmanageractionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicescmInventorystatus(callback)</td>
    <td style="padding:15px">Read manager service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/cm-inventory/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeusersuserid(userid, NodeUserProperties, callback)</td>
    <td style="padding:15px">Update node user</td>
    <td style="padding:15px">{base_path}/{version}/node/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeusersuserid(userid, callback)</td>
    <td style="padding:15px">Read node user</td>
    <td style="padding:15px">{base_path}/{version}/node/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicescmInventoryactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the manager service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/cm-inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesnsxMessageBusactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX Message Bus service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-message-bus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusternodesnodeIdnetworkinterfacesinterfaceIdstats(nodeid, interfaceid, source, callback)</td>
    <td style="padding:15px">Read the NSX Manager/Controller's Network Interface Statistics</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes/{pathv1}/network/interfaces/{pathv2}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusternodesactionrevokeMissingNodes(RevokeNodeRequest, callback)</td>
    <td style="padding:15px">Revoke Missing Nodes from the Cluster</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putConfigsmanagement(ManagementConfig, callback)</td>
    <td style="padding:15px">Update NSX Management nodes global configuration</td>
    <td style="padding:15px">{base_path}/{version}/configs/management?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigsmanagement(callback)</td>
    <td style="padding:15px">Read NSX Management nodes global configuration.</td>
    <td style="padding:15px">{base_path}/{version}/configs/management?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesntpstatus(callback)</td>
    <td style="padding:15px">Read NTP service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ntp/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterrestoreinstructionResources(cursor, includedFields, instructionId, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List resources for a given instruction, to be
shown to/executed by users.
</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore/instruction-resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagementcertificates(cursor, details, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return All the User-Facing Components' Certificates</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagementcertificatescertId(certid, details, callback)</td>
    <td style="padding:15px">Show Certificate Data for the Given Certificate ID</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/certificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTrustManagementcertificatescertId(certid, callback)</td>
    <td style="padding:15px">Delete Certificate for the Given Certificate ID</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/certificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodenetworksearchDomains(NodeSearchDomainsProperties, callback)</td>
    <td style="padding:15px">Update the NSX Manager's Search Domains</td>
    <td style="padding:15px">{base_path}/{version}/node/network/search-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodenetworksearchDomains(callback)</td>
    <td style="padding:15px">Read the NSX Manager's Search Domains</td>
    <td style="padding:15px">{base_path}/{version}/node/network/search-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesnodeMgmtactionrestart(callback)</td>
    <td style="padding:15px">Restart the node management service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/node-mgmt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessshactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the ssh service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessyslogexportersexporterName(exportername, callback)</td>
    <td style="padding:15px">Read node syslog exporter</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog/exporters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodeservicessyslogexportersexporterName(exportername, callback)</td>
    <td style="padding:15px">Delete node syslog exporter</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog/exporters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusternodesnodeIdnetworkinterfacesinterfaceId(nodeid, interfaceid, source, callback)</td>
    <td style="padding:15px">Read the node's Network Interface</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes/{pathv1}/network/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessnmpactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the SNMP service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessnmpactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the SNMP service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodefileStorefileNamethumbprint(filename, callback)</td>
    <td style="padding:15px">Read file thumbprint</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store/{pathv1}/thumbprint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesliagentstatus(callback)</td>
    <td style="padding:15px">Read liagent service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/liagent/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusterrestoreactionstart(InitiateClusterRestoreRequest, callback)</td>
    <td style="padding:15px">Initiate a restore operation</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesnsxMessageBusactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX Message Bus service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-message-bus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessnmpstatus(callback)</td>
    <td style="padding:15px">Read SNMP service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/snmp/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesupportBundle(all, callback)</td>
    <td style="padding:15px">Read node support bundle</td>
    <td style="padding:15px">{base_path}/{version}/node/support-bundle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessearch(callback)</td>
    <td style="padding:15px">Read NSX Search service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusterrestoreactioncancel(callback)</td>
    <td style="padding:15px">Cancel any running restore operation</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagementcsrscsrIdpemFile(csrid, callback)</td>
    <td style="padding:15px">Get CSR PEM File for the Given CSR ID</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/csrs/{pathv1}/pem-file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusterrestoreactionadvance(AdvanceClusterRestoreRequest, callback)</td>
    <td style="padding:15px">Advance any suspended restore operation</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessshstatus(callback)</td>
    <td style="padding:15px">Read ssh service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ssh/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesnsxMessageBusactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX Message Bus service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-message-bus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putClusterrestoreconfig(RestoreConfiguration, callback)</td>
    <td style="padding:15px">Configure Restore SFTP server credentials</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterrestoreconfig(callback)</td>
    <td style="padding:15px">Get Restore configuration</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicescmInventory(callback)</td>
    <td style="padding:15px">Read cm inventory service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/cm-inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusterrestoreactionsuspend(callback)</td>
    <td style="padding:15px">Suspend any running restore operation</td>
    <td style="padding:15px">{base_path}/{version}/cluster/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodetaskstaskIdactioncancel(taskid, callback)</td>
    <td style="padding:15px">Cancel specified task</td>
    <td style="padding:15px">{base_path}/{version}/node/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTrustManagementprincipalIdentitiesprincipalIdentityId(principalidentityid, callback)</td>
    <td style="padding:15px">Delete a principal identity</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/principal-identities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodefileStorefileName(filename, callback)</td>
    <td style="padding:15px">Upload a file to the file store</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodefileStorefileName(filename, callback)</td>
    <td style="padding:15px">Delete file</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodefileStorefileName(filename, callback)</td>
    <td style="padding:15px">Read file properties</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicescmInventoryactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the manager service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/cm-inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeusersuseridsshKeysactionaddSshKey(userid, SshKeyProperties, callback)</td>
    <td style="padding:15px">Add SSH public key to authorized_keys file for node user</td>
    <td style="padding:15px">{base_path}/{version}/node/users/{pathv1}/ssh-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessshactionremoveHostFingerprint(KnownHostParameter, callback)</td>
    <td style="padding:15px">Remove a host's fingerprint from known hosts file</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodetasks(fields, requestMethod, requestPath, requestUri, status, user, callback)</td>
    <td style="padding:15px">List appliance management tasks</td>
    <td style="padding:15px">{base_path}/{version}/node/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesmgmtPlaneBusactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the Rabbit MQ service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/mgmt-plane-bus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesnsxMessageBusstatus(callback)</td>
    <td style="padding:15px">Read NSX Message Bus service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-message-bus/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesnodeMgmtstatus(callback)</td>
    <td style="padding:15px">Read appliance management service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/node-mgmt/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClusternodesnodeId(nodeid, callback)</td>
    <td style="padding:15px">Remove a Node from the Cluster</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusternodesnodeId(nodeid, callback)</td>
    <td style="padding:15px">Read Cluster Node Configuration</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesntpactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the NTP service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessyslogactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the syslog service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeservicesmanager(NodeProtonServiceProperties, callback)</td>
    <td style="padding:15px">Update service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesmanager(callback)</td>
    <td style="padding:15px">Read service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeserviceshttpactionrestart(callback)</td>
    <td style="padding:15px">Restart the http service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/http?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeaaaprovidersvidmstatus(callback)</td>
    <td style="padding:15px">Read AAA provider vIDM status</td>
    <td style="padding:15px">{base_path}/{version}/node/aaa/providers/vidm/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesmanageractionresetManagerLoggingLevels(callback)</td>
    <td style="padding:15px">Reset the logging levels to default values</td>
    <td style="padding:15px">{base_path}/{version}/node/services/manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeusers(callback)</td>
    <td style="padding:15px">List node users</td>
    <td style="padding:15px">{base_path}/{version}/node/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusternodesstatus(callback)</td>
    <td style="padding:15px">Read Cluster Status</td>
    <td style="padding:15px">{base_path}/{version}/cluster/nodes/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesnsxUpgradeAgentstatus(callback)</td>
    <td style="padding:15px">Read Nsx upgrade agent service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-upgrade-agent/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodefileStorefileNameactioncopyFromRemoteFile(filename, CopyFromRemoteFileProperties, callback)</td>
    <td style="padding:15px">Copy a remote file to the file store</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesnodeMgmt(callback)</td>
    <td style="padding:15px">Read appliance management service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/node-mgmt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeservicesinstallUpgrade(NodeInstallUpgradeServiceProperties, callback)</td>
    <td style="padding:15px">Update NSX install-upgrade service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/install-upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesinstallUpgrade(callback)</td>
    <td style="padding:15px">Read NSX install-upgrade service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/install-upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeprocesses(callback)</td>
    <td style="padding:15px">List node processes</td>
    <td style="padding:15px">{base_path}/{version}/node/processes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodenetworkroutesrouteId(routeid, callback)</td>
    <td style="padding:15px">Delete node network route</td>
    <td style="padding:15px">{base_path}/{version}/node/network/routes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodenetworkroutesrouteId(routeid, callback)</td>
    <td style="padding:15px">Read node network route</td>
    <td style="padding:15px">{base_path}/{version}/node/network/routes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodefileStore(callback)</td>
    <td style="padding:15px">List node files</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTrustManagementcsrs(Csr, callback)</td>
    <td style="padding:15px">Generate a New Certificate Signing Request</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/csrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagementcsrs(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return All the Generated CSRs</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/csrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesnsxUpgradeAgent(callback)</td>
    <td style="padding:15px">Read NSX upgrade Agent service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-upgrade-agent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodehardeningPolicymandatoryAccessControlreport(callback)</td>
    <td style="padding:15px">Get the report for Mandatory Access Control</td>
    <td style="padding:15px">{base_path}/{version}/node/hardening-policy/mandatory-access-control/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesinstallUpgradestatus(callback)</td>
    <td style="padding:15px">Read NSX install-upgrade service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/install-upgrade/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCluster(callback)</td>
    <td style="padding:15px">Read Cluster Configuration</td>
    <td style="padding:15px">{base_path}/{version}/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodefileStoreactioncreateRemoteDirectory(CreateRemoteDirectoryProperties, callback)</td>
    <td style="padding:15px">Create directory in remote file server</td>
    <td style="padding:15px">{base_path}/{version}/node/file-store?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessyslogactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the syslog service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessyslog(callback)</td>
    <td style="padding:15px">Read syslog service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putClusterbackupsconfig(BackupConfiguration, callback)</td>
    <td style="padding:15px">Configure backup</td>
    <td style="padding:15px">{base_path}/{version}/cluster/backups/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterbackupsconfig(callback)</td>
    <td style="padding:15px">Get backup configuration</td>
    <td style="padding:15px">{base_path}/{version}/cluster/backups/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTrustManagementcrlscrlId(crlid, Crl, callback)</td>
    <td style="padding:15px">Update CRL for the Given CRL ID</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/crls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagementcrlscrlId(crlid, details, callback)</td>
    <td style="padding:15px">Show CRL Data for the Given CRL ID</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/crls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTrustManagementcrlscrlId(crlid, callback)</td>
    <td style="padding:15px">Delete a CRL</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/crls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTrustManagementcsrscsrIdactionselfSign(csrid, daysValid, callback)</td>
    <td style="padding:15px">Self-Sign the CSR</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/csrs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeusersuseridsshKeysactionremoveSshKey(userid, SshKeyBaseProperties, callback)</td>
    <td style="padding:15px">Remove SSH public key from authorized_keys file for node user</td>
    <td style="padding:15px">{base_path}/{version}/node/users/{pathv1}/ssh-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodenetworkinterfacesinterfaceIdstats(interfaceid, callback)</td>
    <td style="padding:15px">Read the NSX Manager's Network Interface Statistics</td>
    <td style="padding:15px">{base_path}/{version}/node/network/interfaces/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessshactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the ssh service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicessyslogstatus(callback)</td>
    <td style="padding:15px">Read syslog service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTrustManagementcertificatesactionimport(TrustObjectData, callback)</td>
    <td style="padding:15px">Add a New Certificate</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessyslogactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the syslog service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTrustManagementprincipalIdentities(PrincipalIdentity, callback)</td>
    <td style="padding:15px">Register a name-certificate combination.</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/principal-identities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagementprincipalIdentities(callback)</td>
    <td style="padding:15px">Return the list of principal identities</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/principal-identities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesmanageractionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagementcrls(cursor, details, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return All Added CRLs</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/crls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeaaaprovidersvidm(NodeAuthProviderVidmProperties, callback)</td>
    <td style="padding:15px">Update AAA provider vIDM properties</td>
    <td style="padding:15px">{base_path}/{version}/node/aaa/providers/vidm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeaaaprovidersvidm(callback)</td>
    <td style="padding:15px">Read AAA provider vIDM properties</td>
    <td style="padding:15px">{base_path}/{version}/node/aaa/providers/vidm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservices(callback)</td>
    <td style="padding:15px">List node services</td>
    <td style="padding:15px">{base_path}/{version}/node/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTrustManagementcsrscsrId(csrid, callback)</td>
    <td style="padding:15px">Delete a CSR</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/csrs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrustManagementcsrscsrId(csrid, callback)</td>
    <td style="padding:15px">Show CSR Data for the Given CSR ID</td>
    <td style="padding:15px">{base_path}/{version}/trust-management/csrs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeactionshutdown(callback)</td>
    <td style="padding:15px">Restart or shutdown node</td>
    <td style="padding:15px">{base_path}/{version}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeserviceshttpstatus(callback)</td>
    <td style="padding:15px">Read http service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/http/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodenetworkinterfaces(callback)</td>
    <td style="padding:15px">List the NSX Manager's Network Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/node/network/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeserviceshttpactionapplyCertificate(certificateId, callback)</td>
    <td style="padding:15px">Update http service certificate</td>
    <td style="padding:15px">{base_path}/{version}/node/services/http?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessearchactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX Search service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeusersuseridsshKeys(userid, callback)</td>
    <td style="padding:15px">List SSH keys from authorized_keys file for node user</td>
    <td style="padding:15px">{base_path}/{version}/node/users/{pathv1}/ssh-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessearchactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX Search service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicessearchactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX Search service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesinstallUpgradeactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX install-upgrade service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/install-upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesmanagerstatus(callback)</td>
    <td style="padding:15px">Read service status</td>
    <td style="padding:15px">{base_path}/{version}/node/services/manager/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesliagentactionrestart(callback)</td>
    <td style="padding:15px">Restart, start or stop the liagent service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/liagent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesnsxMessageBus(callback)</td>
    <td style="padding:15px">Read NSX Message Bus service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-message-bus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesinstallUpgradeactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX install-upgrade service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/install-upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesnsxUpgradeAgentactionstop(callback)</td>
    <td style="padding:15px">Restart, start or stop the NSX upgrade agent service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/nsx-upgrade-agent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodehardeningPolicymandatoryAccessControl(MandatoryAccessControlProperties, callback)</td>
    <td style="padding:15px">Enable or disable  Mandatory Access Control</td>
    <td style="padding:15px">{base_path}/{version}/node/hardening-policy/mandatory-access-control?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodehardeningPolicymandatoryAccessControl(callback)</td>
    <td style="padding:15px">Gets the enable status for Mandatory Access Control</td>
    <td style="padding:15px">{base_path}/{version}/node/hardening-policy/mandatory-access-control?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNode(NodeProperties, callback)</td>
    <td style="padding:15px">Update node properties</td>
    <td style="padding:15px">{base_path}/{version}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNode(callback)</td>
    <td style="padding:15px">Read node properties</td>
    <td style="padding:15px">{base_path}/{version}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeserviceshttp(NodeHttpServiceProperties, callback)</td>
    <td style="padding:15px">Update http service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/http?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeserviceshttp(callback)</td>
    <td style="padding:15px">Read http service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/http?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeprocessesprocessId(processid, callback)</td>
    <td style="padding:15px">Read node process</td>
    <td style="padding:15px">{base_path}/{version}/node/processes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodeservicesliagentactionstart(callback)</td>
    <td style="padding:15px">Restart, start or stop the liagent service</td>
    <td style="padding:15px">{base_path}/{version}/node/services/liagent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeservicesmgmtPlaneBus(callback)</td>
    <td style="padding:15px">Read Rabbit MQ service properties</td>
    <td style="padding:15px">{base_path}/{version}/node/services/mgmt-plane-bus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodenetworkroutes(NodeRouteProperties, callback)</td>
    <td style="padding:15px">Create node network route</td>
    <td style="padding:15px">{base_path}/{version}/node/network/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodenetworkroutes(callback)</td>
    <td style="padding:15px">List node network routes</td>
    <td style="padding:15px">{base_path}/{version}/node/network/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradenodesSummary(callback)</td>
    <td style="padding:15px">Get summary of nodes</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/nodes-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeupgradeUnitGroups(UpgradeUnitGroup, callback)</td>
    <td style="padding:15px">Create a group</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnitGroups(componentType, cursor, includedFields, pageSize, sortAscending, sortBy, summary, sync, callback)</td>
    <td style="padding:15px">Return information of all upgrade unit groups</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpgradeupgradeUnitGroupsgroupId(groupid, UpgradeUnitGroup, callback)</td>
    <td style="padding:15px">Update the upgrade unit group</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUpgradeupgradeUnitGroupsgroupId(groupid, callback)</td>
    <td style="padding:15px">Delete the upgrade unit group</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnitGroupsgroupId(groupid, summary, callback)</td>
    <td style="padding:15px">Return upgrade unit group information</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeversionWhitelist(callback)</td>
    <td style="padding:15px">Get the version whitelist</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/version-whitelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnitsStats(cursor, includedFields, pageSize, sortAscending, sortBy, sync, callback)</td>
    <td style="padding:15px">Get upgrade units stats</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-units-stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeplanactionstart(callback)</td>
    <td style="padding:15px">Start upgrade</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/plan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpgradeversionWhitelistcomponentType(componentType, VersionList, callback)</td>
    <td style="padding:15px">Update the version whitelist for the specified component type</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/version-whitelist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeversionWhitelistcomponentType(componentType, callback)</td>
    <td style="padding:15px">Get the version whitelist for the specified component</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/version-whitelist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeplanactionpause(callback)</td>
    <td style="padding:15px">Pause upgrade</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/plan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder(groupid, upgradeunitid, ReorderRequest, callback)</td>
    <td style="padding:15px">Reorder an upgrade unit within the upgrade unit group</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups/{pathv1}/upgrade-unit/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnitGroupsaggregateInfo(componentType, cursor, includedFields, pageSize, sortAscending, sortBy, summary, sync, callback)</td>
    <td style="padding:15px">Return aggregate information of all upgrade unit groups</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups/aggregate-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnitsupgradeUnitId(upgradeunitid, callback)</td>
    <td style="padding:15px">Get a specific upgrade unit</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-units/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeupgradeUnitGroupsgroupIdactionreorder(groupid, ReorderRequest, callback)</td>
    <td style="padding:15px">Reorder upgrade unit group</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeplanactioncontinue(skip, callback)</td>
    <td style="padding:15px">Continue upgrade</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/plan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnitsaggregateInfo(componentType, cursor, groupId, hasErrors, includedFields, metadata, pageSize, selectionStatus, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get upgrade units aggregate-info</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-units/aggregate-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeupgradeUnitGroupsgroupIdactionaddUpgradeUnits(groupid, UpgradeUnitList, callback)</td>
    <td style="padding:15px">Add upgrade units to specified upgrade unit group</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnits(componentType, currentVersion, cursor, groupId, hasWarnings, includedFields, metadata, pageSize, sortAscending, sortBy, upgradeUnitType, callback)</td>
    <td style="padding:15px">Get upgrade units</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-units?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradestatusSummary(componentType, selectionStatus, callback)</td>
    <td style="padding:15px">Get upgrade status summary</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/status-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnitGroupsgroupIdstatus(groupid, cursor, hasErrors, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get upgrade status for group</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeplanactionupgradeSelectedUnits(UpgradeUnitList, callback)</td>
    <td style="padding:15px">Upgrade selected units</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/plan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeupgradeUnitGroupsStatus(componentType, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get upgrade status for upgrade unit groups</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/upgrade-unit-groups-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradesummary(callback)</td>
    <td style="padding:15px">Get upgrade summary</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradenodes(componentType, componentVersion, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get list of nodes across all types</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeactionupgradeUc(callback)</td>
    <td style="padding:15px">Upgrade the upgrade coordinator.</td>
    <td style="padding:15px">{base_path}/{version}/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradehistory(callback)</td>
    <td style="padding:15px">Get upgrade history</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpgradeplancomponentTypesettings(componentType, UpgradePlanSettings, callback)</td>
    <td style="padding:15px">Update upgrade plan settings for the component</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/plan/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeplancomponentTypesettings(componentType, callback)</td>
    <td style="padding:15px">Get upgrade plan settings for the component</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/plan/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeplanactionreset(componentType, callback)</td>
    <td style="padding:15px">Reset upgrade plan to default plan</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/plan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpserversserverIdipPools(serverid, DhcpIpPool, callback)</td>
    <td style="padding:15px">Create an IP pool for a DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/ip-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserversserverIdipPools(serverid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get a paginated list of a DHCP server's IP pools</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/ip-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpserverProfiles(DhcpProfile, callback)</td>
    <td style="padding:15px">Create a DHCP server profile</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/server-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserverProfiles(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get a paginated list of DHCP server profiles</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/server-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionkeyPolicieskeyPolicyIdstats(keypolicyid, source, callback)</td>
    <td style="padding:15px">Get the statistics for a network encryption key policy</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancerapplicationProfilesapplicationProfileId(applicationprofileid, LbAppProfile, callback)</td>
    <td style="padding:15px">Update a load balancer application profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/application-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancerapplicationProfilesapplicationProfileId(applicationprofileid, callback)</td>
    <td style="padding:15px">Delete a load balancer application profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/application-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerapplicationProfilesapplicationProfileId(applicationprofileid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer application profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/application-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsections(FirewallSection, id, operation, callback)</td>
    <td style="padding:15px">Create a New Empty Section</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallsections(appliedTos, cursor, destinations, excludeAppliedToType, filterType, includeAppliedToType, includedFields, pageSize, services, sortAscending, sortBy, sources, type, callback)</td>
    <td style="padding:15px">List All Firewall Sections</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancerserverSslProfiles(LbServerSslProfile, callback)</td>
    <td style="padding:15px">Create a load balancer server-ssl profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/server-ssl-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerserverSslProfiles(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer server-ssl profiles</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/server-ssl-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancerservicesserviceId(serviceid, LbService, callback)</td>
    <td style="padding:15px">Update a load balancer service</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancerservicesserviceId(serviceid, callback)</td>
    <td style="padding:15px">Delete a load balancer service</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceId(serviceid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer service</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancerrulesruleId(ruleid, LbRule, callback)</td>
    <td style="padding:15px">Update a load balancer rule</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerrulesruleId(ruleid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer rule</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancerrulesruleId(ruleid, callback)</td>
    <td style="padding:15px">Delete a load balancer rule</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserversserverIdstatus(serverid, callback)</td>
    <td style="padding:15px">Get DHCP service status with given dhcp server id</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpservers(LogicalDhcpServer, callback)</td>
    <td style="padding:15px">Create a DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpservers(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get a paginated list of DHCP servers</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcpserversserverIdipPoolspoolId(serverid, poolid, DhcpIpPool, callback)</td>
    <td style="padding:15px">Update a DHCP server's IP pool</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/ip-pools/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserversserverIdipPoolspoolId(serverid, poolid, callback)</td>
    <td style="padding:15px">Get a DHCP server's IP pool with the specified pool ID</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/ip-pools/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpserversserverIdipPoolspoolId(serverid, poolid, callback)</td>
    <td style="padding:15px">Delete a DHCP server's IP pool</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/ip-pools/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsectionssectionIdactionreviseWithRules(sectionid, FirewallSectionRuleList, id, operation, callback)</td>
    <td style="padding:15px">Update an Existing Section with Rules</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpserversserverIdstaticBindings(serverid, DhcpStaticBinding, callback)</td>
    <td style="padding:15px">Create a static binding for a DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/static-bindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserversserverIdstaticBindings(serverid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get a paginated list of a DHCP server's static bindings</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/static-bindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsectionssectionIdrules(sectionid, DneRule, id, operation, callback)</td>
    <td style="padding:15px">Add a network encryption rule in a section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionsectionssectionIdrules(sectionid, appliedTos, cursor, destinations, filterType, includedFields, keyPolicyId, pageSize, services, sortAscending, sortBy, sources, callback)</td>
    <td style="padding:15px">Get the rules of a network encryption section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFirewallstatuscontextType(contexttype, FirewallStatus, callback)</td>
    <td style="padding:15px">Update global firewall status for dfw context</td>
    <td style="padding:15px">{base_path}/{version}/firewall/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallstatuscontextType(contexttype, callback)</td>
    <td style="padding:15px">Get firewall global status for dfw context</td>
    <td style="padding:15px">{base_path}/{version}/firewall/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsectionssectionIdactionreviseWithRules(sectionid, DneSectionRuleList, id, operation, callback)</td>
    <td style="padding:15px">Update a network encryption section, its rules or position</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancerpoolspoolId(poolid, LbPool, callback)</td>
    <td style="padding:15px">Update a load balancer pool</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancerpoolspoolId(poolid, PoolMemberSettingList, action, callback)</td>
    <td style="padding:15px">Add, remove, or modify load balancer pool members</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancerpoolspoolId(poolid, callback)</td>
    <td style="padding:15px">Delete a load balancer pool</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerpoolspoolId(poolid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer pool</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsectionssectionIdactionupdateWithRules(sectionid, FirewallSectionRuleList, callback)</td>
    <td style="padding:15px">Update an Existing Section, Including Its Rules</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsectionssectionIdactionlistWithRules(sectionid, callback)</td>
    <td style="padding:15px">Get an Existing Section, Including Rules</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdpoolspoolIdstatus(serviceid, poolid, source, callback)</td>
    <td style="padding:15px">Get the status of load balancer pool</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/pools/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionstatus(callback)</td>
    <td style="padding:15px">Get the network encryption status for a given context</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcpserversserverId(serverid, LogicalDhcpServer, callback)</td>
    <td style="padding:15px">Update a DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpserversserverId(serverid, callback)</td>
    <td style="padding:15px">Delete a DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserversserverId(serverid, callback)</td>
    <td style="padding:15px">Get a DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsectionssectionIdrulesactioncreateMultiple(sectionid, FirewallRuleList, id, operation, callback)</td>
    <td style="padding:15px">Add Multiple Rules in a Section</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkEncryptionkeyPolicieskeyPolicyId(keypolicyid, DneKeyPolicy, callback)</td>
    <td style="padding:15px">Update a network encryption key policy</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkEncryptionkeyPolicieskeyPolicyId(keypolicyid, callback)</td>
    <td style="padding:15px">Delete a specific network encryption key policy</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionkeyPolicieskeyPolicyId(keypolicyid, callback)</td>
    <td style="padding:15px">Get a specific network encryption key policy</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsectionsactioncreateWithRules(DneSectionRuleList, id, operation, callback)</td>
    <td style="padding:15px">Create a network encryption section with rules</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancerrules(LbRule, callback)</td>
    <td style="padding:15px">Create a load balancer rule</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerrules(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer rules</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkEncryptionsectionssectionId(sectionid, DneSection, callback)</td>
    <td style="padding:15px">Update a network encryption section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionsectionssectionId(sectionid, callback)</td>
    <td style="padding:15px">Get a specific network encryption section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkEncryptionsectionssectionId(sectionid, cascade, id, operation, callback)</td>
    <td style="padding:15px">Delete a network encryption section and its rules</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserversserverIdstatistics(serverid, callback)</td>
    <td style="padding:15px">Get DHCP statistics with given dhcp server id</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancerpools(LbPool, callback)</td>
    <td style="padding:15px">Create a load balancer pool</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerpools(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer pools</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsections(DneSection, id, operation, callback)</td>
    <td style="padding:15px">Create a network encryption section without rules</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionsections(appliedTos, cursor, destinations, filterType, includedFields, keyPolicyId, pageSize, services, sortAscending, sortBy, sources, type, callback)</td>
    <td style="padding:15px">Get network encryption sections</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallexcludelistactionremoveMember(objectId, callback)</td>
    <td style="padding:15px">Remove an existing object from the exclude list</td>
    <td style="padding:15px">{base_path}/{version}/firewall/excludelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancerpersistenceProfiles(LbPersistenceProfile, callback)</td>
    <td style="padding:15px">Create a load balancer persistence profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/persistence-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerpersistenceProfiles(cursor, includedFields, pageSize, sortAscending, sortBy, type, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer persistence profiles</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/persistence-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancerserverSslProfilesserverSslProfileId(serversslprofileid, LbServerSslProfile, callback)</td>
    <td style="padding:15px">Update a load balancer server-ssl profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/server-ssl-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancerserverSslProfilesserverSslProfileId(serversslprofileid, callback)</td>
    <td style="padding:15px">Delete a load balancer server-ssl profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/server-ssl-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerserverSslProfilesserverSslProfileId(serversslprofileid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer server-ssl profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/server-ssl-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFirewallexcludelist(ExcludeList, callback)</td>
    <td style="padding:15px">Modify exclude list</td>
    <td style="padding:15px">{base_path}/{version}/firewall/excludelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallexcludelist(callback)</td>
    <td style="padding:15px">Get list of entities in exclude list</td>
    <td style="padding:15px">{base_path}/{version}/firewall/excludelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsectionssectionIdactionrevise(sectionid, DneSection, id, operation, callback)</td>
    <td style="padding:15px">Update a network encryption section and/or its position</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsectionsactioncreateWithRules(FirewallSectionRuleList, id, operation, callback)</td>
    <td style="padding:15px">Create a Section with Rules</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise(sectionid, ruleid, DneRule, id, operation, callback)</td>
    <td style="padding:15px">Update or reorder a rule in a network encryption section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMdProxiesproxyId(proxyid, MetadataProxy, callback)</td>
    <td style="padding:15px">Update a metadata proxy</td>
    <td style="padding:15px">{base_path}/{version}/md-proxies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMdProxiesproxyId(proxyid, callback)</td>
    <td style="padding:15px">Get a metadata proxy</td>
    <td style="padding:15px">{base_path}/{version}/md-proxies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMdProxiesproxyId(proxyid, callback)</td>
    <td style="padding:15px">Delete a metadata proxy</td>
    <td style="padding:15px">{base_path}/{version}/md-proxies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionkeyPolicies(DneKeyPolicy, callback)</td>
    <td style="padding:15px">Create a network encryption key policy</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionkeyPolicies(cursor, encryptAlgorithm, encryptType, includedFields, macAlgorithm, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get all network encryption key policies</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcpserversserverIdstaticBindingsbindingId(serverid, bindingid, DhcpStaticBinding, callback)</td>
    <td style="padding:15px">Update a DHCP server's static binding</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/static-bindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpserversserverIdstaticBindingsbindingId(serverid, bindingid, callback)</td>
    <td style="padding:15px">Delete a static binding</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/static-bindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserversserverIdstaticBindingsbindingId(serverid, bindingid, callback)</td>
    <td style="padding:15px">Get a DHCP server's static binding with the specified binding ID</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/static-bindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkEncryptionkeyManagerskeyManagerId(keymanagerid, DneKeyManager, callback)</td>
    <td style="padding:15px">Update a specific network encryption key manager configuration</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-managers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionkeyManagerskeyManagerId(keymanagerid, callback)</td>
    <td style="padding:15px">Get configuration for a specific network encryption key manager</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-managers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkEncryptionkeyManagerskeyManagerId(keymanagerid, callback)</td>
    <td style="padding:15px">Delete an attached network encryption key manager</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-managers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionkeyManagers(DneKeyManager, callback)</td>
    <td style="padding:15px">Add a network encryption key manager configuration</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-managers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionkeyManagers(callback)</td>
    <td style="padding:15px">Get all configured instances of network encryption key managers</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-managers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdvirtualServersstatus(serviceid, source, callback)</td>
    <td style="padding:15px">Get the status list of virtual servers in given load balancer service</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/virtual-servers/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcpserverProfilesprofileId(profileid, DhcpProfile, callback)</td>
    <td style="padding:15px">Update a DHCP server profile</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/server-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserverProfilesprofileId(profileid, callback)</td>
    <td style="padding:15px">Get a DHCP server profile</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/server-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpserverProfilesprofileId(profileid, callback)</td>
    <td style="padding:15px">Delete a DHCP server profile</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/server-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsectionssectionIdactionlistWithRules(sectionid, callback)</td>
    <td style="padding:15px">Get a specific network encryption section and its rules</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancersslciphersAndProtocols(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Retrieve a list of supported SSL ciphers and protocols</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/ssl/ciphers-and-protocols?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsectionssectionIdactionrevise(sectionid, FirewallSection, id, operation, callback)</td>
    <td style="padding:15px">Update an Existing Section, Including Its Position</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallexcludelistactionaddMember(ResourceReference, callback)</td>
    <td style="padding:15px">Add a new object in the exclude list</td>
    <td style="padding:15px">{base_path}/{version}/firewall/excludelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallsectionssectionIdrulesstats(sectionid, source, callback)</td>
    <td style="padding:15px">Get Firewall section level statistics section</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancerapplicationProfiles(LbAppProfile, callback)</td>
    <td style="padding:15px">Create a load balancer application profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/application-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerapplicationProfiles(cursor, includedFields, pageSize, sortAscending, sortBy, type, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer application profiles</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/application-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancermonitorsmonitorId(monitorid, LbMonitor, callback)</td>
    <td style="padding:15px">Update a load balancer monitor</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/monitors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancermonitorsmonitorId(monitorid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer monitor</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/monitors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancermonitorsmonitorId(monitorid, callback)</td>
    <td style="padding:15px">Delete a load balancer monitor</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/monitors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionstatusactionupdateStatus(context, status, callback)</td>
    <td style="padding:15px">Update the firewall status for a given context</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancerclientSslProfilesclientSslProfileId(clientsslprofileid, LbClientSslProfile, callback)</td>
    <td style="padding:15px">Update a load balancer client-ssl profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/client-ssl-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerclientSslProfilesclientSslProfileId(clientsslprofileid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer client-ssl profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/client-ssl-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancerclientSslProfilesclientSslProfileId(clientsslprofileid, callback)</td>
    <td style="padding:15px">Delete a load balancer client-ssl profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/client-ssl-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkEncryptionsectionssectionIdrulesruleId(sectionid, ruleid, DneRule, callback)</td>
    <td style="padding:15px">Update a specific rule in a network encryption section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkEncryptionsectionssectionIdrulesruleId(sectionid, ruleid, callback)</td>
    <td style="padding:15px">Delete a specific network encryption rule</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionsectionssectionIdrulesruleId(sectionid, ruleid, callback)</td>
    <td style="padding:15px">Get a specific rule in a network encryption section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdpoolsstatistics(serviceid, source, callback)</td>
    <td style="padding:15px">Get the statistics list of load balancer pools</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/pools/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatus(serviceid, virtualserverid, source, callback)</td>
    <td style="padding:15px">Get the status of the load balancer virtual server</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/virtual-servers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatistics(serviceid, virtualserverid, source, callback)</td>
    <td style="padding:15px">Get the statistics of the given load balancer virtual server</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/virtual-servers/{pathv2}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsectionssectionIdrulesruleIdactionrevise(sectionid, ruleid, FirewallRule, id, operation, callback)</td>
    <td style="padding:15px">Update an Existing Rule and Reorder the Rule</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionsectionssectionIdstate(sectionid, barrierId, callback)</td>
    <td style="padding:15px">Get the realized state of a network encryption section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMdProxies(MetadataProxy, callback)</td>
    <td style="padding:15px">Create a metadata proxy</td>
    <td style="padding:15px">{base_path}/{version}/md-proxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMdProxies(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get a paginated list of metadata proxies</td>
    <td style="padding:15px">{base_path}/{version}/md-proxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallsectionssectionIdrules(sectionid, FirewallRule, id, operation, callback)</td>
    <td style="padding:15px">Add a Single Rule in a Section</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallsectionssectionIdrules(sectionid, appliedTos, cursor, destinations, filterType, includedFields, pageSize, services, sortAscending, sortBy, sources, callback)</td>
    <td style="padding:15px">Get All the Rules for a Section</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdvirtualServersstatistics(serviceid, source, callback)</td>
    <td style="padding:15px">Get the statistics list of virtual servers</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/virtual-servers/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionkeyPolicieskeyPolicyIdactionrotate(keypolicyid, callback)</td>
    <td style="padding:15px">Manually rotate a network encryption key policy for graceful re-key</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFirewallsectionssectionId(sectionid, FirewallSection, callback)</td>
    <td style="padding:15px">Update an Existing Section</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallsectionssectionId(sectionid, callback)</td>
    <td style="padding:15px">Get an Existing Section</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallsectionssectionId(sectionid, cascade, callback)</td>
    <td style="padding:15px">Delete an Existing Section and Its Associated Rules</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFirewallsectionssectionIdrulesruleId(sectionid, ruleid, FirewallRule, callback)</td>
    <td style="padding:15px">Update an Existing Rule</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallsectionssectionIdrulesruleId(sectionid, ruleid, callback)</td>
    <td style="padding:15px">Delete an Existing Rule</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallsectionssectionIdrulesruleId(sectionid, ruleid, callback)</td>
    <td style="padding:15px">Read an Existing Rule</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMdProxiesproxyIdlogicalSwitchIdstatus(proxyid, logicalswitchid, callback)</td>
    <td style="padding:15px">Get Metadata Proxy status with given proxy id and attached logical switch.</td>
    <td style="padding:15px">{base_path}/{version}/md-proxies/{pathv1}/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallsectionssectionIdstate(sectionid, barrierId, callback)</td>
    <td style="padding:15px">Get the Realized State of a Firewall Section</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMdProxiesproxyIdstatistics(proxyid, logicalSwitchId, source, callback)</td>
    <td style="padding:15px">Get Metadata Proxy statistics with given proxy id</td>
    <td style="padding:15px">{base_path}/{version}/md-proxies/{pathv1}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdstatistics(serviceid, source, callback)</td>
    <td style="padding:15px">Get the statistics of load balancer service</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkEncryptionkeyPoliciesresetencryptType(encrypttype, callback)</td>
    <td style="padding:15px">Reset default network encryption key policy to system default for a specific encryption type</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies/reset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdpoolsstatus(serviceid, source, callback)</td>
    <td style="padding:15px">Get the status list of load balancer pools</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/pools/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallsectionssectionIdrulesruleIdstats(sectionid, ruleid, source, callback)</td>
    <td style="padding:15px">Get Firewall rule level statistics</td>
    <td style="padding:15px">{base_path}/{version}/firewall/sections/{pathv1}/rules/{pathv2}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallrulesruleIdstate(ruleid, barrierId, callback)</td>
    <td style="padding:15px">Get the Realized State of a Firewall Rule</td>
    <td style="padding:15px">{base_path}/{version}/firewall/rules/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallexcludelistactioncheckIfExists(objectId, callback)</td>
    <td style="padding:15px">Check if the object a member of the exclude list</td>
    <td style="padding:15px">{base_path}/{version}/firewall/excludelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionkeyPolicieskeyPolicyIdactionrevoke(keypolicyid, callback)</td>
    <td style="padding:15px">Manually revoke a network encryption key policy for ungraceful re-key</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancerclientSslProfiles(LbClientSslProfile, callback)</td>
    <td style="padding:15px">Create a load balancer client-ssl profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/client-ssl-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerclientSslProfiles(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer client-ssl profiles</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/client-ssl-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirewallstatsactionreset(category, callback)</td>
    <td style="padding:15px">Reset firewall rule statistics</td>
    <td style="padding:15px">{base_path}/{version}/firewall/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple(sectionid, DneRuleList, id, operation, callback)</td>
    <td style="padding:15px">Add multiple rules to a network encryption section</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpserversserverIdleases(serverid, address, poolId, source, callback)</td>
    <td style="padding:15px">Get lease info of a given dhcp server id</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/servers/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancermonitors(LbMonitor, callback)</td>
    <td style="padding:15px">Create a load balancer monitor</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/monitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancermonitors(cursor, includedFields, pageSize, sortAscending, sortBy, type, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer monitors</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/monitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancerservices(LbService, callback)</td>
    <td style="padding:15px">Create a load balancer service</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservices(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer services</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetworkEncryptionconfig(DneGlobalConfig, callback)</td>
    <td style="padding:15px">Update the global configuration of network encryption service</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionconfig(callback)</td>
    <td style="padding:15px">Get the global configuration of network encryption service</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionsectionssectionIdrulesruleIdstate(sectionid, ruleid, barrierId, callback)</td>
    <td style="padding:15px">Get the realized State of a network encryption rule</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules/{pathv2}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdstatus(serviceid, source, callback)</td>
    <td style="padding:15px">Get the status of the given load balancer service</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallstatus(callback)</td>
    <td style="padding:15px">List all firewall status for supported contexts</td>
    <td style="padding:15px">{base_path}/{version}/firewall/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancervirtualServersvirtualServerId(virtualserverid, LbVirtualServer, callback)</td>
    <td style="padding:15px">Update a load balancer virtual server</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/virtual-servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancervirtualServersvirtualServerId(virtualserverid, deleteAssociatedRules, callback)</td>
    <td style="padding:15px">Delete a load balancer virtual server</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/virtual-servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancervirtualServersvirtualServerId(virtualserverid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer virtual server</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/virtual-servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkEncryptionsectionssectionIdactionupdateWithRules(sectionid, DneSectionRuleList, callback)</td>
    <td style="padding:15px">Update a network encryption section and its rules</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionsectionssectionIdrulesruleIdstats(sectionid, ruleid, source, callback)</td>
    <td style="padding:15px">Get the statistics for a network encryption rule</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/sections/{pathv1}/rules/{pathv2}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLoadbalancerpersistenceProfilespersistenceProfileId(persistenceprofileid, LbPersistenceProfile, callback)</td>
    <td style="padding:15px">Update a load balancer persistence profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/persistence-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerpersistenceProfilespersistenceProfileId(persistenceprofileid, callback)</td>
    <td style="padding:15px">Retrieve a load balancer persistence profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/persistence-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoadbalancerpersistenceProfilespersistenceProfileId(persistenceprofileid, callback)</td>
    <td style="padding:15px">Delete a load balancer persistence profile</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/persistence-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEncryptionkeyPolicieskeypolicyIdrules(keypolicyid, appliedTos, cursor, destinations, filterType, includedFields, keyPolicyId, pageSize, services, sortAscending, sortBy, sources, callback)</td>
    <td style="padding:15px">Get all network encryption rules attached to a specific key policy</td>
    <td style="padding:15px">{base_path}/{version}/network-encryption/key-policies/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancerservicesserviceIdpoolspoolIdstatistics(serviceid, poolid, source, callback)</td>
    <td style="padding:15px">Get the statistics of load balancer pool</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/services/{pathv1}/pools/{pathv2}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoadbalancervirtualServers(LbVirtualServer, callback)</td>
    <td style="padding:15px">Create a load balancer virtual server</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/virtual-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadbalancervirtualServers(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Retrieve a paginated list of load balancer virtual servers</td>
    <td style="padding:15px">{base_path}/{version}/loadbalancer/virtual-servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEulacontent(cursor, format, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return the content of end user license agreement
</td>
    <td style="padding:15px">{base_path}/{version}/eula/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLicense(License, callback)</td>
    <td style="padding:15px">Deprecated. Assign an Updated Enterprise License Key
</td>
    <td style="padding:15px">{base_path}/{version}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicense(callback)</td>
    <td style="padding:15px">Deprecated. Return the Enterprise License
</td>
    <td style="padding:15px">{base_path}/{version}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEulaacceptance(callback)</td>
    <td style="padding:15px">Return the acceptance status of end user license agreement
</td>
    <td style="padding:15px">{base_path}/{version}/eula/acceptance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpgradeeulaaccept(callback)</td>
    <td style="padding:15px">Accept end user license agreement
</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/eula/accept?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseslicensesUsage(callback)</td>
    <td style="padding:15px">Get usage report of all registered modules</td>
    <td style="padding:15px">{base_path}/{version}/licenses/licenses-usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseslicenseKey(licensekey, callback)</td>
    <td style="padding:15px">Deprecated. Get license properties for license identified by the license-key</td>
    <td style="padding:15px">{base_path}/{version}/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLicenseslicenseKey(licensekey, callback)</td>
    <td style="padding:15px">Deprecated. Remove a license identified by the license-key</td>
    <td style="padding:15px">{base_path}/{version}/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEulaaccept(callback)</td>
    <td style="padding:15px">Accept end user license agreement
</td>
    <td style="padding:15px">{base_path}/{version}/eula/accept?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseslicensesUsageformatcsv(callback)</td>
    <td style="padding:15px">Get usage report of all registred modules in CSV format</td>
    <td style="padding:15px">{base_path}/{version}/licenses/licenses-usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeeulacontent(cursor, format, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return the content of end user license agreement
</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/eula/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLicensesactiondelete(License, callback)</td>
    <td style="padding:15px">Remove a license</td>
    <td style="padding:15px">{base_path}/{version}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLicenses(License, callback)</td>
    <td style="padding:15px">Add a new license key</td>
    <td style="padding:15px">{base_path}/{version}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenses(callback)</td>
    <td style="padding:15px">Get all licenses</td>
    <td style="padding:15px">{base_path}/{version}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradeeulaacceptance(callback)</td>
    <td style="padding:15px">Return the acceptance status of end user license agreement
</td>
    <td style="padding:15px">{base_path}/{version}/upgrade/eula/acceptance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservicesnsserviceName(enforcementpointname, nsservicename, callback)</td>
    <td style="padding:15px">Read NSService</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/services/nsservices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxt(enforcementpointname, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List IPSets</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/ip-sets/ip-sets-nsxt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradeploymentZonesdeploymentZoneIdenforcementPoints(deploymentzoneid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List enforcementpoints for infra</td>
    <td style="padding:15px">{base_path}/{version}/infra/deployment-zones/{pathv1}/enforcement-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSectionsfirewallSectionId(enforcementpointname, firewallsectionid, callback)</td>
    <td style="padding:15px">Read Firewall</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/firewalls/firewall-sections/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroups(enforcementpointname, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List NS Groups</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/groups/nsgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfraservicesserviceIdserviceEntriesserviceEntryId(serviceid, serviceentryid, ServiceEntry, callback)</td>
    <td style="padding:15px">Create or update a ServiceEntry</td>
    <td style="padding:15px">{base_path}/{version}/infra/services/{pathv1}/service-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfraservicesserviceIdserviceEntriesserviceEntryId(serviceid, serviceentryid, callback)</td>
    <td style="padding:15px">Delete Service entry</td>
    <td style="padding:15px">{base_path}/{version}/infra/services/{pathv1}/service-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfraservicesserviceIdserviceEntriesserviceEntryId(serviceid, serviceentryid, callback)</td>
    <td style="padding:15px">Service entry</td>
    <td style="padding:15px">{base_path}/{version}/infra/services/{pathv1}/service-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroups(enforcementpointname, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Security Groups</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/groups/securitygroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplates(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Policy Templates</td>
    <td style="padding:15px">{base_path}/{version}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId(domainid, domaindeploymentmapid, DomainDeploymentMap, callback)</td>
    <td style="padding:15px">Create a new Domain Deployment Map under infra</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/domain-deployment-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId(domainid, domaindeploymentmapid, callback)</td>
    <td style="padding:15px">Read a DomainDeploymentMap</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/domain-deployment-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId(domainid, domaindeploymentmapid, callback)</td>
    <td style="padding:15px">Delete Domain Deployment Map</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/domain-deployment-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradeploymentZones(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Deployment Zones for infra</td>
    <td style="padding:15px">{base_path}/{version}/infra/deployment-zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomainsdomainIdcommunicationMapcommunicationEntries(domainid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List CommunicationEntries</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/communication-map/communication-entries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfradomainsdomainIdcommunicationMapactionrevise(domainid, CommunicationMap, anchorPath, operation, callback)</td>
    <td style="padding:15px">Revise the positioninng of communication maps</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/communication-map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxtmacSetName(enforcementpointname, macsetname, callback)</td>
    <td style="padding:15px">Read MACSet Realized state</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/mac-sets/mac-sets-nsxt/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroupssecuritygroupName(enforcementpointname, securitygroupname, callback)</td>
    <td style="padding:15px">Read Group</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/groups/securitygroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfraservicesserviceId(serviceid, Service, callback)</td>
    <td style="padding:15px">Create or update a Service</td>
    <td style="padding:15px">{base_path}/{version}/infra/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfraservicesserviceId(serviceid, callback)</td>
    <td style="padding:15px">Read a service</td>
    <td style="padding:15px">{base_path}/{version}/infra/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfraservicesserviceId(serviceid, callback)</td>
    <td style="padding:15px">Delete Service</td>
    <td style="padding:15px">{base_path}/{version}/infra/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId(deploymentzoneid, enforcementpointid, EnforcementPoint, callback)</td>
    <td style="padding:15px">Create/update a new Enforcement Point under infra</td>
    <td style="padding:15px">{base_path}/{version}/infra/deployment-zones/{pathv1}/enforcement-points/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId(deploymentzoneid, enforcementpointid, callback)</td>
    <td style="padding:15px">Read an Enforcement Point</td>
    <td style="padding:15px">{base_path}/{version}/infra/deployment-zones/{pathv1}/enforcement-points/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId(deploymentzoneid, enforcementpointid, callback)</td>
    <td style="padding:15px">Delete EnforcementPoint</td>
    <td style="padding:15px">{base_path}/{version}/infra/deployment-zones/{pathv1}/enforcement-points/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfradomainsdomainIdgroupsgroupId(domainid, groupid, Group, callback)</td>
    <td style="padding:15px">Create or update a group</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfradomainsdomainIdgroupsgroupId(domainid, groupid, force, callback)</td>
    <td style="padding:15px">Delete Group</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomainsdomainIdgroupsgroupId(domainid, groupid, callback)</td>
    <td style="padding:15px">Read group</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfracommunicationProfiles(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List CommunicationProfiles</td>
    <td style="padding:15px">{base_path}/{version}/infra/communication-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxtipSetName(enforcementpointname, ipsetname, callback)</td>
    <td style="padding:15px">Read IPSet Realized state</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/ip-sets/ip-sets-nsxt/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntries(communicationprofileid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List CommunicationProfileEntries for CommunicationProfile</td>
    <td style="padding:15px">{base_path}/{version}/infra/communication-profiles/{pathv1}/communication-profile-entries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfra(Infra, callback)</td>
    <td style="padding:15px">Update the infra including all the nested entities</td>
    <td style="padding:15px">{base_path}/{version}/infra?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfra(callback)</td>
    <td style="padding:15px">Read infra</td>
    <td style="padding:15px">{base_path}/{version}/infra?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfracommunicationProfilescommunicationProfileId(communicationprofileid, CommunicationProfile, callback)</td>
    <td style="padding:15px">Create or update a CommunicationProfile</td>
    <td style="padding:15px">{base_path}/{version}/infra/communication-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfracommunicationProfilescommunicationProfileId(communicationprofileid, callback)</td>
    <td style="padding:15px">Read CommunicationProfile</td>
    <td style="padding:15px">{base_path}/{version}/infra/communication-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfracommunicationProfilescommunicationProfileId(communicationprofileid, callback)</td>
    <td style="padding:15px">Delete CommunicationProfile</td>
    <td style="padding:15px">{base_path}/{version}/infra/communication-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId(communicationprofileid, communicationprofileentryid, CommunicationProfileEntry, callback)</td>
    <td style="padding:15px">Create or update a CommunicationProfileEntry</td>
    <td style="padding:15px">{base_path}/{version}/infra/communication-profiles/{pathv1}/communication-profile-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId(communicationprofileid, communicationprofileentryid, callback)</td>
    <td style="padding:15px">Delete CommunicationProfileEntry</td>
    <td style="padding:15px">{base_path}/{version}/infra/communication-profiles/{pathv1}/communication-profile-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId(communicationprofileid, communicationprofileentryid, callback)</td>
    <td style="padding:15px">Read CommunicationProfileEntry</td>
    <td style="padding:15px">{base_path}/{version}/infra/communication-profiles/{pathv1}/communication-profile-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservices(enforcementpointname, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Realized NSServices</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/services/nsservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTemplatestemplateId(templateid, PolicyTemplate, callback)</td>
    <td style="padding:15px">Create or update a template.</td>
    <td style="padding:15px">{base_path}/{version}/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplatestemplateId(templateid, callback)</td>
    <td style="padding:15px">Read template.</td>
    <td style="padding:15px">{base_path}/{version}/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplatestemplateId(templateid, callback)</td>
    <td style="padding:15px">Delete template.</td>
    <td style="padding:15px">{base_path}/{version}/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfraservicesserviceIdserviceEntries(serviceid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Service entries for the given service</td>
    <td style="padding:15px">{base_path}/{version}/infra/services/{pathv1}/service-entries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSections(enforcementpointname, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Firewall Sections</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/firewalls/firewall-sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfraservices(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Services for infra</td>
    <td style="padding:15px">{base_path}/{version}/infra/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfradomainsdomainId(domainid, Domain, callback)</td>
    <td style="padding:15px">Create or update a domain</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomainsdomainId(domainid, callback)</td>
    <td style="padding:15px">Read domain</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfradomainsdomainId(domainid, callback)</td>
    <td style="padding:15px">Delete Domain and all the entities contained by this domain</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPoints(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Enforcement Points</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradeploymentZonesdeploymentZoneId(deploymentzoneid, callback)</td>
    <td style="padding:15px">Read a DeploymentZone</td>
    <td style="padding:15px">{base_path}/{version}/infra/deployment-zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroupsnsgroupName(enforcementpointname, nsgroupname, callback)</td>
    <td style="padding:15px">Read Group</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/groups/nsgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxt(enforcementpointname, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List MACSets</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}/mac-sets/mac-sets-nsxt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomainsdomainIddomainDeploymentMaps(domainid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Domain Deployment maps for infra</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/domain-deployment-maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfrarealizedStateenforcementPointsenforcementPointName(enforcementpointname, callback)</td>
    <td style="padding:15px">Read Enforcement Point</td>
    <td style="padding:15px">{base_path}/{version}/infra/realized-state/enforcement-points/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId(domainid, communicationentryid, CommunicationEntry, callback)</td>
    <td style="padding:15px">Create or update a CommunicationEntry</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/communication-map/communication-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId(domainid, communicationentryid, callback)</td>
    <td style="padding:15px">Delete CommunicationEntry</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/communication-map/communication-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId(domainid, communicationentryid, callback)</td>
    <td style="padding:15px">Read CommunicationEntry</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/communication-map/communication-entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomains(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List domains for infra</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInfradomainsdomainIdcommunicationMap(domainid, CommunicationMap, callback)</td>
    <td style="padding:15px">Update communication map</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/communication-map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomainsdomainIdcommunicationMap(domainid, callback)</td>
    <td style="padding:15px">Read communication-map</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/communication-map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTemplatestemplateIdactiondeploy(templateid, PolicyTemplateParameters, callback)</td>
    <td style="padding:15px">Deploy template.</td>
    <td style="padding:15px">{base_path}/{version}/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfradomainsdomainIdgroups(domainid, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Groups for a domain</td>
    <td style="padding:15px">{base_path}/{version}/infra/domains/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaarolesrole(role, callback)</td>
    <td style="padding:15px">Get role information</td>
    <td style="padding:15px">{base_path}/{version}/aaa/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaauserInfo(callback)</td>
    <td style="padding:15px">Get information about logged-in user</td>
    <td style="padding:15px">{base_path}/{version}/aaa/user-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaavidmgroups(cursor, includedFields, pageSize, searchString, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get all the User Groups where vIDM display name matches the search key case insensitively. The search key is checked to be a substring of display name.</td>
    <td style="padding:15px">{base_path}/{version}/aaa/vidm/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaavidmusers(cursor, includedFields, pageSize, searchString, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get all the users from vIDM whose userName, givenName or familyName matches the search key case insensitively. The search key is checked to be a substring of name or given name or family name.</td>
    <td style="padding:15px">{base_path}/{version}/aaa/vidm/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAaaroleBindingsbindingId(bindingid, RoleBinding, callback)</td>
    <td style="padding:15px">Update User or Group's roles</td>
    <td style="padding:15px">{base_path}/{version}/aaa/role-bindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAaaroleBindingsbindingId(bindingid, callback)</td>
    <td style="padding:15px">Delete user/group's roles assignment</td>
    <td style="padding:15px">{base_path}/{version}/aaa/role-bindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaaroleBindingsbindingId(bindingid, callback)</td>
    <td style="padding:15px">Get user/group's role information</td>
    <td style="padding:15px">{base_path}/{version}/aaa/role-bindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAaaregistrationToken(callback)</td>
    <td style="padding:15px">Create registration access token</td>
    <td style="padding:15px">{base_path}/{version}/aaa/registration-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaaroles(callback)</td>
    <td style="padding:15px">Get information about all roles</td>
    <td style="padding:15px">{base_path}/{version}/aaa/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAaavidmsearch(cursor, includedFields, pageSize, searchString, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get all the users and groups from vIDM matching the search key case insensitively. The search key is checked to be a substring of name or given name or family name of user and display name of group.</td>
    <td style="padding:15px">{base_path}/{version}/aaa/vidm/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAaaregistrationTokentoken(token, callback)</td>
    <td style="padding:15px">Delete registration access token</td>
    <td style="padding:15px">{base_path}/{version}/aaa/registration-token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaaregistrationTokentoken(token, callback)</td>
    <td style="padding:15px">Get registration access token</td>
    <td style="padding:15px">{base_path}/{version}/aaa/registration-token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAaaroleBindings(RoleBinding, callback)</td>
    <td style="padding:15px">Assign roles to User or Group</td>
    <td style="padding:15px">{base_path}/{version}/aaa/role-bindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaaroleBindings(cursor, includedFields, name, pageSize, sortAscending, sortBy, type, callback)</td>
    <td style="padding:15px">Get all users and groups with their roles</td>
    <td style="padding:15px">{base_path}/{version}/aaa/role-bindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsvniPools(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List VNI Pools</td>
    <td style="padding:15px">{base_path}/{version}/pools/vni-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsmacPools(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List MAC Pools</td>
    <td style="padding:15px">{base_path}/{version}/pools/mac-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPoolsipSubnets(IpBlockSubnet, callback)</td>
    <td style="padding:15px">Create subnet of specified size within an IP block</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsipSubnets(blockId, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List subnets within an IP block</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsvtepLabelPools(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List virtual tunnel endpoint Label Pools</td>
    <td style="padding:15px">{base_path}/{version}/pools/vtep-label-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPoolsipBlocksblockId(blockid, IpBlock, callback)</td>
    <td style="padding:15px">Update an IP Address Block</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-blocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsipBlocksblockId(blockid, callback)</td>
    <td style="padding:15px">Get IP address block information.</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-blocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePoolsipBlocksblockId(blockid, callback)</td>
    <td style="padding:15px">Delete an IP Address Block</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-blocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsmacPoolspoolId(poolid, callback)</td>
    <td style="padding:15px">Read MAC Pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/mac-pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPoolsipBlocks(IpBlock, callback)</td>
    <td style="padding:15px">Create a new IP address block.</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-blocks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsipBlocks(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Returns list of configured IP address blocks.</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-blocks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPoolsvniPoolspoolId(poolid, VniPool, callback)</td>
    <td style="padding:15px">Update a VNI Pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/vni-pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsvniPoolspoolId(poolid, callback)</td>
    <td style="padding:15px">Read VNI Pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/vni-pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPoolsipSubnetssubnetId(subnetid, AllocationIpAddress, action, callback)</td>
    <td style="padding:15px">Allocate or Release an IP Address from a Ip Subnet</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-subnets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePoolsipSubnetssubnetId(subnetid, callback)</td>
    <td style="padding:15px">Delete subnet within an IP block</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-subnets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsipSubnetssubnetId(subnetid, callback)</td>
    <td style="padding:15px">Get the subnet within an IP block</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-subnets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPoolsipPools(IpPool, callback)</td>
    <td style="padding:15px">Create an IP Pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsipPools(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List IP Pools</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPoolsipPoolspoolId(poolid, IpPool, callback)</td>
    <td style="padding:15px">Update an IP Pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPoolsipPoolspoolId(poolid, AllocationIpAddress, action, callback)</td>
    <td style="padding:15px">Allocate or Release an IP Address from a Pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsipPoolspoolId(poolid, callback)</td>
    <td style="padding:15px">Read IP Pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePoolsipPoolspoolId(poolid, force, callback)</td>
    <td style="padding:15px">Delete an IP Pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsvtepLabelPoolspoolId(poolid, callback)</td>
    <td style="padding:15px">Read a virtual tunnel endpoint label pool</td>
    <td style="padding:15px">{base_path}/{version}/pools/vtep-label-pools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoolsipPoolspoolIdallocations(poolid, callback)</td>
    <td style="padding:15px">List IP Pool Allocations</td>
    <td style="padding:15px">{base_path}/{version}/pools/ip-pools/{pathv1}/allocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCsmawsgatewayAmis(AwsGatewayAmiInfo, callback)</td>
    <td style="padding:15px">Registers a AWS Gateway AMI for the region specified in the body. One can
register only one gateway AMI ID per region. If a gateway AMI is already
registered with a region, user is expected to use update API to overwrite
the registerd AMI for a region.
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateway-amis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsgatewayAmis(cursor, includedFields, pageSize, regionId, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Returns a list of Aws Gateway Amis</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateway-amis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawskeyPairs(accountId, cursor, includedFields, pageSize, regionId, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Returns a list of Aws Key Pairs</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/key-pairs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsgatewaysvpcIdstatus(vpcid, callback)</td>
    <td style="padding:15px">Returns status information for primary gateway and secondary gateway
for the vpc, if exists.
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateways/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCsmawsgatewaysvpcId(vpcid, AwsGatewayDeployConfig, callback)</td>
    <td style="padding:15px">Updates configuration for primary gateway and secondary gateway
for the vpc, if exists.
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateways/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsgatewaysvpcId(vpcid, callback)</td>
    <td style="padding:15px">Returns configuration for primary gateway and secondary gateway
for the vpc,if exists.
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateways/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsregionsregionId(regionid, callback)</td>
    <td style="padding:15px">Returns information about the particual Aws Region</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCsmawsgatewaysactiondeploy(AwsGatewayDeployConfig, callback)</td>
    <td style="padding:15px">All the required configuration to deploy Aws gateways will be absorbed as a
part of request body in this api and gateway deployment will be triggered.
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmcsmstatus(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return Csm status information</td>
    <td style="padding:15px">{base_path}/{version}/csm/csmstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsvpcs(accountId, cidr, cursor, includedFields, opStatus, pageSize, regionId, sortAscending, sortBy, vpcId, callback)</td>
    <td style="padding:15px">Returns a list of Vpcs. Support optional query parameters like account_id,
region_id, cidr and/or op_status
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/vpcs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCsmnsxManagerAccounts(NsxManagerAccount, callback)</td>
    <td style="padding:15px">Create a NSX Manager account</td>
    <td style="padding:15px">{base_path}/{version}/csm/nsx-manager-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmnsxManagerAccounts(callback)</td>
    <td style="padding:15px">Returns a list of NSX Manager accounts</td>
    <td style="padding:15px">{base_path}/{version}/csm/nsx-manager-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsregions(accountId, cursor, includedFields, pageSize, regionId, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Returns a list of Aws regions</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmvirtualMachinesvirtualMachineId(virtualmachineid, callback)</td>
    <td style="padding:15px">Returns information about the particular virtual machine</td>
    <td style="padding:15px">{base_path}/{version}/csm/virtual-machines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCsmawsgatewayAmisregionId(regionid, AwsGatewayAmiInfo, callback)</td>
    <td style="padding:15px">Update a AWS Gateway AMI</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateway-amis/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCsmawsgatewayAmisregionId(regionid, callback)</td>
    <td style="padding:15px">Delete a AWS Gateway AMI</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateway-amis/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsgatewayAmisregionId(regionid, callback)</td>
    <td style="padding:15px">Returns AWS Gateway AMI for a particular region</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateway-amis/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCsmnsxManagerAccountsaccountId(accountid, NsxManagerAccount, callback)</td>
    <td style="padding:15px">Update a NSX Manager account</td>
    <td style="padding:15px">{base_path}/{version}/csm/nsx-manager-accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCsmnsxManagerAccountsaccountId(accountid, callback)</td>
    <td style="padding:15px">Delete a NSX Manager account</td>
    <td style="padding:15px">{base_path}/{version}/csm/nsx-manager-accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmnsxManagerAccountsaccountId(accountid, callback)</td>
    <td style="padding:15px">Returns the particular NSX Manager account information</td>
    <td style="padding:15px">{base_path}/{version}/csm/nsx-manager-accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCsmawsaccountsaccountIdactionsyncInventory(accountid, callback)</td>
    <td style="padding:15px">Synchronizes Aws account related inventory like Regions, Vpcs, Instances
Status of inventory synchronization can be known from Aws account status api
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCsmawsaccountsaccountId(accountid, AwsAccount, callback)</td>
    <td style="padding:15px">Update a AWS account information</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCsmawsaccountsaccountId(accountid, callback)</td>
    <td style="padding:15px">Delete AWS account information</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsaccountsaccountId(accountid, callback)</td>
    <td style="padding:15px">Returns the details of the particular AWS account</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCsmawsaccounts(AwsAccount, callback)</td>
    <td style="padding:15px">Add a AWS account to cloud serivce manager</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsaccounts(cursor, includedFields, pageSize, regionId, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return a list of all AWS accounts</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmvirtualMachines(accountId, cursor, includedFields, instanceId, isGateway, logicalSwitchId, pageSize, publicIp, quarantineState, regionId, resourceType, sortAscending, sortBy, vpcId, callback)</td>
    <td style="padding:15px">Returns the list of all virtual machines created or imported under a particular account id. Supports optional query parameters like region id, vpc id, public_ip, is_gateway.</td>
    <td style="padding:15px">{base_path}/{version}/csm/virtual-machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCsmawsgatewaysactionundeploy(AwsGatewayUndeployConfig, callback)</td>
    <td style="padding:15px">All the required configuration to undeploy Aws gateways will be absorbed as a
part of request body in this api and gateway undeployment will be triggered.
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsvpcsvpcId(vpcid, callback)</td>
    <td style="padding:15px">Returns Vpc information</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/vpcs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawssubnets(accountId, availabilityZoneName, cursor, includedFields, pageSize, regionName, sortAscending, sortBy, vpcId, callback)</td>
    <td style="padding:15px">Returns a list of subnets</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsgateways(accountId, cursor, includedFields, pageSize, regionId, sortAscending, sortBy, vpcId, callback)</td>
    <td style="padding:15px">Returns configuration information for all gateways</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/gateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsmawsaccountsaccountIdstatus(accountid, callback)</td>
    <td style="padding:15px">Return status of the account like credentails validity, inventory
synchronization status and inventory synchronization state
</td>
    <td style="padding:15px">{base_path}/{version}/csm/aws/accounts/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesnodeIdmodules(nodeid, callback)</td>
    <td style="padding:15px">Get the module details of a Fabric Node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}/modules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFabricvirtualMachinesactionupdateTags(VirtualMachineTagUpdate, callback)</td>
    <td style="padding:15px">Update tags applied to a virtual machine</td>
    <td style="padding:15px">{base_path}/{version}/fabric/virtual-machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabriccomputeManagerscomputeManagerIdstatus(computemanagerid, callback)</td>
    <td style="padding:15px">Return runtime status information for a compute manager</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-managers/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesstatus(ReadNodesStatusRequestParameters, callback)</td>
    <td style="padding:15px">Return Runtime Status Information for given Nodes</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabriccomputeCollections(cmLocalId, cursor, discoveredNodeId, displayName, externalId, includedFields, nodeId, originId, originType, ownerId, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return the List of Compute Collections</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFabricnodesnodeIdactionrestartInventorySync(nodeid, callback)</td>
    <td style="padding:15px">Restart the inventory sync for the node if it is paused currently.</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFabriccomputeCollectionFabricTemplates(ComputeCollectionFabricTemplate, callback)</td>
    <td style="padding:15px">Create a compute collection fabric template</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-collection-fabric-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabriccomputeCollectionFabricTemplates(computeCollectionId, callback)</td>
    <td style="padding:15px">Get compute collection fabric templates</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-collection-fabric-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricvifs(cursor, hostId, includedFields, lportAttachmentId, ownerVmId, pageSize, sortAscending, sortBy, vmId, callback)</td>
    <td style="padding:15px">Return the List of Virtual Network Interfaces (VIFs)</td>
    <td style="padding:15px">{base_path}/{version}/fabric/vifs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesnodeIdnetworkinterfacesinterfaceId(nodeid, interfaceid, source, callback)</td>
    <td style="padding:15px">Read the node's Network Interface</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}/network/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricdiscoveredNodesnodeExtId(nodeextid, callback)</td>
    <td style="padding:15px">Return Discovered Node Information</td>
    <td style="padding:15px">{base_path}/{version}/fabric/discovered-nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricdiscoveredNodes(cmLocalId, cursor, displayName, externalId, hasParent, includedFields, ipAddress, nodeId, nodeType, originId, pageSize, parentComputeCollection, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return the List of Discovered Nodes</td>
    <td style="padding:15px">{base_path}/{version}/fabric/discovered-nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabriccomputeCollectionsccExtId(ccextid, callback)</td>
    <td style="padding:15px">Return Compute Collection Information</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-collections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricvirtualMachines(cursor, displayName, externalId, hostId, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return the List of Virtual Machines</td>
    <td style="padding:15px">{base_path}/{version}/fabric/virtual-machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesnodeIdnetworkinterfaces(nodeid, source, callback)</td>
    <td style="padding:15px">List the specified node's Network Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}/network/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFabricnodesnodeIdactionupgradeInfra(nodeid, disableVmMigration, callback)</td>
    <td style="padding:15px">Perform a service deployment upgrade on a host node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFabriccomputeManagers(ComputeManager, callback)</td>
    <td style="padding:15px">Register compute manager with NSX</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-managers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabriccomputeManagers(cursor, includedFields, originType, pageSize, server, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return the List of Compute managers</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-managers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesnodeIdstate(nodeid, callback)</td>
    <td style="padding:15px">Get the Realized State of a Fabric Node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFabricdiscoveredNodesnodeExtIdactionhostprep(nodeextid, callback)</td>
    <td style="padding:15px">Prepares discovered Node for NSX</td>
    <td style="padding:15px">{base_path}/{version}/fabric/discovered-nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabriccomputeManagerscomputeManagerIdstate(computemanagerid, callback)</td>
    <td style="padding:15px">Get the realized state of a compute manager</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-managers/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesnodeIdcapabilities(nodeid, callback)</td>
    <td style="padding:15px">Return the List of Capabilities of a Single Node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}/capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFabricnodes(Node, callback)</td>
    <td style="padding:15px">Register and Install NSX Components on a Node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodes(cursor, discoveredNodeId, displayName, externalId, hypervisorOsType, includedFields, ipAddress, pageSize, resourceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Return the List of Nodes</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesnodeIdstatus(nodeid, source, callback)</td>
    <td style="padding:15px">Return Runtime Status Information for a Node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFabriccomputeCollectionFabricTemplatesfabricTemplateId(fabrictemplateid, ComputeCollectionFabricTemplate, callback)</td>
    <td style="padding:15px">Updates compute collection fabric template</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-collection-fabric-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabriccomputeCollectionFabricTemplatesfabricTemplateId(fabrictemplateid, callback)</td>
    <td style="padding:15px">Get compute collection fabric template by id</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-collection-fabric-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFabriccomputeCollectionFabricTemplatesfabricTemplateId(fabrictemplateid, callback)</td>
    <td style="padding:15px">Deletes compute collection fabric template</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-collection-fabric-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFabriccomputeManagerscomputeManagerId(computemanagerid, ComputeManager, callback)</td>
    <td style="padding:15px">Update compute manager</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-managers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFabriccomputeManagerscomputeManagerId(computemanagerid, callback)</td>
    <td style="padding:15px">Unregister a compute manager</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-managers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabriccomputeManagerscomputeManagerId(computemanagerid, callback)</td>
    <td style="padding:15px">Return compute manager Information</td>
    <td style="padding:15px">{base_path}/{version}/fabric/compute-managers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putFabricnodesnodeId(nodeid, Node, callback)</td>
    <td style="padding:15px">Update a Node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFabricnodesnodeId(nodeid, action, callback)</td>
    <td style="padding:15px">Perform an Action on Fabric Node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesnodeId(nodeid, callback)</td>
    <td style="padding:15px">Return Node Information</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFabricnodesnodeId(nodeid, unprepareHost, callback)</td>
    <td style="padding:15px">Delete a Node</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFabricnodesnodeIdnetworkinterfacesinterfaceIdstats(nodeid, interfaceid, source, callback)</td>
    <td style="padding:15px">Read the NSX Manager's Network Interface Statistics</td>
    <td style="padding:15px">{base_path}/{version}/fabric/nodes/{pathv1}/network/interfaces/{pathv2}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTransportNodestransportnodeId(transportnodeid, TransportNode, esxMgmtIfMigrationDest, ifId, callback)</td>
    <td style="padding:15px">Update a Transport Node</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransportNodestransportnodeId(transportnodeid, action, callback)</td>
    <td style="padding:15px">Update transport node maintenance mode</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransportNodestransportnodeId(transportnodeid, callback)</td>
    <td style="padding:15px">Delete a Transport Node</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodestransportnodeId(transportnodeid, callback)</td>
    <td style="padding:15px">Get a Transport Node</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postComputeCollectionTransportNodeTemplates(ComputeCollectionTransportNodeTemplate, callback)</td>
    <td style="padding:15px">Create transport node template for compute collection.</td>
    <td style="padding:15px">{base_path}/{version}/compute-collection-transport-node-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComputeCollectionTransportNodeTemplates(computeCollectionId, callback)</td>
    <td style="padding:15px">List compute collection transportnode templates</td>
    <td style="padding:15px">{base_path}/{version}/compute-collection-transport-node-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBridgeEndpoints(BridgeEndpoint, callback)</td>
    <td style="padding:15px">Create a Bridge Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/bridge-endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeEndpoints(bridgeClusterId, cursor, includedFields, logicalSwitchId, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List All Bridge Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/bridge-endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZoneszoneIdsummary(zoneid, callback)</td>
    <td style="padding:15px">Get a Transport Zone's Current Runtime Status Information</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEdgeClustersedgeClusterId(edgeclusterid, EdgeCluster, callback)</td>
    <td style="padding:15px">Update Edge Cluster</td>
    <td style="padding:15px">{base_path}/{version}/edge-clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEdgeClustersedgeClusterId(edgeclusterid, callback)</td>
    <td style="padding:15px">Delete Edge Cluster</td>
    <td style="padding:15px">{base_path}/{version}/edge-clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEdgeClustersedgeClusterId(edgeclusterid, callback)</td>
    <td style="padding:15px">Read Edge Cluster</td>
    <td style="padding:15px">{base_path}/{version}/edge-clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putHostSwitchProfileshostSwitchProfileId(hostswitchprofileid, BaseHostSwitchProfile, callback)</td>
    <td style="padding:15px">Update a Hostswitch Profile</td>
    <td style="padding:15px">{base_path}/{version}/host-switch-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostSwitchProfileshostSwitchProfileId(hostswitchprofileid, callback)</td>
    <td style="padding:15px">Get a Hostswitch Profile by ID</td>
    <td style="padding:15px">{base_path}/{version}/host-switch-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHostSwitchProfileshostSwitchProfileId(hostswitchprofileid, callback)</td>
    <td style="padding:15px">Delete a Hostswitch Profile</td>
    <td style="padding:15px">{base_path}/{version}/host-switch-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeEndpointsendpointIdstatus(endpointid, source, callback)</td>
    <td style="padding:15px">Returns status of a specified Bridge Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/bridge-endpoints/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBridgeEndpointsbridgeendpointId(bridgeendpointid, BridgeEndpoint, callback)</td>
    <td style="padding:15px">Update a Bridge Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/bridge-endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeEndpointsbridgeendpointId(bridgeendpointid, callback)</td>
    <td style="padding:15px">Get Information about a bridge endpoint</td>
    <td style="padding:15px">{base_path}/{version}/bridge-endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBridgeEndpointsbridgeendpointId(bridgeendpointid, callback)</td>
    <td style="padding:15px">Delete a Bridge Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/bridge-endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEdgeClusters(EdgeCluster, callback)</td>
    <td style="padding:15px">Create Edge Cluster</td>
    <td style="padding:15px">{base_path}/{version}/edge-clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEdgeClusters(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Edge Clusters</td>
    <td style="padding:15px">{base_path}/{version}/edge-clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransportNodestransportnodeIdactionresyncHostConfig(transportnodeid, callback)</td>
    <td style="padding:15px">Resync a Transport Node</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBridgeClusters(BridgeCluster, callback)</td>
    <td style="padding:15px">Create a Bridge Cluster</td>
    <td style="padding:15px">{base_path}/{version}/bridge-clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeClusters(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List All Bridge Clusters</td>
    <td style="padding:15px">{base_path}/{version}/bridge-clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putClusterProfilesclusterProfileId(clusterprofileid, ClusterProfile, callback)</td>
    <td style="padding:15px">Update a cluster profile</td>
    <td style="padding:15px">{base_path}/{version}/cluster-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClusterProfilesclusterProfileId(clusterprofileid, callback)</td>
    <td style="padding:15px">Delete a cluster profile</td>
    <td style="padding:15px">{base_path}/{version}/cluster-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterProfilesclusterProfileId(clusterprofileid, callback)</td>
    <td style="padding:15px">Get cluster profile by Id</td>
    <td style="padding:15px">{base_path}/{version}/cluster-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLldptransportNodesnodeIdinterfacesinterfaceName(nodeid, interfacename, callback)</td>
    <td style="padding:15px">Read LLDP Neighbor Properties</td>
    <td style="padding:15px">{base_path}/{version}/lldp/transport-nodes/{pathv1}/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEdgeClustersedgeClusterIdactionreplaceTransportNode(edgeclusterid, EdgeClusterMemberTransportNode, callback)</td>
    <td style="padding:15px">Replace the transport node in the specified member of the edge-cluster</td>
    <td style="padding:15px">{base_path}/{version}/edge-clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeClustersclusterIdstatus(clusterid, source, callback)</td>
    <td style="padding:15px">Returns status of a specified Bridge Cluster</td>
    <td style="padding:15px">{base_path}/{version}/bridge-clusters/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComputeCollectionTransportNodeTemplatestemplateIdstate(templateid, computeCollectionId, cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get compute collection transportnode template application states</td>
    <td style="padding:15px">{base_path}/{version}/compute-collection-transport-node-templates/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHostSwitchProfiles(BaseHostSwitchProfile, callback)</td>
    <td style="padding:15px">Create a Hostswitch Profile</td>
    <td style="padding:15px">{base_path}/{version}/host-switch-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostSwitchProfiles(cursor, hostswitchProfileType, includeSystemOwned, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Hostswitch Profiles</td>
    <td style="padding:15px">{base_path}/{version}/host-switch-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransportZones(TransportZone, callback)</td>
    <td style="padding:15px">Create a Transport Zone</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZones(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Transport Zones</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTransportZoneszoneId(zoneid, TransportZone, callback)</td>
    <td style="padding:15px">Update a Transport Zone</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportZoneszoneId(zoneid, callback)</td>
    <td style="padding:15px">Get a Transport Zone</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransportZoneszoneId(zoneid, callback)</td>
    <td style="padding:15px">Delete a Transport Zone</td>
    <td style="padding:15px">{base_path}/{version}/transport-zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodestransportnodeIdstate(transportnodeid, callback)</td>
    <td style="padding:15px">Get a Transport Node's State</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransportNodes(TransportNode, callback)</td>
    <td style="padding:15px">Create a Transport Node</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodes(cursor, inMaintenanceMode, includedFields, nodeId, nodeIp, nodeTypes, pageSize, sortAscending, sortBy, transportZoneId, callback)</td>
    <td style="padding:15px">List Transport Nodes</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postClusterProfiles(ClusterProfile, callback)</td>
    <td style="padding:15px">Create a Cluster Profile</td>
    <td style="padding:15px">{base_path}/{version}/cluster-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterProfiles(cursor, includeSystemOwned, includedFields, pageSize, resourceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List Cluster Profiles</td>
    <td style="padding:15px">{base_path}/{version}/cluster-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLldptransportNodesnodeIdinterfaces(nodeid, callback)</td>
    <td style="padding:15px">List LLDP Neighbor Properties</td>
    <td style="padding:15px">{base_path}/{version}/lldp/transport-nodes/{pathv1}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBridgeClustersbridgeclusterId(bridgeclusterid, BridgeCluster, callback)</td>
    <td style="padding:15px">Update a Bridge Cluster</td>
    <td style="padding:15px">{base_path}/{version}/bridge-clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBridgeClustersbridgeclusterId(bridgeclusterid, callback)</td>
    <td style="padding:15px">Delete a Bridge Cluster</td>
    <td style="padding:15px">{base_path}/{version}/bridge-clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeClustersbridgeclusterId(bridgeclusterid, callback)</td>
    <td style="padding:15px">Get Information about a bridge cluster</td>
    <td style="padding:15px">{base_path}/{version}/bridge-clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putComputeCollectionTransportNodeTemplatestemplateId(templateid, ComputeCollectionTransportNodeTemplate, callback)</td>
    <td style="padding:15px">Update compute collection transportnode template</td>
    <td style="padding:15px">{base_path}/{version}/compute-collection-transport-node-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComputeCollectionTransportNodeTemplatestemplateId(templateid, callback)</td>
    <td style="padding:15px">Get compute collection transportnode template by id</td>
    <td style="padding:15px">{base_path}/{version}/compute-collection-transport-node-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteComputeCollectionTransportNodeTemplatestemplateId(templateid, callback)</td>
    <td style="padding:15px">Delete a compute collection transport node template</td>
    <td style="padding:15px">{base_path}/{version}/compute-collection-transport-node-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeEndpointsendpointIdstatistics(endpointid, source, callback)</td>
    <td style="padding:15px">Returns statistics of a specified Bridge Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/bridge-endpoints/{pathv1}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTransportzoneProfilestransportzoneProfileId(transportzoneprofileid, TransportZoneProfile, callback)</td>
    <td style="padding:15px">Update a transport zone profile</td>
    <td style="padding:15px">{base_path}/{version}/transportzone-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransportzoneProfilestransportzoneProfileId(transportzoneprofileid, callback)</td>
    <td style="padding:15px">Delete a transport zone Profile</td>
    <td style="padding:15px">{base_path}/{version}/transportzone-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportzoneProfilestransportzoneProfileId(transportzoneprofileid, callback)</td>
    <td style="padding:15px">Get transport zone profile by identifier</td>
    <td style="padding:15px">{base_path}/{version}/transportzone-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodesstate(mmState, status, vtepIp, callback)</td>
    <td style="padding:15px">List transport nodes by realized state</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransportzoneProfiles(TransportZoneProfile, callback)</td>
    <td style="padding:15px">Create a transport zone Profile</td>
    <td style="padding:15px">{base_path}/{version}/transportzone-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportzoneProfiles(cursor, includeSystemOwned, includedFields, pageSize, resourceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List transport zone profiles</td>
    <td style="padding:15px">{base_path}/{version}/transportzone-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpfixconfigsconfigId(configid, IpfixConfig, callback)</td>
    <td style="padding:15px">Update an existing IPFIX configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpfixconfigsconfigId(configid, callback)</td>
    <td style="padding:15px">Delete an existing IPFIX configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpfixconfigsconfigId(configid, callback)</td>
    <td style="padding:15px">Get an existing IPFIX configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpfixconfigs(IpfixConfig, callback)</td>
    <td style="padding:15px">Create a new IPFIX configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpfixconfigs(appliedTo, cursor, includedFields, ipfixConfigType, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List IPFIX configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpfixcollectorconfigs(IpfixCollectorConfig, callback)</td>
    <td style="padding:15px">Create a new IPFIX collector configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/collectorconfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpfixcollectorconfigs(cursor, includedFields, pageSize, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">List IPFIX collector configurations</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/collectorconfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpfixcollectorconfigscollectorConfigId(collectorconfigid, IpfixCollectorConfig, callback)</td>
    <td style="padding:15px">Update an existing IPFIX collector configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/collectorconfigs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpfixcollectorconfigscollectorConfigId(collectorconfigid, callback)</td>
    <td style="padding:15px">Delete an existing IPFIX collector configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/collectorconfigs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpfixcollectorconfigscollectorConfigId(collectorconfigid, callback)</td>
    <td style="padding:15px">Get an existing IPFIX collector configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipfix/collectorconfigs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNormalizations(cursor, includedFields, pageSize, preferredNormalizationType, resourceId, resourceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get normalizations based on the query parameters</td>
    <td style="padding:15px">{base_path}/{version}/normalizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealizationStateBarriercurrent(callback)</td>
    <td style="padding:15px">Gets the current barrier number</td>
    <td style="padding:15px">{base_path}/{version}/realization-state-barrier/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRealizationStateBarriercurrentactionincrement(callback)</td>
    <td style="padding:15px">Increments the barrier count by 1</td>
    <td style="padding:15px">{base_path}/{version}/realization-state-barrier/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRealizationStateBarrierconfig(RealizationStateBarrierConfig, callback)</td>
    <td style="padding:15px">Updates the barrier configuration</td>
    <td style="padding:15px">{base_path}/{version}/realization-state-barrier/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealizationStateBarrierconfig(callback)</td>
    <td style="padding:15px">Gets the realization state barrier configuration</td>
    <td style="padding:15px">{base_path}/{version}/realization-state-barrier/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodesnodeIdtunnels(nodeid, bfdDiagnosticCode, cursor, includedFields, pageSize, remoteNodeId, sortAscending, sortBy, source, status, callback)</td>
    <td style="padding:15px">List of tunnels</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}/tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportNodesnodeIdtunnelstunnelName(nodeid, tunnelname, source, callback)</td>
    <td style="padding:15px">Tunnel properties</td>
    <td style="padding:15px">{base_path}/{version}/transport-nodes/{pathv1}/tunnels/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssociations(associatedResourceType, cursor, fetchAncestors, includedFields, pageSize, resourceId, resourceType, sortAscending, sortBy, callback)</td>
    <td style="padding:15px">Get ResourceReference objects to which the given resource belongs to
</td>
    <td style="padding:15px">{base_path}/{version}/associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUiViewsdashboardwidgetconfigurationswidgetconfigurationId(widgetconfigurationid, WidgetConfiguration, callback)</td>
    <td style="padding:15px">Update Widget Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ui-views/dashboard/widgetconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUiViewsdashboardwidgetconfigurationswidgetconfigurationId(widgetconfigurationid, callback)</td>
    <td style="padding:15px">Returns Widget Configuration Information</td>
    <td style="padding:15px">{base_path}/{version}/ui-views/dashboard/widgetconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUiViewsdashboardwidgetconfigurationswidgetconfigurationId(widgetconfigurationid, callback)</td>
    <td style="padding:15px">Delete Widget Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ui-views/dashboard/widgetconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUiViewsdashboardwidgetconfigurations(WidgetConfiguration, callback)</td>
    <td style="padding:15px">Creates a new Widget Configuration.</td>
    <td style="padding:15px">{base_path}/{version}/ui-views/dashboard/widgetconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUiViewsdashboardwidgetconfigurations(container, widgetIds, callback)</td>
    <td style="padding:15px">Returns the Widget Configurations based on query criteria defined in WidgetQueryParameters.</td>
    <td style="padding:15px">{base_path}/{version}/ui-views/dashboard/widgetconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getErrorResolvererrorId(errorId, callback)</td>
    <td style="padding:15px">Fetches metadata about the given error_id</td>
    <td style="padding:15px">{base_path}/{version}/error-resolver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getErrorResolver(callback)</td>
    <td style="padding:15px">Fetches a list of metadata for all the registered error resolvers</td>
    <td style="padding:15px">{base_path}/{version}/error-resolver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postErrorResolveractionresolveError(ErrorResolverMetadataList, callback)</td>
    <td style="padding:15px">Resolves the error</td>
    <td style="padding:15px">{base_path}/{version}/error-resolver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodeaaaauthPolicy(AuthenticationPolicyProperties, callback)</td>
    <td style="padding:15px">Update node authentication policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/node/aaa/auth-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeaaaauthPolicy(callback)</td>
    <td style="padding:15px">Read node authentication policy configuration</td>
    <td style="padding:15px">{base_path}/{version}/node/aaa/auth-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskstaskId(taskid, callback)</td>
    <td style="padding:15px">Get information about the specified task</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBatch(BatchRequest, atomic, callback)</td>
    <td style="padding:15px">Register a Collection of API Calls at a Single End Point</td>
    <td style="padding:15px">{base_path}/{version}/batch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasks(cursor, includedFields, pageSize, requestUri, sortAscending, sortBy, status, user, callback)</td>
    <td style="padding:15px">Get information about all tasks</td>
    <td style="padding:15px">{base_path}/{version}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskstaskIdresponse(taskid, callback)</td>
    <td style="padding:15px">Get the response of a task</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}/response?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
