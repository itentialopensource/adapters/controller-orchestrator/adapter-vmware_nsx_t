/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-vmware_nsx_t',
      type: 'VmwareNsxT',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const VmwareNsxT = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Vmware_nsx_t Adapter Test', () => {
  describe('VmwareNsxT Class Tests', () => {
    const a = new VmwareNsxT(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('vmware_nsx_t'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.7.3', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.14.2', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('vmware_nsx_t'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('VmwareNsxT', pronghornDotJson.export);
          assert.equal('Vmware_nsx_t', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-vmware_nsx_t', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('vmware_nsx_t'));
          assert.equal('VmwareNsxT', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-vmware_nsx_t', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-vmware_nsx_t', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#putHpmfeaturesfeatureStackName - errors', () => {
      it('should have a putHpmfeaturesfeatureStackName function', (done) => {
        try {
          assert.equal(true, typeof a.putHpmfeaturesfeatureStackName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featurestackname', (done) => {
        try {
          a.putHpmfeaturesfeatureStackName(null, null, (data, error) => {
            try {
              const displayE = 'featurestackname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putHpmfeaturesfeatureStackName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FeatureStackCollectionConfiguration', (done) => {
        try {
          a.putHpmfeaturesfeatureStackName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'FeatureStackCollectionConfiguration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putHpmfeaturesfeatureStackName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHpmfeaturesfeatureStackName - errors', () => {
      it('should have a getHpmfeaturesfeatureStackName function', (done) => {
        try {
          assert.equal(true, typeof a.getHpmfeaturesfeatureStackName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featurestackname', (done) => {
        try {
          a.getHpmfeaturesfeatureStackName(null, (data, error) => {
            try {
              const displayE = 'featurestackname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getHpmfeaturesfeatureStackName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postHpmfeaturesfeatureStackNameactionresetCollectionFrequency - errors', () => {
      it('should have a postHpmfeaturesfeatureStackNameactionresetCollectionFrequency function', (done) => {
        try {
          assert.equal(true, typeof a.postHpmfeaturesfeatureStackNameactionresetCollectionFrequency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featurestackname', (done) => {
        try {
          a.postHpmfeaturesfeatureStackNameactionresetCollectionFrequency(null, null, (data, error) => {
            try {
              const displayE = 'featurestackname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postHpmfeaturesfeatureStackNameactionresetCollectionFrequency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.postHpmfeaturesfeatureStackNameactionresetCollectionFrequency('fakeparam', null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postHpmfeaturesfeatureStackNameactionresetCollectionFrequency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putHpmglobalConfig - errors', () => {
      it('should have a putHpmglobalConfig function', (done) => {
        try {
          assert.equal(true, typeof a.putHpmglobalConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing GlobalCollectionConfiguration', (done) => {
        try {
          a.putHpmglobalConfig(null, (data, error) => {
            try {
              const displayE = 'GlobalCollectionConfiguration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putHpmglobalConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHpmglobalConfig - errors', () => {
      it('should have a getHpmglobalConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getHpmglobalConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHpmfeatures - errors', () => {
      it('should have a getHpmfeatures function', (done) => {
        try {
          assert.equal(true, typeof a.getHpmfeatures === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitcheslswitchIdvtepTable - errors', () => {
      it('should have a getLogicalSwitcheslswitchIdvtepTable function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitcheslswitchIdvtepTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.getLogicalSwitcheslswitchIdvtepTable(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalSwitcheslswitchIdvtepTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchingProfilesswitchingProfileIdsummary - errors', () => {
      it('should have a getSwitchingProfilesswitchingProfileIdsummary function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchingProfilesswitchingProfileIdsummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchingprofileid', (done) => {
        try {
          a.getSwitchingProfilesswitchingProfileIdsummary(null, (data, error) => {
            try {
              const displayE = 'switchingprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getSwitchingProfilesswitchingProfileIdsummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalSwitches - errors', () => {
      it('should have a postLogicalSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalSwitch', (done) => {
        try {
          a.postLogicalSwitches(null, (data, error) => {
            try {
              const displayE = 'LogicalSwitch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalSwitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitches - errors', () => {
      it('should have a getLogicalSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalPorts - errors', () => {
      it('should have a postLogicalPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalPort', (done) => {
        try {
          a.postLogicalPorts(null, (data, error) => {
            try {
              const displayE = 'LogicalPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPorts - errors', () => {
      it('should have a getLogicalPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPortslportIdstate - errors', () => {
      it('should have a getLogicalPortslportIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPortslportIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.getLogicalPortslportIdstate(null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalPortslportIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitcheslswitchIdsummary - errors', () => {
      it('should have a getLogicalSwitcheslswitchIdsummary function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitcheslswitchIdsummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.getLogicalSwitcheslswitchIdsummary(null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalSwitcheslswitchIdsummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitcheslswitchIdmacTableformatcsv - errors', () => {
      it('should have a getLogicalSwitcheslswitchIdmacTableformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitcheslswitchIdmacTableformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.getLogicalSwitcheslswitchIdmacTableformatcsv(null, null, null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalSwitcheslswitchIdmacTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitcheslswitchIdstate - errors', () => {
      it('should have a getLogicalSwitcheslswitchIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitcheslswitchIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.getLogicalSwitcheslswitchIdstate(null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalSwitcheslswitchIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitcheslswitchIdstatistics - errors', () => {
      it('should have a getLogicalSwitcheslswitchIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitcheslswitchIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.getLogicalSwitcheslswitchIdstatistics(null, null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalSwitcheslswitchIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitchesstatus - errors', () => {
      it('should have a getLogicalSwitchesstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitchesstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSwitchingProfilesswitchingProfileId - errors', () => {
      it('should have a putSwitchingProfilesswitchingProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putSwitchingProfilesswitchingProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchingprofileid', (done) => {
        try {
          a.putSwitchingProfilesswitchingProfileId(null, null, (data, error) => {
            try {
              const displayE = 'switchingprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putSwitchingProfilesswitchingProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BaseSwitchingProfile', (done) => {
        try {
          a.putSwitchingProfilesswitchingProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'BaseSwitchingProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putSwitchingProfilesswitchingProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchingProfilesswitchingProfileId - errors', () => {
      it('should have a getSwitchingProfilesswitchingProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchingProfilesswitchingProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchingprofileid', (done) => {
        try {
          a.getSwitchingProfilesswitchingProfileId(null, (data, error) => {
            try {
              const displayE = 'switchingprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getSwitchingProfilesswitchingProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSwitchingProfilesswitchingProfileId - errors', () => {
      it('should have a deleteSwitchingProfilesswitchingProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSwitchingProfilesswitchingProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchingprofileid', (done) => {
        try {
          a.deleteSwitchingProfilesswitchingProfileId(null, null, (data, error) => {
            try {
              const displayE = 'switchingprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteSwitchingProfilesswitchingProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSwitchingProfiles - errors', () => {
      it('should have a postSwitchingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postSwitchingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BaseSwitchingProfile', (done) => {
        try {
          a.postSwitchingProfiles(null, (data, error) => {
            try {
              const displayE = 'BaseSwitchingProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postSwitchingProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchingProfiles - errors', () => {
      it('should have a getSwitchingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPortslportIdstatus - errors', () => {
      it('should have a getLogicalPortslportIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPortslportIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.getLogicalPortslportIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalPortslportIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPortslportIdmacTableformatcsv - errors', () => {
      it('should have a getLogicalPortslportIdmacTableformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPortslportIdmacTableformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.getLogicalPortslportIdmacTableformatcsv(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalPortslportIdmacTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPortslportIdmacTable - errors', () => {
      it('should have a getLogicalPortslportIdmacTable function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPortslportIdmacTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.getLogicalPortslportIdmacTable(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalPortslportIdmacTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitcheslswitchIdvtepTableformatcsv - errors', () => {
      it('should have a getLogicalSwitcheslswitchIdvtepTableformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitcheslswitchIdvtepTableformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.getLogicalSwitcheslswitchIdvtepTableformatcsv(null, null, null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalSwitcheslswitchIdvtepTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitcheslswitchIdmacTable - errors', () => {
      it('should have a getLogicalSwitcheslswitchIdmacTable function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitcheslswitchIdmacTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.getLogicalSwitcheslswitchIdmacTable(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalSwitcheslswitchIdmacTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPortsstatus - errors', () => {
      it('should have a getLogicalPortsstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPortsstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPortslportIdstatistics - errors', () => {
      it('should have a getLogicalPortslportIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPortslportIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.getLogicalPortslportIdstatistics(null, null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalPortslportIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalSwitcheslswitchId - errors', () => {
      it('should have a putLogicalSwitcheslswitchId function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalSwitcheslswitchId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.putLogicalSwitcheslswitchId(null, null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalSwitcheslswitchId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalSwitch', (done) => {
        try {
          a.putLogicalSwitcheslswitchId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LogicalSwitch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalSwitcheslswitchId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalSwitcheslswitchId - errors', () => {
      it('should have a deleteLogicalSwitcheslswitchId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalSwitcheslswitchId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.deleteLogicalSwitcheslswitchId(null, null, null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalSwitcheslswitchId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitcheslswitchId - errors', () => {
      it('should have a getLogicalSwitcheslswitchId function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitcheslswitchId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lswitchid', (done) => {
        try {
          a.getLogicalSwitcheslswitchId(null, (data, error) => {
            try {
              const displayE = 'lswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalSwitcheslswitchId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalSwitchesstate - errors', () => {
      it('should have a getLogicalSwitchesstate function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalSwitchesstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalPortslportId - errors', () => {
      it('should have a putLogicalPortslportId function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalPortslportId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.putLogicalPortslportId(null, null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalPortslportId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalPort', (done) => {
        try {
          a.putLogicalPortslportId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LogicalPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalPortslportId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPortslportId - errors', () => {
      it('should have a getLogicalPortslportId function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPortslportId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.getLogicalPortslportId(null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalPortslportId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalPortslportId - errors', () => {
      it('should have a deleteLogicalPortslportId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalPortslportId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.deleteLogicalPortslportId(null, null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalPortslportId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoverysessionssessionIdinstalledApps - errors', () => {
      it('should have a getAppDiscoverysessionssessionIdinstalledApps function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoverysessionssessionIdinstalledApps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.getAppDiscoverysessionssessionIdinstalledApps(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoverysessionssessionIdinstalledApps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAppDiscoverysessionssessionIdreClassify - errors', () => {
      it('should have a postAppDiscoverysessionssessionIdreClassify function', (done) => {
        try {
          assert.equal(true, typeof a.postAppDiscoverysessionssessionIdreClassify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.postAppDiscoverysessionssessionIdreClassify(null, null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAppDiscoverysessionssessionIdreClassify', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing SessionReclassificationParameter', (done) => {
        try {
          a.postAppDiscoverysessionssessionIdreClassify('fakeparam', null, (data, error) => {
            try {
              const displayE = 'SessionReclassificationParameter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAppDiscoverysessionssessionIdreClassify', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAppDiscoverysessionssessionIdreportappInfoAndVmformatcsv - errors', () => {
      it('should have a postAppDiscoverysessionssessionIdreportappInfoAndVmformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.postAppDiscoverysessionssessionIdreportappInfoAndVmformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.postAppDiscoverysessionssessionIdreportappInfoAndVmformatcsv(null, null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAppDiscoverysessionssessionIdreportappInfoAndVmformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ReportAppResultsForVmsRequestParameters', (done) => {
        try {
          a.postAppDiscoverysessionssessionIdreportappInfoAndVmformatcsv('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ReportAppResultsForVmsRequestParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAppDiscoverysessionssessionIdreportappInfoAndVmformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoverysessionssessionIdnsGroups - errors', () => {
      it('should have a getAppDiscoverysessionssessionIdnsGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoverysessionssessionIdnsGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.getAppDiscoverysessionssessionIdnsGroups(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoverysessionssessionIdnsGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoverysessionssessionIdsummary - errors', () => {
      it('should have a getAppDiscoverysessionssessionIdsummary function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoverysessionssessionIdsummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.getAppDiscoverysessionssessionIdsummary(null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoverysessionssessionIdsummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAppDiscoveryappProfiles - errors', () => {
      it('should have a postAppDiscoveryappProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postAppDiscoveryappProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AppProfile', (done) => {
        try {
          a.postAppDiscoveryappProfiles(null, (data, error) => {
            try {
              const displayE = 'AppProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAppDiscoveryappProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoveryappProfiles - errors', () => {
      it('should have a getAppDiscoveryappProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoveryappProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoverysessionssessionIdreportappProfileAndAppInfoformatcsv - errors', () => {
      it('should have a getAppDiscoverysessionssessionIdreportappProfileAndAppInfoformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoverysessionssessionIdreportappProfileAndAppInfoformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.getAppDiscoverysessionssessionIdreportappProfileAndAppInfoformatcsv(null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoverysessionssessionIdreportappProfileAndAppInfoformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppDiscoverysessionssessionId - errors', () => {
      it('should have a deleteAppDiscoverysessionssessionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppDiscoverysessionssessionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.deleteAppDiscoverysessionssessionId(null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteAppDiscoverysessionssessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoverysessionssessionId - errors', () => {
      it('should have a getAppDiscoverysessionssessionId function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoverysessionssessionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.getAppDiscoverysessionssessionId(null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoverysessionssessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoverysessionssessionIdappProfiles - errors', () => {
      it('should have a getAppDiscoverysessionssessionIdappProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoverysessionssessionIdappProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.getAppDiscoverysessionssessionIdappProfiles(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoverysessionssessionIdappProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoverysessionssessionIdnsGroupsnsGroupIdmembers - errors', () => {
      it('should have a getAppDiscoverysessionssessionIdnsGroupsnsGroupIdmembers function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoverysessionssessionIdnsGroupsnsGroupIdmembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionid', (done) => {
        try {
          a.getAppDiscoverysessionssessionIdnsGroupsnsGroupIdmembers(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoverysessionssessionIdnsGroupsnsGroupIdmembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.getAppDiscoverysessionssessionIdnsGroupsnsGroupIdmembers('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoverysessionssessionIdnsGroupsnsGroupIdmembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAppDiscoveryappProfilesappProfileId - errors', () => {
      it('should have a putAppDiscoveryappProfilesappProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putAppDiscoveryappProfilesappProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appprofileid', (done) => {
        try {
          a.putAppDiscoveryappProfilesappProfileId(null, null, (data, error) => {
            try {
              const displayE = 'appprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putAppDiscoveryappProfilesappProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AppProfile', (done) => {
        try {
          a.putAppDiscoveryappProfilesappProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'AppProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putAppDiscoveryappProfilesappProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoveryappProfilesappProfileId - errors', () => {
      it('should have a getAppDiscoveryappProfilesappProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoveryappProfilesappProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appprofileid', (done) => {
        try {
          a.getAppDiscoveryappProfilesappProfileId(null, (data, error) => {
            try {
              const displayE = 'appprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAppDiscoveryappProfilesappProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppDiscoveryappProfilesappProfileId - errors', () => {
      it('should have a deleteAppDiscoveryappProfilesappProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppDiscoveryappProfilesappProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appprofileid', (done) => {
        try {
          a.deleteAppDiscoveryappProfilesappProfileId(null, null, (data, error) => {
            try {
              const displayE = 'appprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteAppDiscoveryappProfilesappProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAppDiscoverysessions - errors', () => {
      it('should have a postAppDiscoverysessions function', (done) => {
        try {
          assert.equal(true, typeof a.postAppDiscoverysessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing StartAppDiscoverySessionParameters', (done) => {
        try {
          a.postAppDiscoverysessions(null, (data, error) => {
            try {
              const displayE = 'StartAppDiscoverySessionParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAppDiscoverysessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppDiscoverysessions - errors', () => {
      it('should have a getAppDiscoverysessions function', (done) => {
        try {
          assert.equal(true, typeof a.getAppDiscoverysessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMacSetsmacSetIdmembersmacAddress - errors', () => {
      it('should have a deleteMacSetsmacSetIdmembersmacAddress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMacSetsmacSetIdmembersmacAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macsetid', (done) => {
        try {
          a.deleteMacSetsmacSetIdmembersmacAddress(null, null, (data, error) => {
            try {
              const displayE = 'macsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteMacSetsmacSetIdmembersmacAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macaddress', (done) => {
        try {
          a.deleteMacSetsmacSetIdmembersmacAddress('fakeparam', null, (data, error) => {
            try {
              const displayE = 'macaddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteMacSetsmacSetIdmembersmacAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNsServicesnsServiceId - errors', () => {
      it('should have a putNsServicesnsServiceId function', (done) => {
        try {
          assert.equal(true, typeof a.putNsServicesnsServiceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsserviceid', (done) => {
        try {
          a.putNsServicesnsServiceId(null, null, (data, error) => {
            try {
              const displayE = 'nsserviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNsServicesnsServiceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NSService', (done) => {
        try {
          a.putNsServicesnsServiceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'NSService is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNsServicesnsServiceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsServicesnsServiceId - errors', () => {
      it('should have a deleteNsServicesnsServiceId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsServicesnsServiceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsserviceid', (done) => {
        try {
          a.deleteNsServicesnsServiceId(null, null, (data, error) => {
            try {
              const displayE = 'nsserviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNsServicesnsServiceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsServicesnsServiceId - errors', () => {
      it('should have a getNsServicesnsServiceId function', (done) => {
        try {
          assert.equal(true, typeof a.getNsServicesnsServiceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsserviceid', (done) => {
        try {
          a.getNsServicesnsServiceId(null, (data, error) => {
            try {
              const displayE = 'nsserviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsServicesnsServiceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMacSetsmacSetIdmembers - errors', () => {
      it('should have a postMacSetsmacSetIdmembers function', (done) => {
        try {
          assert.equal(true, typeof a.postMacSetsmacSetIdmembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macsetid', (done) => {
        try {
          a.postMacSetsmacSetIdmembers(null, null, (data, error) => {
            try {
              const displayE = 'macsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postMacSetsmacSetIdmembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MACAddressElement', (done) => {
        try {
          a.postMacSetsmacSetIdmembers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'MACAddressElement is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postMacSetsmacSetIdmembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMacSetsmacSetIdmembers - errors', () => {
      it('should have a getMacSetsmacSetIdmembers function', (done) => {
        try {
          assert.equal(true, typeof a.getMacSetsmacSetIdmembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macsetid', (done) => {
        try {
          a.getMacSetsmacSetIdmembers(null, (data, error) => {
            try {
              const displayE = 'macsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getMacSetsmacSetIdmembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupsnsgroupIdserviceAssociations - errors', () => {
      it('should have a getNsGroupsnsgroupIdserviceAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupsnsgroupIdserviceAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.getNsGroupsnsgroupIdserviceAssociations(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsGroupsnsgroupIdserviceAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceType', (done) => {
        try {
          a.getNsGroupsnsgroupIdserviceAssociations('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'serviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsGroupsnsgroupIdserviceAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupsnsGroupIdeffectiveIpAddressMembers - errors', () => {
      it('should have a getNsGroupsnsGroupIdeffectiveIpAddressMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupsnsGroupIdeffectiveIpAddressMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.getNsGroupsnsGroupIdeffectiveIpAddressMembers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsGroupsnsGroupIdeffectiveIpAddressMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpSets - errors', () => {
      it('should have a postIpSets function', (done) => {
        try {
          assert.equal(true, typeof a.postIpSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IPSet', (done) => {
        try {
          a.postIpSets(null, (data, error) => {
            try {
              const displayE = 'IPSet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postIpSets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpSets - errors', () => {
      it('should have a getIpSets function', (done) => {
        try {
          assert.equal(true, typeof a.getIpSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupsnsGroupIdeffectiveLogicalSwitchMembers - errors', () => {
      it('should have a getNsGroupsnsGroupIdeffectiveLogicalSwitchMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupsnsGroupIdeffectiveLogicalSwitchMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.getNsGroupsnsGroupIdeffectiveLogicalSwitchMembers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsGroupsnsGroupIdeffectiveLogicalSwitchMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupsnsGroupIdmemberTypes - errors', () => {
      it('should have a getNsGroupsnsGroupIdmemberTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupsnsGroupIdmemberTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.getNsGroupsnsGroupIdmemberTypes(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsGroupsnsGroupIdmemberTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNsGroupsnsGroupId - errors', () => {
      it('should have a putNsGroupsnsGroupId function', (done) => {
        try {
          assert.equal(true, typeof a.putNsGroupsnsGroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.putNsGroupsnsGroupId(null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNsGroupsnsGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NSGroup', (done) => {
        try {
          a.putNsGroupsnsGroupId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'NSGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNsGroupsnsGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNsGroupsnsGroupId - errors', () => {
      it('should have a postNsGroupsnsGroupId function', (done) => {
        try {
          assert.equal(true, typeof a.postNsGroupsnsGroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.postNsGroupsnsGroupId(null, null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNsGroupsnsGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NSGroupExpressionList', (done) => {
        try {
          a.postNsGroupsnsGroupId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'NSGroupExpressionList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNsGroupsnsGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.postNsGroupsnsGroupId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNsGroupsnsGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupsnsGroupId - errors', () => {
      it('should have a getNsGroupsnsGroupId function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupsnsGroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.getNsGroupsnsGroupId(null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsGroupsnsGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupsnsGroupId - errors', () => {
      it('should have a deleteNsGroupsnsGroupId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsGroupsnsGroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.deleteNsGroupsnsGroupId(null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNsGroupsnsGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupsnsGroupIdeffectiveVirtualMachineMembers - errors', () => {
      it('should have a getNsGroupsnsGroupIdeffectiveVirtualMachineMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupsnsGroupIdeffectiveVirtualMachineMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.getNsGroupsnsGroupIdeffectiveVirtualMachineMembers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsGroupsnsGroupIdeffectiveVirtualMachineMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNsServiceGroupsnsServiceGroupId - errors', () => {
      it('should have a putNsServiceGroupsnsServiceGroupId function', (done) => {
        try {
          assert.equal(true, typeof a.putNsServiceGroupsnsServiceGroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsservicegroupid', (done) => {
        try {
          a.putNsServiceGroupsnsServiceGroupId(null, null, (data, error) => {
            try {
              const displayE = 'nsservicegroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNsServiceGroupsnsServiceGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NSServiceGroup', (done) => {
        try {
          a.putNsServiceGroupsnsServiceGroupId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'NSServiceGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNsServiceGroupsnsServiceGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsServiceGroupsnsServiceGroupId - errors', () => {
      it('should have a deleteNsServiceGroupsnsServiceGroupId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsServiceGroupsnsServiceGroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsservicegroupid', (done) => {
        try {
          a.deleteNsServiceGroupsnsServiceGroupId(null, null, (data, error) => {
            try {
              const displayE = 'nsservicegroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNsServiceGroupsnsServiceGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsServiceGroupsnsServiceGroupId - errors', () => {
      it('should have a getNsServiceGroupsnsServiceGroupId function', (done) => {
        try {
          assert.equal(true, typeof a.getNsServiceGroupsnsServiceGroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsservicegroupid', (done) => {
        try {
          a.getNsServiceGroupsnsServiceGroupId(null, (data, error) => {
            try {
              const displayE = 'nsservicegroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsServiceGroupsnsServiceGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpSetsipSetId - errors', () => {
      it('should have a putIpSetsipSetId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpSetsipSetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsetid', (done) => {
        try {
          a.putIpSetsipSetId(null, null, (data, error) => {
            try {
              const displayE = 'ipsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putIpSetsipSetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IPSet', (done) => {
        try {
          a.putIpSetsipSetId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'IPSet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putIpSetsipSetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpSetsipSetId - errors', () => {
      it('should have a deleteIpSetsipSetId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpSetsipSetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsetid', (done) => {
        try {
          a.deleteIpSetsipSetId(null, null, (data, error) => {
            try {
              const displayE = 'ipsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteIpSetsipSetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpSetsipSetId - errors', () => {
      it('should have a getIpSetsipSetId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpSetsipSetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsetid', (done) => {
        try {
          a.getIpSetsipSetId(null, (data, error) => {
            try {
              const displayE = 'ipsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getIpSetsipSetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putMacSetsmacSetId - errors', () => {
      it('should have a putMacSetsmacSetId function', (done) => {
        try {
          assert.equal(true, typeof a.putMacSetsmacSetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macsetid', (done) => {
        try {
          a.putMacSetsmacSetId(null, null, (data, error) => {
            try {
              const displayE = 'macsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putMacSetsmacSetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MACSet', (done) => {
        try {
          a.putMacSetsmacSetId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'MACSet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putMacSetsmacSetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMacSetsmacSetId - errors', () => {
      it('should have a deleteMacSetsmacSetId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMacSetsmacSetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macsetid', (done) => {
        try {
          a.deleteMacSetsmacSetId(null, null, (data, error) => {
            try {
              const displayE = 'macsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteMacSetsmacSetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMacSetsmacSetId - errors', () => {
      it('should have a getMacSetsmacSetId function', (done) => {
        try {
          assert.equal(true, typeof a.getMacSetsmacSetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macsetid', (done) => {
        try {
          a.getMacSetsmacSetId(null, (data, error) => {
            try {
              const displayE = 'macsetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getMacSetsmacSetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNsServices - errors', () => {
      it('should have a postNsServices function', (done) => {
        try {
          assert.equal(true, typeof a.postNsServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NSService', (done) => {
        try {
          a.postNsServices(null, (data, error) => {
            try {
              const displayE = 'NSService is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNsServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsServices - errors', () => {
      it('should have a getNsServices function', (done) => {
        try {
          assert.equal(true, typeof a.getNsServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupsunassociatedVirtualMachines - errors', () => {
      it('should have a getNsGroupsunassociatedVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupsunassociatedVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMacSets - errors', () => {
      it('should have a postMacSets function', (done) => {
        try {
          assert.equal(true, typeof a.postMacSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MACSet', (done) => {
        try {
          a.postMacSets(null, (data, error) => {
            try {
              const displayE = 'MACSet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postMacSets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMacSets - errors', () => {
      it('should have a getMacSets function', (done) => {
        try {
          assert.equal(true, typeof a.getMacSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupsnsGroupIdeffectiveLogicalPortMembers - errors', () => {
      it('should have a getNsGroupsnsGroupIdeffectiveLogicalPortMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupsnsGroupIdeffectiveLogicalPortMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupid', (done) => {
        try {
          a.getNsGroupsnsGroupIdeffectiveLogicalPortMembers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nsgroupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNsGroupsnsGroupIdeffectiveLogicalPortMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNsServiceGroups - errors', () => {
      it('should have a postNsServiceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postNsServiceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NSServiceGroup', (done) => {
        try {
          a.postNsServiceGroups(null, (data, error) => {
            try {
              const displayE = 'NSServiceGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNsServiceGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsServiceGroups - errors', () => {
      it('should have a getNsServiceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getNsServiceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNsGroups - errors', () => {
      it('should have a postNsGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postNsGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NSGroup', (done) => {
        try {
          a.postNsGroups(null, (data, error) => {
            try {
              const displayE = 'NSGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNsGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroups - errors', () => {
      it('should have a getNsGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpfixObsPointsswitchGlobal - errors', () => {
      it('should have a putIpfixObsPointsswitchGlobal function', (done) => {
        try {
          assert.equal(true, typeof a.putIpfixObsPointsswitchGlobal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpfixObsPointConfig', (done) => {
        try {
          a.putIpfixObsPointsswitchGlobal(null, (data, error) => {
            try {
              const displayE = 'IpfixObsPointConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putIpfixObsPointsswitchGlobal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpfixObsPointsswitchGlobal - errors', () => {
      it('should have a getIpfixObsPointsswitchGlobal function', (done) => {
        try {
          assert.equal(true, typeof a.getIpfixObsPointsswitchGlobal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTraceflowstraceflowId - errors', () => {
      it('should have a deleteTraceflowstraceflowId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTraceflowstraceflowId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing traceflowid', (done) => {
        try {
          a.deleteTraceflowstraceflowId(null, (data, error) => {
            try {
              const displayE = 'traceflowid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTraceflowstraceflowId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTraceflowstraceflowId - errors', () => {
      it('should have a getTraceflowstraceflowId function', (done) => {
        try {
          assert.equal(true, typeof a.getTraceflowstraceflowId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing traceflowid', (done) => {
        try {
          a.getTraceflowstraceflowId(null, (data, error) => {
            try {
              const displayE = 'traceflowid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTraceflowstraceflowId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZoneszoneIdtransportNodeStatus - errors', () => {
      it('should have a getTransportZoneszoneIdtransportNodeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZoneszoneIdtransportNodeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.getTransportZoneszoneIdtransportNodeStatus(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportZoneszoneIdtransportNodeStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTraceflows - errors', () => {
      it('should have a postTraceflows function', (done) => {
        try {
          assert.equal(true, typeof a.postTraceflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TraceflowRequest', (done) => {
        try {
          a.postTraceflows(null, (data, error) => {
            try {
              const displayE = 'TraceflowRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTraceflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTraceflows - errors', () => {
      it('should have a getTraceflows function', (done) => {
        try {
          assert.equal(true, typeof a.getTraceflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodelogslogName - errors', () => {
      it('should have a getNodelogslogName function', (done) => {
        try {
          assert.equal(true, typeof a.getNodelogslogName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logname', (done) => {
        try {
          a.getNodelogslogName(null, (data, error) => {
            try {
              const displayE = 'logname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodelogslogName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodelogslogNamedata - errors', () => {
      it('should have a getNodelogslogNamedata function', (done) => {
        try {
          assert.equal(true, typeof a.getNodelogslogNamedata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logname', (done) => {
        try {
          a.getNodelogslogNamedata(null, (data, error) => {
            try {
              const displayE = 'logname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodelogslogNamedata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodesnodeIdpnicBondStatus - errors', () => {
      it('should have a getTransportNodesnodeIdpnicBondStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodesnodeIdpnicBondStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getTransportNodesnodeIdpnicBondStatus(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodesnodeIdpnicBondStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodelogs - errors', () => {
      it('should have a getNodelogs function', (done) => {
        try {
          assert.equal(true, typeof a.getNodelogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodesstatus - errors', () => {
      it('should have a getTransportNodesstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodesstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodesnodeIdremoteTransportNodeStatus - errors', () => {
      it('should have a getTransportNodesnodeIdremoteTransportNodeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodesnodeIdremoteTransportNodeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getTransportNodesnodeIdremoteTransportNodeStatus(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodesnodeIdremoteTransportNodeStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodesnodeIdstatus - errors', () => {
      it('should have a getTransportNodesnodeIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodesnodeIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getTransportNodesnodeIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodesnodeIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMirrorSessions - errors', () => {
      it('should have a postMirrorSessions function', (done) => {
        try {
          assert.equal(true, typeof a.postMirrorSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing PortMirroringSession', (done) => {
        try {
          a.postMirrorSessions(null, (data, error) => {
            try {
              const displayE = 'PortMirroringSession is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postMirrorSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMirrorSessions - errors', () => {
      it('should have a getMirrorSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getMirrorSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putMirrorSessionsmirrorSessionId - errors', () => {
      it('should have a putMirrorSessionsmirrorSessionId function', (done) => {
        try {
          assert.equal(true, typeof a.putMirrorSessionsmirrorSessionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mirrorsessionid', (done) => {
        try {
          a.putMirrorSessionsmirrorSessionId(null, null, (data, error) => {
            try {
              const displayE = 'mirrorsessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putMirrorSessionsmirrorSessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing PortMirroringSession', (done) => {
        try {
          a.putMirrorSessionsmirrorSessionId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'PortMirroringSession is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putMirrorSessionsmirrorSessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMirrorSessionsmirrorSessionId - errors', () => {
      it('should have a getMirrorSessionsmirrorSessionId function', (done) => {
        try {
          assert.equal(true, typeof a.getMirrorSessionsmirrorSessionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mirrorsessionid', (done) => {
        try {
          a.getMirrorSessionsmirrorSessionId(null, (data, error) => {
            try {
              const displayE = 'mirrorsessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getMirrorSessionsmirrorSessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMirrorSessionsmirrorSessionId - errors', () => {
      it('should have a deleteMirrorSessionsmirrorSessionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMirrorSessionsmirrorSessionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mirrorsessionid', (done) => {
        try {
          a.deleteMirrorSessionsmirrorSessionId(null, (data, error) => {
            try {
              const displayE = 'mirrorsessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteMirrorSessionsmirrorSessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalPortslportIdforwardingPath - errors', () => {
      it('should have a getLogicalPortslportIdforwardingPath function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalPortslportIdforwardingPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lportid', (done) => {
        try {
          a.getLogicalPortslportIdforwardingPath(null, null, (data, error) => {
            try {
              const displayE = 'lportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalPortslportIdforwardingPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peerPortId', (done) => {
        try {
          a.getLogicalPortslportIdforwardingPath('fakeparam', null, (data, error) => {
            try {
              const displayE = 'peerPortId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalPortslportIdforwardingPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTraceflowstraceflowIdobservations - errors', () => {
      it('should have a getTraceflowstraceflowIdobservations function', (done) => {
        try {
          assert.equal(true, typeof a.getTraceflowstraceflowIdobservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing traceflowid', (done) => {
        try {
          a.getTraceflowstraceflowIdobservations(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'traceflowid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTraceflowstraceflowIdobservations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZonesstatus - errors', () => {
      it('should have a getTransportZonesstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZonesstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpfixObsPoints - errors', () => {
      it('should have a getIpfixObsPoints function', (done) => {
        try {
          assert.equal(true, typeof a.getIpfixObsPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZoneszoneIdstatus - errors', () => {
      it('should have a getTransportZoneszoneIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZoneszoneIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.getTransportZoneszoneIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportZoneszoneIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZoneszoneIdtransportNodeStatusReport - errors', () => {
      it('should have a getTransportZoneszoneIdtransportNodeStatusReport function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZoneszoneIdtransportNodeStatusReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.getTransportZoneszoneIdtransportNodeStatusReport(null, null, null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportZoneszoneIdtransportNodeStatusReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZonestransportNodeStatusReport - errors', () => {
      it('should have a getTransportZonestransportNodeStatusReport function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZonestransportNodeStatusReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMirrorSessionsmirrorSessionIdactionverify - errors', () => {
      it('should have a postMirrorSessionsmirrorSessionIdactionverify function', (done) => {
        try {
          assert.equal(true, typeof a.postMirrorSessionsmirrorSessionIdactionverify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mirrorsessionid', (done) => {
        try {
          a.postMirrorSessionsmirrorSessionIdactionverify(null, (data, error) => {
            try {
              const displayE = 'mirrorsessionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postMirrorSessionsmirrorSessionIdactionverify', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZonestransportNodeStatus - errors', () => {
      it('should have a getTransportZonestransportNodeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZonestransportNodeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterslogicalRouterIdroutingstaticRoutes - errors', () => {
      it('should have a postLogicalRouterslogicalRouterIdroutingstaticRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterslogicalRouterIdroutingstaticRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingstaticRoutes(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingstaticRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing StaticRoute', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingstaticRoutes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'StaticRoute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingstaticRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingstaticRoutes - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingstaticRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingstaticRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingstaticRoutes(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingstaticRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouters - errors', () => {
      it('should have a postLogicalRouters function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalRouter', (done) => {
        try {
          a.postLogicalRouters(null, (data, error) => {
            try {
              const displayE = 'LogicalRouter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouters - errors', () => {
      it('should have a getLogicalRouters function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingforwardingTable - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingforwardingTable function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingforwardingTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingforwardingTable(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingforwardingTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportNodeId', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingforwardingTable('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'transportNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingforwardingTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterslogicalRouterIdroutingrouteMaps - errors', () => {
      it('should have a postLogicalRouterslogicalRouterIdroutingrouteMaps function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterslogicalRouterIdroutingrouteMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingrouteMaps(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingrouteMaps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RouteMap', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingrouteMaps('fakeparam', null, (data, error) => {
            try {
              const displayE = 'RouteMap is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingrouteMaps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingrouteMaps - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingrouteMaps function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingrouteMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingrouteMaps(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingrouteMaps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdnatrulesruleIdstatistics - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdnatrulesruleIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdnatrulesruleIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdnatrulesruleIdstatistics(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdnatrulesruleIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdnatrulesruleIdstatistics('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdnatrulesruleIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingrouteTableformatcsv - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingrouteTableformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingrouteTableformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingrouteTableformatcsv(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingrouteTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportNodeId', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingrouteTableformatcsv('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'transportNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingrouteTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bfdpeerid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bfdpeerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing StaticHopBfdPeer', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'StaticHopBfdPeer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bfdpeerid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bfdpeerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId - errors', () => {
      it('should have a deleteLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bfdpeerid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bfdpeerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeersbfdPeerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdstatus - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterslogicalRouterIdroutingipPrefixLists - errors', () => {
      it('should have a postLogicalRouterslogicalRouterIdroutingipPrefixLists function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterslogicalRouterIdroutingipPrefixLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingipPrefixLists(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingipPrefixLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IPPrefixList', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingipPrefixLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'IPPrefixList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingipPrefixLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingipPrefixLists - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingipPrefixLists function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingipPrefixLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingipPrefixLists(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingipPrefixLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingredistribution - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingredistribution function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingredistribution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingredistribution(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingredistribution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RedistributionConfig', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingredistribution('fakeparam', null, (data, error) => {
            try {
              const displayE = 'RedistributionConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingredistribution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingredistribution - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingredistribution function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingredistribution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingredistribution(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingredistribution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDhcprelaysrelayId - errors', () => {
      it('should have a putDhcprelaysrelayId function', (done) => {
        try {
          assert.equal(true, typeof a.putDhcprelaysrelayId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relayid', (done) => {
        try {
          a.putDhcprelaysrelayId(null, null, (data, error) => {
            try {
              const displayE = 'relayid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcprelaysrelayId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpRelayService', (done) => {
        try {
          a.putDhcprelaysrelayId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DhcpRelayService is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcprelaysrelayId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcprelaysrelayId - errors', () => {
      it('should have a getDhcprelaysrelayId function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcprelaysrelayId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relayid', (done) => {
        try {
          a.getDhcprelaysrelayId(null, (data, error) => {
            try {
              const displayE = 'relayid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcprelaysrelayId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcprelaysrelayId - errors', () => {
      it('should have a deleteDhcprelaysrelayId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcprelaysrelayId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relayid', (done) => {
        try {
          a.deleteDhcprelaysrelayId(null, (data, error) => {
            try {
              const displayE = 'relayid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteDhcprelaysrelayId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingstaticRoutesid - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingstaticRoutesid function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingstaticRoutesid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingstaticRoutesid(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingstaticRoutesid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingstaticRoutesid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingstaticRoutesid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing StaticRoute', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingstaticRoutesid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'StaticRoute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingstaticRoutesid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingstaticRoutesid - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingstaticRoutesid function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingstaticRoutesid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingstaticRoutesid(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingstaticRoutesid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingstaticRoutesid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingstaticRoutesid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalRouterslogicalRouterIdroutingstaticRoutesid - errors', () => {
      it('should have a deleteLogicalRouterslogicalRouterIdroutingstaticRoutesid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalRouterslogicalRouterIdroutingstaticRoutesid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingstaticRoutesid(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingstaticRoutesid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingstaticRoutesid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingstaticRoutesid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDhcprelayProfilesrelayProfileId - errors', () => {
      it('should have a putDhcprelayProfilesrelayProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putDhcprelayProfilesrelayProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relayprofileid', (done) => {
        try {
          a.putDhcprelayProfilesrelayProfileId(null, null, (data, error) => {
            try {
              const displayE = 'relayprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcprelayProfilesrelayProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpRelayProfile', (done) => {
        try {
          a.putDhcprelayProfilesrelayProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DhcpRelayProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcprelayProfilesrelayProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcprelayProfilesrelayProfileId - errors', () => {
      it('should have a deleteDhcprelayProfilesrelayProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcprelayProfilesrelayProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relayprofileid', (done) => {
        try {
          a.deleteDhcprelayProfilesrelayProfileId(null, (data, error) => {
            try {
              const displayE = 'relayprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteDhcprelayProfilesrelayProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcprelayProfilesrelayProfileId - errors', () => {
      it('should have a getDhcprelayProfilesrelayProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcprelayProfilesrelayProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relayprofileid', (done) => {
        try {
          a.getDhcprelayProfilesrelayProfileId(null, (data, error) => {
            try {
              const displayE = 'relayprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcprelayProfilesrelayProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterslogicalRouterIdactionreprocess - errors', () => {
      it('should have a postLogicalRouterslogicalRouterIdactionreprocess function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterslogicalRouterIdactionreprocess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdactionreprocess(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdactionreprocess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIddebugInfoformattext - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIddebugInfoformattext function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIddebugInfoformattext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIddebugInfoformattext(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIddebugInfoformattext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingbgpneighborsstatus - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingbgpneighborsstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingbgpneighborsstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingbgpneighborsstatus(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingbgpneighborsstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingredistributionrules - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingredistributionrules function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingredistributionrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingredistributionrules(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingredistributionrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RedistributionRuleList', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingredistributionrules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'RedistributionRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingredistributionrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingredistributionrules - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingredistributionrules function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingredistributionrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingredistributionrules(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingredistributionrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postServices - errors', () => {
      it('should have a postServices function', (done) => {
        try {
          assert.equal(true, typeof a.postServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalService', (done) => {
        try {
          a.postServices(null, (data, error) => {
            try {
              const displayE = 'LogicalService is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServices - errors', () => {
      it('should have a getServices function', (done) => {
        try {
          assert.equal(true, typeof a.getServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDhcprelayProfiles - errors', () => {
      it('should have a postDhcprelayProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postDhcprelayProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpRelayProfile', (done) => {
        try {
          a.postDhcprelayProfiles(null, (data, error) => {
            try {
              const displayE = 'DhcpRelayProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postDhcprelayProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcprelayProfiles - errors', () => {
      it('should have a getDhcprelayProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcprelayProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingrouteTable - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingrouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingrouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingrouteTable(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingrouteTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportNodeId', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingrouteTable('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'transportNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingrouteTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterslogicalRouterIdroutingbgpneighbors - errors', () => {
      it('should have a postLogicalRouterslogicalRouterIdroutingbgpneighbors function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterslogicalRouterIdroutingbgpneighbors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingbgpneighbors(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingbgpneighbors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BgpNeighbor', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingbgpneighbors('fakeparam', null, (data, error) => {
            try {
              const displayE = 'BgpNeighbor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingbgpneighbors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingbgpneighbors - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingbgpneighbors function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingbgpneighbors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingbgpneighbors(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingbgpneighbors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingforwardingTableformatcsv - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingforwardingTableformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingforwardingTableformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingforwardingTableformatcsv(null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingforwardingTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportNodeId', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingforwardingTableformatcsv('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'transportNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingforwardingTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingroutingTable - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingroutingTable function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingroutingTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingroutingTable(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingroutingTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportNodeId', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingroutingTable('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'transportNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingroutingTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterPortslogicalRouterPortIdarpTable - errors', () => {
      it('should have a getLogicalRouterPortslogicalRouterPortIdarpTable function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterPortslogicalRouterPortIdarpTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterportid', (done) => {
        try {
          a.getLogicalRouterPortslogicalRouterPortIdarpTable(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterPortslogicalRouterPortIdarpTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterPortslogicalRouterPortIdstatisticssummary - errors', () => {
      it('should have a getLogicalRouterPortslogicalRouterPortIdstatisticssummary function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterPortslogicalRouterPortIdstatisticssummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterportid', (done) => {
        try {
          a.getLogicalRouterPortslogicalRouterPortIdstatisticssummary(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterPortslogicalRouterPortIdstatisticssummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postServiceProfiles - errors', () => {
      it('should have a postServiceProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postServiceProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ServiceProfile', (done) => {
        try {
          a.postServiceProfiles(null, (data, error) => {
            try {
              const displayE = 'ServiceProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postServiceProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceProfiles - errors', () => {
      it('should have a getServiceProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterPorts - errors', () => {
      it('should have a postLogicalRouterPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalRouterPort', (done) => {
        try {
          a.postLogicalRouterPorts(null, (data, error) => {
            try {
              const displayE = 'LogicalRouterPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterPorts - errors', () => {
      it('should have a getLogicalRouterPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdnatrulesstatistics - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdnatrulesstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdnatrulesstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdnatrulesstatistics(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdnatrulesstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodesnodeIdstatisticsnatRules - errors', () => {
      it('should have a getTransportNodesnodeIdstatisticsnatRules function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodesnodeIdstatisticsnatRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getTransportNodesnodeIdstatisticsnatRules(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodesnodeIdstatisticsnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers - errors', () => {
      it('should have a postLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing StaticHopBfdPeer', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'StaticHopBfdPeer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingstaticRoutesbfdPeers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterId - errors', () => {
      it('should have a putLogicalRouterslogicalRouterId function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterId(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalRouter', (done) => {
        try {
          a.putLogicalRouterslogicalRouterId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LogicalRouter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterId - errors', () => {
      it('should have a getLogicalRouterslogicalRouterId function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterId(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalRouterslogicalRouterId - errors', () => {
      it('should have a deleteLogicalRouterslogicalRouterId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalRouterslogicalRouterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterId(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingadvertisementrules - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingadvertisementrules function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingadvertisementrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingadvertisementrules(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingadvertisementrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AdvertiseRuleList', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingadvertisementrules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'AdvertiseRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingadvertisementrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingadvertisementrules - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingadvertisementrules function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingadvertisementrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingadvertisementrules(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingadvertisementrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingroutingTableformatcsv - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingroutingTableformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingroutingTableformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingroutingTableformatcsv(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingroutingTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportNodeId', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingroutingTableformatcsv('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'transportNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingroutingTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDhcprelays - errors', () => {
      it('should have a postDhcprelays function', (done) => {
        try {
          assert.equal(true, typeof a.postDhcprelays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpRelayService', (done) => {
        try {
          a.postDhcprelays(null, (data, error) => {
            try {
              const displayE = 'DhcpRelayService is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postDhcprelays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcprelays - errors', () => {
      it('should have a getDhcprelays function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcprelays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingipPrefixListsid - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingipPrefixListsid function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingipPrefixListsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingipPrefixListsid(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingipPrefixListsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingipPrefixListsid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingipPrefixListsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IPPrefixList', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingipPrefixListsid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'IPPrefixList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingipPrefixListsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingipPrefixListsid - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingipPrefixListsid function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingipPrefixListsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingipPrefixListsid(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingipPrefixListsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingipPrefixListsid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingipPrefixListsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalRouterslogicalRouterIdroutingipPrefixListsid - errors', () => {
      it('should have a deleteLogicalRouterslogicalRouterIdroutingipPrefixListsid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalRouterslogicalRouterIdroutingipPrefixListsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingipPrefixListsid(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingipPrefixListsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingipPrefixListsid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingipPrefixListsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterPortslogicalRouterPortIdstatistics - errors', () => {
      it('should have a getLogicalRouterPortslogicalRouterPortIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterPortslogicalRouterPortIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterportid', (done) => {
        try {
          a.getLogicalRouterPortslogicalRouterPortIdstatistics(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterPortslogicalRouterPortIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingrouteMapsid - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingrouteMapsid function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingrouteMapsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingrouteMapsid(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingrouteMapsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingrouteMapsid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingrouteMapsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RouteMap', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingrouteMapsid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'RouteMap is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingrouteMapsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingrouteMapsid - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingrouteMapsid function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingrouteMapsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingrouteMapsid(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingrouteMapsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingrouteMapsid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingrouteMapsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalRouterslogicalRouterIdroutingrouteMapsid - errors', () => {
      it('should have a deleteLogicalRouterslogicalRouterIdroutingrouteMapsid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalRouterslogicalRouterIdroutingrouteMapsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingrouteMapsid(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingrouteMapsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingrouteMapsid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingrouteMapsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingbgp - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingbgp function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingbgp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingbgp(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingbgp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BgpConfig', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingbgp('fakeparam', null, (data, error) => {
            try {
              const displayE = 'BgpConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingbgp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingbgp - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingbgp function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingbgp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingbgp(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingbgp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdrouting - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdrouting function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdrouting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdrouting(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdrouting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RoutingConfig', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdrouting('fakeparam', null, (data, error) => {
            try {
              const displayE = 'RoutingConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdrouting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdrouting - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdrouting function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdrouting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdrouting(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdrouting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingadvertisement - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingadvertisement function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingadvertisement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingadvertisement(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingadvertisement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AdvertisementConfig', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingadvertisement('fakeparam', null, (data, error) => {
            try {
              const displayE = 'AdvertisementConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingadvertisement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingadvertisement - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingadvertisement function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingadvertisement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingadvertisement(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingadvertisement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putServicesserviceId - errors', () => {
      it('should have a putServicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.putServicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.putServicesserviceId(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putServicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalService', (done) => {
        try {
          a.putServicesserviceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LogicalService is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putServicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesserviceId - errors', () => {
      it('should have a getServicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getServicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getServicesserviceId(null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getServicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicesserviceId - errors', () => {
      it('should have a deleteServicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.deleteServicesserviceId(null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteServicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingbgpneighborsid - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingbgpneighborsid function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingbgpneighborsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingbgpneighborsid(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingbgpneighborsid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BgpNeighbor', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingbgpneighborsid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'BgpNeighbor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterslogicalRouterIdroutingbgpneighborsid - errors', () => {
      it('should have a postLogicalRouterslogicalRouterIdroutingbgpneighborsid function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterslogicalRouterIdroutingbgpneighborsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingbgpneighborsid(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdroutingbgpneighborsid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingbgpneighborsid - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingbgpneighborsid function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingbgpneighborsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingbgpneighborsid(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingbgpneighborsid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalRouterslogicalRouterIdroutingbgpneighborsid - errors', () => {
      it('should have a deleteLogicalRouterslogicalRouterIdroutingbgpneighborsid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalRouterslogicalRouterIdroutingbgpneighborsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingbgpneighborsid(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdroutingbgpneighborsid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdroutingbgpneighborsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdroutingbfdConfig - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdroutingbfdConfig function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdroutingbfdConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingbfdConfig(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingbfdConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BfdConfig', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdroutingbfdConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'BfdConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdroutingbfdConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdroutingbfdConfig - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdroutingbfdConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdroutingbfdConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdroutingbfdConfig(null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdroutingbfdConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putServiceProfilesserviceProfileId - errors', () => {
      it('should have a putServiceProfilesserviceProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putServiceProfilesserviceProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceprofileid', (done) => {
        try {
          a.putServiceProfilesserviceProfileId(null, null, (data, error) => {
            try {
              const displayE = 'serviceprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putServiceProfilesserviceProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ServiceProfile', (done) => {
        try {
          a.putServiceProfilesserviceProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ServiceProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putServiceProfilesserviceProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceProfilesserviceProfileId - errors', () => {
      it('should have a getServiceProfilesserviceProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceProfilesserviceProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceprofileid', (done) => {
        try {
          a.getServiceProfilesserviceProfileId(null, (data, error) => {
            try {
              const displayE = 'serviceprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getServiceProfilesserviceProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceProfilesserviceProfileId - errors', () => {
      it('should have a deleteServiceProfilesserviceProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServiceProfilesserviceProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceprofileid', (done) => {
        try {
          a.deleteServiceProfilesserviceProfileId(null, (data, error) => {
            try {
              const displayE = 'serviceprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteServiceProfilesserviceProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterPortslogicalRouterPortIdarpTableformatcsv - errors', () => {
      it('should have a getLogicalRouterPortslogicalRouterPortIdarpTableformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterPortslogicalRouterPortIdarpTableformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterportid', (done) => {
        try {
          a.getLogicalRouterPortslogicalRouterPortIdarpTableformatcsv(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterPortslogicalRouterPortIdarpTableformatcsv', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterPortslogicalRouterPortId - errors', () => {
      it('should have a putLogicalRouterPortslogicalRouterPortId function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterPortslogicalRouterPortId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterportid', (done) => {
        try {
          a.putLogicalRouterPortslogicalRouterPortId(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterPortslogicalRouterPortId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalRouterPort', (done) => {
        try {
          a.putLogicalRouterPortslogicalRouterPortId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LogicalRouterPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterPortslogicalRouterPortId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterPortslogicalRouterPortId - errors', () => {
      it('should have a getLogicalRouterPortslogicalRouterPortId function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterPortslogicalRouterPortId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterportid', (done) => {
        try {
          a.getLogicalRouterPortslogicalRouterPortId(null, (data, error) => {
            try {
              const displayE = 'logicalrouterportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterPortslogicalRouterPortId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalRouterPortslogicalRouterPortId - errors', () => {
      it('should have a deleteLogicalRouterPortslogicalRouterPortId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalRouterPortslogicalRouterPortId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterportid', (done) => {
        try {
          a.deleteLogicalRouterPortslogicalRouterPortId(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterportid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterPortslogicalRouterPortId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLogicalRouterslogicalRouterIdnatrulesruleId - errors', () => {
      it('should have a putLogicalRouterslogicalRouterIdnatrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.putLogicalRouterslogicalRouterIdnatrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdnatrulesruleId(null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdnatrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdnatrulesruleId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdnatrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NatRule', (done) => {
        try {
          a.putLogicalRouterslogicalRouterIdnatrulesruleId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'NatRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLogicalRouterslogicalRouterIdnatrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdnatrulesruleId - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdnatrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdnatrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdnatrulesruleId(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdnatrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdnatrulesruleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdnatrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLogicalRouterslogicalRouterIdnatrulesruleId - errors', () => {
      it('should have a deleteLogicalRouterslogicalRouterIdnatrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLogicalRouterslogicalRouterIdnatrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdnatrulesruleId(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdnatrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.deleteLogicalRouterslogicalRouterIdnatrulesruleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLogicalRouterslogicalRouterIdnatrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogicalRouterslogicalRouterIdnatrules - errors', () => {
      it('should have a postLogicalRouterslogicalRouterIdnatrules function', (done) => {
        try {
          assert.equal(true, typeof a.postLogicalRouterslogicalRouterIdnatrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdnatrules(null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdnatrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NatRule', (done) => {
        try {
          a.postLogicalRouterslogicalRouterIdnatrules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'NatRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLogicalRouterslogicalRouterIdnatrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalRouterslogicalRouterIdnatrules - errors', () => {
      it('should have a getLogicalRouterslogicalRouterIdnatrules function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalRouterslogicalRouterIdnatrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalrouterid', (done) => {
        try {
          a.getLogicalRouterslogicalRouterIdnatrules(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'logicalrouterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLogicalRouterslogicalRouterIdnatrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusternodesnodeIdnetworkinterfaces - errors', () => {
      it('should have a getClusternodesnodeIdnetworkinterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getClusternodesnodeIdnetworkinterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getClusternodesnodeIdnetworkinterfaces(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusternodesnodeIdnetworkinterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessyslogexporters - errors', () => {
      it('should have a postNodeservicessyslogexporters function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessyslogexporters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeSyslogExporterProperties', (done) => {
        try {
          a.postNodeservicessyslogexporters(null, (data, error) => {
            try {
              const displayE = 'NodeSyslogExporterProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodeservicessyslogexporters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessyslogexporters - errors', () => {
      it('should have a getNodeservicessyslogexporters function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessyslogexporters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicescmInventoryactionstop - errors', () => {
      it('should have a postNodeservicescmInventoryactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicescmInventoryactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodenetwork - errors', () => {
      it('should have a getNodenetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getNodenetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusternodesnodeIdstatus - errors', () => {
      it('should have a getClusternodesnodeIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getClusternodesnodeIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getClusternodesnodeIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusternodesnodeIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesmgmtPlaneBusactionrestart - errors', () => {
      it('should have a postNodeservicesmgmtPlaneBusactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesmgmtPlaneBusactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodenetworknameServers - errors', () => {
      it('should have a putNodenetworknameServers function', (done) => {
        try {
          assert.equal(true, typeof a.putNodenetworknameServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeNameServersProperties', (done) => {
        try {
          a.putNodenetworknameServers(null, (data, error) => {
            try {
              const displayE = 'NodeNameServersProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodenetworknameServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodenetworknameServers - errors', () => {
      it('should have a getNodenetworknameServers function', (done) => {
        try {
          assert.equal(true, typeof a.getNodenetworknameServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusternodes - errors', () => {
      it('should have a postClusternodes function', (done) => {
        try {
          assert.equal(true, typeof a.postClusternodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AddClusterNodeSpec', (done) => {
        try {
          a.postClusternodes(null, null, (data, error) => {
            try {
              const displayE = 'AddClusterNodeSpec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postClusternodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.postClusternodes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postClusternodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusternodes - errors', () => {
      it('should have a getClusternodes function', (done) => {
        try {
          assert.equal(true, typeof a.getClusternodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodefileStorefileNameactioncopyToRemoteFile - errors', () => {
      it('should have a postNodefileStorefileNameactioncopyToRemoteFile function', (done) => {
        try {
          assert.equal(true, typeof a.postNodefileStorefileNameactioncopyToRemoteFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.postNodefileStorefileNameactioncopyToRemoteFile(null, null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodefileStorefileNameactioncopyToRemoteFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CopyToRemoteFileProperties', (done) => {
        try {
          a.postNodefileStorefileNameactioncopyToRemoteFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'CopyToRemoteFileProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodefileStorefileNameactioncopyToRemoteFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodefileStorefileNamedata - errors', () => {
      it('should have a putNodefileStorefileNamedata function', (done) => {
        try {
          assert.equal(true, typeof a.putNodefileStorefileNamedata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.putNodefileStorefileNamedata(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodefileStorefileNamedata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodefileStorefileNamedata - errors', () => {
      it('should have a getNodefileStorefileNamedata function', (done) => {
        try {
          assert.equal(true, typeof a.getNodefileStorefileNamedata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.getNodefileStorefileNamedata(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodefileStorefileNamedata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesnsxUpgradeAgentactionrestart - errors', () => {
      it('should have a postNodeservicesnsxUpgradeAgentactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesnsxUpgradeAgentactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessearchstatus - errors', () => {
      it('should have a getNodeservicessearchstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessearchstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesntpactionrestart - errors', () => {
      it('should have a postNodeservicesntpactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesntpactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesmanageractionrestart - errors', () => {
      it('should have a postNodeservicesmanageractionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesmanageractionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeservicessnmp - errors', () => {
      it('should have a putNodeservicessnmp function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeservicessnmp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeSnmpServiceProperties', (done) => {
        try {
          a.putNodeservicessnmp(null, (data, error) => {
            try {
              const displayE = 'NodeSnmpServiceProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeservicessnmp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessnmp - errors', () => {
      it('should have a getNodeservicessnmp function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessnmp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterrestorestatus - errors', () => {
      it('should have a getClusterrestorestatus function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterrestorestatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagement - errors', () => {
      it('should have a getTrustManagement function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterbackupshistory - errors', () => {
      it('should have a getClusterbackupshistory function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterbackupshistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesliagentactionstop - errors', () => {
      it('should have a postNodeservicesliagentactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesliagentactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeservicesntp - errors', () => {
      it('should have a putNodeservicesntp function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeservicesntp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeNtpServiceProperties', (done) => {
        try {
          a.putNodeservicesntp(null, (data, error) => {
            try {
              const displayE = 'NodeNtpServiceProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeservicesntp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesntp - errors', () => {
      it('should have a getNodeservicesntp function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesntp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeserviceshttpactionstop - errors', () => {
      it('should have a postNodeserviceshttpactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeserviceshttpactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesnsxUpgradeAgentactionstart - errors', () => {
      it('should have a postNodeservicesnsxUpgradeAgentactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesnsxUpgradeAgentactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessshactionstart - errors', () => {
      it('should have a postNodeservicessshactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessshactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesliagent - errors', () => {
      it('should have a getNodeservicesliagent function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesliagent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTrustManagementcrlsactionimport - errors', () => {
      it('should have a postTrustManagementcrlsactionimport function', (done) => {
        try {
          assert.equal(true, typeof a.postTrustManagementcrlsactionimport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CrlObjectData', (done) => {
        try {
          a.postTrustManagementcrlsactionimport(null, (data, error) => {
            try {
              const displayE = 'CrlObjectData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTrustManagementcrlsactionimport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterrestorebackuptimestamps - errors', () => {
      it('should have a getClusterrestorebackuptimestamps function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterrestorebackuptimestamps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesmgmtPlaneBusactionstop - errors', () => {
      it('should have a postNodeservicesmgmtPlaneBusactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesmgmtPlaneBusactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAdministrationsupportBundlesactioncollect - errors', () => {
      it('should have a postAdministrationsupportBundlesactioncollect function', (done) => {
        try {
          assert.equal(true, typeof a.postAdministrationsupportBundlesactioncollect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing SupportBundleRequest', (done) => {
        try {
          a.postAdministrationsupportBundlesactioncollect(null, null, (data, error) => {
            try {
              const displayE = 'SupportBundleRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAdministrationsupportBundlesactioncollect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessnmpactionstop - errors', () => {
      it('should have a postNodeservicessnmpactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessnmpactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusterrestoreactionretry - errors', () => {
      it('should have a postClusterrestoreactionretry function', (done) => {
        try {
          assert.equal(true, typeof a.postClusterrestoreactionretry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesinstallUpgradeactionstart - errors', () => {
      it('should have a postNodeservicesinstallUpgradeactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesinstallUpgradeactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeserviceshttpactionstart - errors', () => {
      it('should have a postNodeserviceshttpactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeserviceshttpactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeactionrestart - errors', () => {
      it('should have a postNodeactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodenetworkinterfacesinterfaceId - errors', () => {
      it('should have a putNodenetworkinterfacesinterfaceId function', (done) => {
        try {
          assert.equal(true, typeof a.putNodenetworkinterfacesinterfaceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceid', (done) => {
        try {
          a.putNodenetworkinterfacesinterfaceId(null, null, (data, error) => {
            try {
              const displayE = 'interfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodenetworkinterfacesinterfaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeNetworkInterfaceProperties', (done) => {
        try {
          a.putNodenetworkinterfacesinterfaceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'NodeNetworkInterfaceProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodenetworkinterfacesinterfaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodenetworkinterfacesinterfaceId - errors', () => {
      it('should have a getNodenetworkinterfacesinterfaceId function', (done) => {
        try {
          assert.equal(true, typeof a.getNodenetworkinterfacesinterfaceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceid', (done) => {
        try {
          a.getNodenetworkinterfacesinterfaceId(null, (data, error) => {
            try {
              const displayE = 'interfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodenetworkinterfacesinterfaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeservicesssh - errors', () => {
      it('should have a putNodeservicesssh function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeservicesssh === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeSshServiceProperties', (done) => {
        try {
          a.putNodeservicesssh(null, (data, error) => {
            try {
              const displayE = 'NodeSshServiceProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeservicesssh', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesssh - errors', () => {
      it('should have a getNodeservicesssh function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesssh === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNoderabbitmqManagementPort - errors', () => {
      it('should have a postNoderabbitmqManagementPort function', (done) => {
        try {
          assert.equal(true, typeof a.postNoderabbitmqManagementPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNoderabbitmqManagementPort - errors', () => {
      it('should have a deleteNoderabbitmqManagementPort function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNoderabbitmqManagementPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNoderabbitmqManagementPort - errors', () => {
      it('should have a getNoderabbitmqManagementPort function', (done) => {
        try {
          assert.equal(true, typeof a.getNoderabbitmqManagementPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodetaskstaskId - errors', () => {
      it('should have a deleteNodetaskstaskId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodetaskstaskId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskid', (done) => {
        try {
          a.deleteNodetaskstaskId(null, (data, error) => {
            try {
              const displayE = 'taskid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNodetaskstaskId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodetaskstaskId - errors', () => {
      it('should have a getNodetaskstaskId function', (done) => {
        try {
          assert.equal(true, typeof a.getNodetaskstaskId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskid', (done) => {
        try {
          a.getNodetaskstaskId(null, null, (data, error) => {
            try {
              const displayE = 'taskid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodetaskstaskId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusteractionsummarizeInventoryToRemote - errors', () => {
      it('should have a postClusteractionsummarizeInventoryToRemote function', (done) => {
        try {
          assert.equal(true, typeof a.postClusteractionsummarizeInventoryToRemote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodetaskstaskIdresponse - errors', () => {
      it('should have a getNodetaskstaskIdresponse function', (done) => {
        try {
          assert.equal(true, typeof a.getNodetaskstaskIdresponse === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskid', (done) => {
        try {
          a.getNodetaskstaskIdresponse(null, (data, error) => {
            try {
              const displayE = 'taskid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodetaskstaskIdresponse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesmgmtPlaneBusstatus - errors', () => {
      it('should have a getNodeservicesmgmtPlaneBusstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesmgmtPlaneBusstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusteractionbackupToRemote - errors', () => {
      it('should have a postClusteractionbackupToRemote function', (done) => {
        try {
          assert.equal(true, typeof a.postClusteractionbackupToRemote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterbackupsstatus - errors', () => {
      it('should have a getClusterbackupsstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterbackupsstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTrustManagementcsrscsrIdactionimport - errors', () => {
      it('should have a postTrustManagementcsrscsrIdactionimport function', (done) => {
        try {
          assert.equal(true, typeof a.postTrustManagementcsrscsrIdactionimport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrid', (done) => {
        try {
          a.postTrustManagementcsrscsrIdactionimport(null, null, (data, error) => {
            try {
              const displayE = 'csrid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTrustManagementcsrscsrIdactionimport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TrustObjectData', (done) => {
        try {
          a.postTrustManagementcsrscsrIdactionimport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'TrustObjectData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTrustManagementcsrscsrIdactionimport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesntpactionstart - errors', () => {
      it('should have a postNodeservicesntpactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesntpactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterstatus - errors', () => {
      it('should have a getClusterstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesmanageractionstart - errors', () => {
      it('should have a postNodeservicesmanageractionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesmanageractionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicescmInventorystatus - errors', () => {
      it('should have a getNodeservicescmInventorystatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicescmInventorystatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeusersuserid - errors', () => {
      it('should have a putNodeusersuserid function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeusersuserid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.putNodeusersuserid(null, null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeusersuserid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeUserProperties', (done) => {
        try {
          a.putNodeusersuserid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'NodeUserProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeusersuserid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeusersuserid - errors', () => {
      it('should have a getNodeusersuserid function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeusersuserid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.getNodeusersuserid(null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodeusersuserid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicescmInventoryactionstart - errors', () => {
      it('should have a postNodeservicescmInventoryactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicescmInventoryactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesnsxMessageBusactionstart - errors', () => {
      it('should have a postNodeservicesnsxMessageBusactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesnsxMessageBusactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusternodesnodeIdnetworkinterfacesinterfaceIdstats - errors', () => {
      it('should have a getClusternodesnodeIdnetworkinterfacesinterfaceIdstats function', (done) => {
        try {
          assert.equal(true, typeof a.getClusternodesnodeIdnetworkinterfacesinterfaceIdstats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getClusternodesnodeIdnetworkinterfacesinterfaceIdstats(null, null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusternodesnodeIdnetworkinterfacesinterfaceIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceid', (done) => {
        try {
          a.getClusternodesnodeIdnetworkinterfacesinterfaceIdstats('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusternodesnodeIdnetworkinterfacesinterfaceIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusternodesactionrevokeMissingNodes - errors', () => {
      it('should have a postClusternodesactionrevokeMissingNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postClusternodesactionrevokeMissingNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RevokeNodeRequest', (done) => {
        try {
          a.postClusternodesactionrevokeMissingNodes(null, (data, error) => {
            try {
              const displayE = 'RevokeNodeRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postClusternodesactionrevokeMissingNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putConfigsmanagement - errors', () => {
      it('should have a putConfigsmanagement function', (done) => {
        try {
          assert.equal(true, typeof a.putConfigsmanagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ManagementConfig', (done) => {
        try {
          a.putConfigsmanagement(null, (data, error) => {
            try {
              const displayE = 'ManagementConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putConfigsmanagement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigsmanagement - errors', () => {
      it('should have a getConfigsmanagement function', (done) => {
        try {
          assert.equal(true, typeof a.getConfigsmanagement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesntpstatus - errors', () => {
      it('should have a getNodeservicesntpstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesntpstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterrestoreinstructionResources - errors', () => {
      it('should have a getClusterrestoreinstructionResources function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterrestoreinstructionResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instructionId', (done) => {
        try {
          a.getClusterrestoreinstructionResources('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'instructionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusterrestoreinstructionResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagementcertificates - errors', () => {
      it('should have a getTrustManagementcertificates function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagementcertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagementcertificatescertId - errors', () => {
      it('should have a getTrustManagementcertificatescertId function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagementcertificatescertId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certid', (done) => {
        try {
          a.getTrustManagementcertificatescertId(null, null, (data, error) => {
            try {
              const displayE = 'certid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTrustManagementcertificatescertId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrustManagementcertificatescertId - errors', () => {
      it('should have a deleteTrustManagementcertificatescertId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTrustManagementcertificatescertId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certid', (done) => {
        try {
          a.deleteTrustManagementcertificatescertId(null, (data, error) => {
            try {
              const displayE = 'certid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTrustManagementcertificatescertId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodenetworksearchDomains - errors', () => {
      it('should have a putNodenetworksearchDomains function', (done) => {
        try {
          assert.equal(true, typeof a.putNodenetworksearchDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeSearchDomainsProperties', (done) => {
        try {
          a.putNodenetworksearchDomains(null, (data, error) => {
            try {
              const displayE = 'NodeSearchDomainsProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodenetworksearchDomains', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodenetworksearchDomains - errors', () => {
      it('should have a getNodenetworksearchDomains function', (done) => {
        try {
          assert.equal(true, typeof a.getNodenetworksearchDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesnodeMgmtactionrestart - errors', () => {
      it('should have a postNodeservicesnodeMgmtactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesnodeMgmtactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessshactionrestart - errors', () => {
      it('should have a postNodeservicessshactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessshactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessyslogexportersexporterName - errors', () => {
      it('should have a getNodeservicessyslogexportersexporterName function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessyslogexportersexporterName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exportername', (done) => {
        try {
          a.getNodeservicessyslogexportersexporterName(null, (data, error) => {
            try {
              const displayE = 'exportername is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodeservicessyslogexportersexporterName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeservicessyslogexportersexporterName - errors', () => {
      it('should have a deleteNodeservicessyslogexportersexporterName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodeservicessyslogexportersexporterName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exportername', (done) => {
        try {
          a.deleteNodeservicessyslogexportersexporterName(null, (data, error) => {
            try {
              const displayE = 'exportername is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNodeservicessyslogexportersexporterName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusternodesnodeIdnetworkinterfacesinterfaceId - errors', () => {
      it('should have a getClusternodesnodeIdnetworkinterfacesinterfaceId function', (done) => {
        try {
          assert.equal(true, typeof a.getClusternodesnodeIdnetworkinterfacesinterfaceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getClusternodesnodeIdnetworkinterfacesinterfaceId(null, null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusternodesnodeIdnetworkinterfacesinterfaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceid', (done) => {
        try {
          a.getClusternodesnodeIdnetworkinterfacesinterfaceId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusternodesnodeIdnetworkinterfacesinterfaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessnmpactionstart - errors', () => {
      it('should have a postNodeservicessnmpactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessnmpactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessnmpactionrestart - errors', () => {
      it('should have a postNodeservicessnmpactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessnmpactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodefileStorefileNamethumbprint - errors', () => {
      it('should have a getNodefileStorefileNamethumbprint function', (done) => {
        try {
          assert.equal(true, typeof a.getNodefileStorefileNamethumbprint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.getNodefileStorefileNamethumbprint(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodefileStorefileNamethumbprint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesliagentstatus - errors', () => {
      it('should have a getNodeservicesliagentstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesliagentstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusterrestoreactionstart - errors', () => {
      it('should have a postClusterrestoreactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postClusterrestoreactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing InitiateClusterRestoreRequest', (done) => {
        try {
          a.postClusterrestoreactionstart(null, (data, error) => {
            try {
              const displayE = 'InitiateClusterRestoreRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postClusterrestoreactionstart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesnsxMessageBusactionstop - errors', () => {
      it('should have a postNodeservicesnsxMessageBusactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesnsxMessageBusactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessnmpstatus - errors', () => {
      it('should have a getNodeservicessnmpstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessnmpstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodesupportBundle - errors', () => {
      it('should have a getNodesupportBundle function', (done) => {
        try {
          assert.equal(true, typeof a.getNodesupportBundle === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessearch - errors', () => {
      it('should have a getNodeservicessearch function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusterrestoreactioncancel - errors', () => {
      it('should have a postClusterrestoreactioncancel function', (done) => {
        try {
          assert.equal(true, typeof a.postClusterrestoreactioncancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagementcsrscsrIdpemFile - errors', () => {
      it('should have a getTrustManagementcsrscsrIdpemFile function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagementcsrscsrIdpemFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrid', (done) => {
        try {
          a.getTrustManagementcsrscsrIdpemFile(null, (data, error) => {
            try {
              const displayE = 'csrid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTrustManagementcsrscsrIdpemFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusterrestoreactionadvance - errors', () => {
      it('should have a postClusterrestoreactionadvance function', (done) => {
        try {
          assert.equal(true, typeof a.postClusterrestoreactionadvance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AdvanceClusterRestoreRequest', (done) => {
        try {
          a.postClusterrestoreactionadvance(null, (data, error) => {
            try {
              const displayE = 'AdvanceClusterRestoreRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postClusterrestoreactionadvance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessshstatus - errors', () => {
      it('should have a getNodeservicessshstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessshstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesnsxMessageBusactionrestart - errors', () => {
      it('should have a postNodeservicesnsxMessageBusactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesnsxMessageBusactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putClusterrestoreconfig - errors', () => {
      it('should have a putClusterrestoreconfig function', (done) => {
        try {
          assert.equal(true, typeof a.putClusterrestoreconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RestoreConfiguration', (done) => {
        try {
          a.putClusterrestoreconfig(null, (data, error) => {
            try {
              const displayE = 'RestoreConfiguration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putClusterrestoreconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterrestoreconfig - errors', () => {
      it('should have a getClusterrestoreconfig function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterrestoreconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicescmInventory - errors', () => {
      it('should have a getNodeservicescmInventory function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicescmInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusterrestoreactionsuspend - errors', () => {
      it('should have a postClusterrestoreactionsuspend function', (done) => {
        try {
          assert.equal(true, typeof a.postClusterrestoreactionsuspend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodetaskstaskIdactioncancel - errors', () => {
      it('should have a postNodetaskstaskIdactioncancel function', (done) => {
        try {
          assert.equal(true, typeof a.postNodetaskstaskIdactioncancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskid', (done) => {
        try {
          a.postNodetaskstaskIdactioncancel(null, (data, error) => {
            try {
              const displayE = 'taskid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodetaskstaskIdactioncancel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrustManagementprincipalIdentitiesprincipalIdentityId - errors', () => {
      it('should have a deleteTrustManagementprincipalIdentitiesprincipalIdentityId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTrustManagementprincipalIdentitiesprincipalIdentityId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing principalidentityid', (done) => {
        try {
          a.deleteTrustManagementprincipalIdentitiesprincipalIdentityId(null, (data, error) => {
            try {
              const displayE = 'principalidentityid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTrustManagementprincipalIdentitiesprincipalIdentityId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodefileStorefileName - errors', () => {
      it('should have a postNodefileStorefileName function', (done) => {
        try {
          assert.equal(true, typeof a.postNodefileStorefileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.postNodefileStorefileName(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodefileStorefileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodefileStorefileName - errors', () => {
      it('should have a deleteNodefileStorefileName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodefileStorefileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.deleteNodefileStorefileName(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNodefileStorefileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodefileStorefileName - errors', () => {
      it('should have a getNodefileStorefileName function', (done) => {
        try {
          assert.equal(true, typeof a.getNodefileStorefileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.getNodefileStorefileName(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodefileStorefileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicescmInventoryactionrestart - errors', () => {
      it('should have a postNodeservicescmInventoryactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicescmInventoryactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeusersuseridsshKeysactionaddSshKey - errors', () => {
      it('should have a postNodeusersuseridsshKeysactionaddSshKey function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeusersuseridsshKeysactionaddSshKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.postNodeusersuseridsshKeysactionaddSshKey(null, null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodeusersuseridsshKeysactionaddSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing SshKeyProperties', (done) => {
        try {
          a.postNodeusersuseridsshKeysactionaddSshKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'SshKeyProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodeusersuseridsshKeysactionaddSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessshactionremoveHostFingerprint - errors', () => {
      it('should have a postNodeservicessshactionremoveHostFingerprint function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessshactionremoveHostFingerprint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing KnownHostParameter', (done) => {
        try {
          a.postNodeservicessshactionremoveHostFingerprint(null, (data, error) => {
            try {
              const displayE = 'KnownHostParameter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodeservicessshactionremoveHostFingerprint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodetasks - errors', () => {
      it('should have a getNodetasks function', (done) => {
        try {
          assert.equal(true, typeof a.getNodetasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesmgmtPlaneBusactionstart - errors', () => {
      it('should have a postNodeservicesmgmtPlaneBusactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesmgmtPlaneBusactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesnsxMessageBusstatus - errors', () => {
      it('should have a getNodeservicesnsxMessageBusstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesnsxMessageBusstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesnodeMgmtstatus - errors', () => {
      it('should have a getNodeservicesnodeMgmtstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesnodeMgmtstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClusternodesnodeId - errors', () => {
      it('should have a deleteClusternodesnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClusternodesnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.deleteClusternodesnodeId(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteClusternodesnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusternodesnodeId - errors', () => {
      it('should have a getClusternodesnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.getClusternodesnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getClusternodesnodeId(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusternodesnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesntpactionstop - errors', () => {
      it('should have a postNodeservicesntpactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesntpactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessyslogactionrestart - errors', () => {
      it('should have a postNodeservicessyslogactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessyslogactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeservicesmanager - errors', () => {
      it('should have a putNodeservicesmanager function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeservicesmanager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeProtonServiceProperties', (done) => {
        try {
          a.putNodeservicesmanager(null, (data, error) => {
            try {
              const displayE = 'NodeProtonServiceProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeservicesmanager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesmanager - errors', () => {
      it('should have a getNodeservicesmanager function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesmanager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeserviceshttpactionrestart - errors', () => {
      it('should have a postNodeserviceshttpactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeserviceshttpactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeaaaprovidersvidmstatus - errors', () => {
      it('should have a getNodeaaaprovidersvidmstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeaaaprovidersvidmstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesmanageractionresetManagerLoggingLevels - errors', () => {
      it('should have a postNodeservicesmanageractionresetManagerLoggingLevels function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesmanageractionresetManagerLoggingLevels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeusers - errors', () => {
      it('should have a getNodeusers function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeusers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusternodesstatus - errors', () => {
      it('should have a getClusternodesstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getClusternodesstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesnsxUpgradeAgentstatus - errors', () => {
      it('should have a getNodeservicesnsxUpgradeAgentstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesnsxUpgradeAgentstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodefileStorefileNameactioncopyFromRemoteFile - errors', () => {
      it('should have a postNodefileStorefileNameactioncopyFromRemoteFile function', (done) => {
        try {
          assert.equal(true, typeof a.postNodefileStorefileNameactioncopyFromRemoteFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.postNodefileStorefileNameactioncopyFromRemoteFile(null, null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodefileStorefileNameactioncopyFromRemoteFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CopyFromRemoteFileProperties', (done) => {
        try {
          a.postNodefileStorefileNameactioncopyFromRemoteFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'CopyFromRemoteFileProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodefileStorefileNameactioncopyFromRemoteFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesnodeMgmt - errors', () => {
      it('should have a getNodeservicesnodeMgmt function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesnodeMgmt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeservicesinstallUpgrade - errors', () => {
      it('should have a putNodeservicesinstallUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeservicesinstallUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeInstallUpgradeServiceProperties', (done) => {
        try {
          a.putNodeservicesinstallUpgrade(null, (data, error) => {
            try {
              const displayE = 'NodeInstallUpgradeServiceProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeservicesinstallUpgrade', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesinstallUpgrade - errors', () => {
      it('should have a getNodeservicesinstallUpgrade function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesinstallUpgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeprocesses - errors', () => {
      it('should have a getNodeprocesses function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeprocesses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodenetworkroutesrouteId - errors', () => {
      it('should have a deleteNodenetworkroutesrouteId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodenetworkroutesrouteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeid', (done) => {
        try {
          a.deleteNodenetworkroutesrouteId(null, (data, error) => {
            try {
              const displayE = 'routeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNodenetworkroutesrouteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodenetworkroutesrouteId - errors', () => {
      it('should have a getNodenetworkroutesrouteId function', (done) => {
        try {
          assert.equal(true, typeof a.getNodenetworkroutesrouteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeid', (done) => {
        try {
          a.getNodenetworkroutesrouteId(null, (data, error) => {
            try {
              const displayE = 'routeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodenetworkroutesrouteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodefileStore - errors', () => {
      it('should have a getNodefileStore function', (done) => {
        try {
          assert.equal(true, typeof a.getNodefileStore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTrustManagementcsrs - errors', () => {
      it('should have a postTrustManagementcsrs function', (done) => {
        try {
          assert.equal(true, typeof a.postTrustManagementcsrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Csr', (done) => {
        try {
          a.postTrustManagementcsrs(null, (data, error) => {
            try {
              const displayE = 'Csr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTrustManagementcsrs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagementcsrs - errors', () => {
      it('should have a getTrustManagementcsrs function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagementcsrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesnsxUpgradeAgent - errors', () => {
      it('should have a getNodeservicesnsxUpgradeAgent function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesnsxUpgradeAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodehardeningPolicymandatoryAccessControlreport - errors', () => {
      it('should have a getNodehardeningPolicymandatoryAccessControlreport function', (done) => {
        try {
          assert.equal(true, typeof a.getNodehardeningPolicymandatoryAccessControlreport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesinstallUpgradestatus - errors', () => {
      it('should have a getNodeservicesinstallUpgradestatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesinstallUpgradestatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCluster - errors', () => {
      it('should have a getCluster function', (done) => {
        try {
          assert.equal(true, typeof a.getCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodefileStoreactioncreateRemoteDirectory - errors', () => {
      it('should have a postNodefileStoreactioncreateRemoteDirectory function', (done) => {
        try {
          assert.equal(true, typeof a.postNodefileStoreactioncreateRemoteDirectory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CreateRemoteDirectoryProperties', (done) => {
        try {
          a.postNodefileStoreactioncreateRemoteDirectory(null, (data, error) => {
            try {
              const displayE = 'CreateRemoteDirectoryProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodefileStoreactioncreateRemoteDirectory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessyslogactionstart - errors', () => {
      it('should have a postNodeservicessyslogactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessyslogactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessyslog - errors', () => {
      it('should have a getNodeservicessyslog function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putClusterbackupsconfig - errors', () => {
      it('should have a putClusterbackupsconfig function', (done) => {
        try {
          assert.equal(true, typeof a.putClusterbackupsconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BackupConfiguration', (done) => {
        try {
          a.putClusterbackupsconfig(null, (data, error) => {
            try {
              const displayE = 'BackupConfiguration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putClusterbackupsconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterbackupsconfig - errors', () => {
      it('should have a getClusterbackupsconfig function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterbackupsconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTrustManagementcrlscrlId - errors', () => {
      it('should have a putTrustManagementcrlscrlId function', (done) => {
        try {
          assert.equal(true, typeof a.putTrustManagementcrlscrlId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crlid', (done) => {
        try {
          a.putTrustManagementcrlscrlId(null, null, (data, error) => {
            try {
              const displayE = 'crlid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putTrustManagementcrlscrlId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Crl', (done) => {
        try {
          a.putTrustManagementcrlscrlId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'Crl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putTrustManagementcrlscrlId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagementcrlscrlId - errors', () => {
      it('should have a getTrustManagementcrlscrlId function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagementcrlscrlId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crlid', (done) => {
        try {
          a.getTrustManagementcrlscrlId(null, null, (data, error) => {
            try {
              const displayE = 'crlid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTrustManagementcrlscrlId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrustManagementcrlscrlId - errors', () => {
      it('should have a deleteTrustManagementcrlscrlId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTrustManagementcrlscrlId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing crlid', (done) => {
        try {
          a.deleteTrustManagementcrlscrlId(null, (data, error) => {
            try {
              const displayE = 'crlid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTrustManagementcrlscrlId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTrustManagementcsrscsrIdactionselfSign - errors', () => {
      it('should have a postTrustManagementcsrscsrIdactionselfSign function', (done) => {
        try {
          assert.equal(true, typeof a.postTrustManagementcsrscsrIdactionselfSign === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrid', (done) => {
        try {
          a.postTrustManagementcsrscsrIdactionselfSign(null, null, (data, error) => {
            try {
              const displayE = 'csrid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTrustManagementcsrscsrIdactionselfSign', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing daysValid', (done) => {
        try {
          a.postTrustManagementcsrscsrIdactionselfSign('fakeparam', null, (data, error) => {
            try {
              const displayE = 'daysValid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTrustManagementcsrscsrIdactionselfSign', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeusersuseridsshKeysactionremoveSshKey - errors', () => {
      it('should have a postNodeusersuseridsshKeysactionremoveSshKey function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeusersuseridsshKeysactionremoveSshKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.postNodeusersuseridsshKeysactionremoveSshKey(null, null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodeusersuseridsshKeysactionremoveSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing SshKeyBaseProperties', (done) => {
        try {
          a.postNodeusersuseridsshKeysactionremoveSshKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'SshKeyBaseProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodeusersuseridsshKeysactionremoveSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodenetworkinterfacesinterfaceIdstats - errors', () => {
      it('should have a getNodenetworkinterfacesinterfaceIdstats function', (done) => {
        try {
          assert.equal(true, typeof a.getNodenetworkinterfacesinterfaceIdstats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceid', (done) => {
        try {
          a.getNodenetworkinterfacesinterfaceIdstats(null, (data, error) => {
            try {
              const displayE = 'interfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodenetworkinterfacesinterfaceIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessshactionstop - errors', () => {
      it('should have a postNodeservicessshactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessshactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicessyslogstatus - errors', () => {
      it('should have a getNodeservicessyslogstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicessyslogstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTrustManagementcertificatesactionimport - errors', () => {
      it('should have a postTrustManagementcertificatesactionimport function', (done) => {
        try {
          assert.equal(true, typeof a.postTrustManagementcertificatesactionimport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TrustObjectData', (done) => {
        try {
          a.postTrustManagementcertificatesactionimport(null, (data, error) => {
            try {
              const displayE = 'TrustObjectData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTrustManagementcertificatesactionimport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessyslogactionstop - errors', () => {
      it('should have a postNodeservicessyslogactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessyslogactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTrustManagementprincipalIdentities - errors', () => {
      it('should have a postTrustManagementprincipalIdentities function', (done) => {
        try {
          assert.equal(true, typeof a.postTrustManagementprincipalIdentities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing PrincipalIdentity', (done) => {
        try {
          a.postTrustManagementprincipalIdentities(null, (data, error) => {
            try {
              const displayE = 'PrincipalIdentity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTrustManagementprincipalIdentities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagementprincipalIdentities - errors', () => {
      it('should have a getTrustManagementprincipalIdentities function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagementprincipalIdentities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesmanageractionstop - errors', () => {
      it('should have a postNodeservicesmanageractionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesmanageractionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagementcrls - errors', () => {
      it('should have a getTrustManagementcrls function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagementcrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeaaaprovidersvidm - errors', () => {
      it('should have a putNodeaaaprovidersvidm function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeaaaprovidersvidm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeAuthProviderVidmProperties', (done) => {
        try {
          a.putNodeaaaprovidersvidm(null, (data, error) => {
            try {
              const displayE = 'NodeAuthProviderVidmProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeaaaprovidersvidm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeaaaprovidersvidm - errors', () => {
      it('should have a getNodeaaaprovidersvidm function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeaaaprovidersvidm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservices - errors', () => {
      it('should have a getNodeservices function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrustManagementcsrscsrId - errors', () => {
      it('should have a deleteTrustManagementcsrscsrId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTrustManagementcsrscsrId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrid', (done) => {
        try {
          a.deleteTrustManagementcsrscsrId(null, (data, error) => {
            try {
              const displayE = 'csrid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTrustManagementcsrscsrId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrustManagementcsrscsrId - errors', () => {
      it('should have a getTrustManagementcsrscsrId function', (done) => {
        try {
          assert.equal(true, typeof a.getTrustManagementcsrscsrId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrid', (done) => {
        try {
          a.getTrustManagementcsrscsrId(null, (data, error) => {
            try {
              const displayE = 'csrid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTrustManagementcsrscsrId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeactionshutdown - errors', () => {
      it('should have a postNodeactionshutdown function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeactionshutdown === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeserviceshttpstatus - errors', () => {
      it('should have a getNodeserviceshttpstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeserviceshttpstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodenetworkinterfaces - errors', () => {
      it('should have a getNodenetworkinterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getNodenetworkinterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeserviceshttpactionapplyCertificate - errors', () => {
      it('should have a postNodeserviceshttpactionapplyCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeserviceshttpactionapplyCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateId', (done) => {
        try {
          a.postNodeserviceshttpactionapplyCertificate(null, (data, error) => {
            try {
              const displayE = 'certificateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodeserviceshttpactionapplyCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessearchactionstop - errors', () => {
      it('should have a postNodeservicessearchactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessearchactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeusersuseridsshKeys - errors', () => {
      it('should have a getNodeusersuseridsshKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeusersuseridsshKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userid', (done) => {
        try {
          a.getNodeusersuseridsshKeys(null, (data, error) => {
            try {
              const displayE = 'userid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodeusersuseridsshKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessearchactionstart - errors', () => {
      it('should have a postNodeservicessearchactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessearchactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicessearchactionrestart - errors', () => {
      it('should have a postNodeservicessearchactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicessearchactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesinstallUpgradeactionrestart - errors', () => {
      it('should have a postNodeservicesinstallUpgradeactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesinstallUpgradeactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesmanagerstatus - errors', () => {
      it('should have a getNodeservicesmanagerstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesmanagerstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesliagentactionrestart - errors', () => {
      it('should have a postNodeservicesliagentactionrestart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesliagentactionrestart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesnsxMessageBus - errors', () => {
      it('should have a getNodeservicesnsxMessageBus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesnsxMessageBus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesinstallUpgradeactionstop - errors', () => {
      it('should have a postNodeservicesinstallUpgradeactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesinstallUpgradeactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesnsxUpgradeAgentactionstop - errors', () => {
      it('should have a postNodeservicesnsxUpgradeAgentactionstop function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesnsxUpgradeAgentactionstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodehardeningPolicymandatoryAccessControl - errors', () => {
      it('should have a putNodehardeningPolicymandatoryAccessControl function', (done) => {
        try {
          assert.equal(true, typeof a.putNodehardeningPolicymandatoryAccessControl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MandatoryAccessControlProperties', (done) => {
        try {
          a.putNodehardeningPolicymandatoryAccessControl(null, (data, error) => {
            try {
              const displayE = 'MandatoryAccessControlProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodehardeningPolicymandatoryAccessControl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodehardeningPolicymandatoryAccessControl - errors', () => {
      it('should have a getNodehardeningPolicymandatoryAccessControl function', (done) => {
        try {
          assert.equal(true, typeof a.getNodehardeningPolicymandatoryAccessControl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNode - errors', () => {
      it('should have a putNode function', (done) => {
        try {
          assert.equal(true, typeof a.putNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeProperties', (done) => {
        try {
          a.putNode(null, (data, error) => {
            try {
              const displayE = 'NodeProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNode - errors', () => {
      it('should have a getNode function', (done) => {
        try {
          assert.equal(true, typeof a.getNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeserviceshttp - errors', () => {
      it('should have a putNodeserviceshttp function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeserviceshttp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeHttpServiceProperties', (done) => {
        try {
          a.putNodeserviceshttp(null, (data, error) => {
            try {
              const displayE = 'NodeHttpServiceProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeserviceshttp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeserviceshttp - errors', () => {
      it('should have a getNodeserviceshttp function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeserviceshttp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeprocessesprocessId - errors', () => {
      it('should have a getNodeprocessesprocessId function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeprocessesprocessId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing processid', (done) => {
        try {
          a.getNodeprocessesprocessId(null, (data, error) => {
            try {
              const displayE = 'processid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNodeprocessesprocessId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodeservicesliagentactionstart - errors', () => {
      it('should have a postNodeservicesliagentactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postNodeservicesliagentactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeservicesmgmtPlaneBus - errors', () => {
      it('should have a getNodeservicesmgmtPlaneBus function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeservicesmgmtPlaneBus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodenetworkroutes - errors', () => {
      it('should have a postNodenetworkroutes function', (done) => {
        try {
          assert.equal(true, typeof a.postNodenetworkroutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NodeRouteProperties', (done) => {
        try {
          a.postNodenetworkroutes(null, (data, error) => {
            try {
              const displayE = 'NodeRouteProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNodenetworkroutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodenetworkroutes - errors', () => {
      it('should have a getNodenetworkroutes function', (done) => {
        try {
          assert.equal(true, typeof a.getNodenetworkroutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradenodesSummary - errors', () => {
      it('should have a getUpgradenodesSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradenodesSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeupgradeUnitGroups - errors', () => {
      it('should have a postUpgradeupgradeUnitGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeupgradeUnitGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing UpgradeUnitGroup', (done) => {
        try {
          a.postUpgradeupgradeUnitGroups(null, (data, error) => {
            try {
              const displayE = 'UpgradeUnitGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeupgradeUnitGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnitGroups - errors', () => {
      it('should have a getUpgradeupgradeUnitGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnitGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpgradeupgradeUnitGroupsgroupId - errors', () => {
      it('should have a putUpgradeupgradeUnitGroupsgroupId function', (done) => {
        try {
          assert.equal(true, typeof a.putUpgradeupgradeUnitGroupsgroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.putUpgradeupgradeUnitGroupsgroupId(null, null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putUpgradeupgradeUnitGroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing UpgradeUnitGroup', (done) => {
        try {
          a.putUpgradeupgradeUnitGroupsgroupId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'UpgradeUnitGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putUpgradeupgradeUnitGroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUpgradeupgradeUnitGroupsgroupId - errors', () => {
      it('should have a deleteUpgradeupgradeUnitGroupsgroupId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUpgradeupgradeUnitGroupsgroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.deleteUpgradeupgradeUnitGroupsgroupId(null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteUpgradeupgradeUnitGroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnitGroupsgroupId - errors', () => {
      it('should have a getUpgradeupgradeUnitGroupsgroupId function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnitGroupsgroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.getUpgradeupgradeUnitGroupsgroupId(null, null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getUpgradeupgradeUnitGroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeversionWhitelist - errors', () => {
      it('should have a getUpgradeversionWhitelist function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeversionWhitelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnitsStats - errors', () => {
      it('should have a getUpgradeupgradeUnitsStats function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnitsStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeplanactionstart - errors', () => {
      it('should have a postUpgradeplanactionstart function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeplanactionstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpgradeversionWhitelistcomponentType - errors', () => {
      it('should have a putUpgradeversionWhitelistcomponentType function', (done) => {
        try {
          assert.equal(true, typeof a.putUpgradeversionWhitelistcomponentType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing componentType', (done) => {
        try {
          a.putUpgradeversionWhitelistcomponentType(null, null, (data, error) => {
            try {
              const displayE = 'componentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putUpgradeversionWhitelistcomponentType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing VersionList', (done) => {
        try {
          a.putUpgradeversionWhitelistcomponentType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'VersionList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putUpgradeversionWhitelistcomponentType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeversionWhitelistcomponentType - errors', () => {
      it('should have a getUpgradeversionWhitelistcomponentType function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeversionWhitelistcomponentType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing componentType', (done) => {
        try {
          a.getUpgradeversionWhitelistcomponentType(null, (data, error) => {
            try {
              const displayE = 'componentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getUpgradeversionWhitelistcomponentType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeplanactionpause - errors', () => {
      it('should have a postUpgradeplanactionpause function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeplanactionpause === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder - errors', () => {
      it('should have a postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder(null, null, null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing upgradeunitid', (done) => {
        try {
          a.postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'upgradeunitid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ReorderRequest', (done) => {
        try {
          a.postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ReorderRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeupgradeUnitGroupsgroupIdupgradeUnitupgradeUnitIdactionreorder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnitGroupsaggregateInfo - errors', () => {
      it('should have a getUpgradeupgradeUnitGroupsaggregateInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnitGroupsaggregateInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnitsupgradeUnitId - errors', () => {
      it('should have a getUpgradeupgradeUnitsupgradeUnitId function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnitsupgradeUnitId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing upgradeunitid', (done) => {
        try {
          a.getUpgradeupgradeUnitsupgradeUnitId(null, (data, error) => {
            try {
              const displayE = 'upgradeunitid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getUpgradeupgradeUnitsupgradeUnitId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeupgradeUnitGroupsgroupIdactionreorder - errors', () => {
      it('should have a postUpgradeupgradeUnitGroupsgroupIdactionreorder function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeupgradeUnitGroupsgroupIdactionreorder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.postUpgradeupgradeUnitGroupsgroupIdactionreorder(null, null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeupgradeUnitGroupsgroupIdactionreorder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ReorderRequest', (done) => {
        try {
          a.postUpgradeupgradeUnitGroupsgroupIdactionreorder('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ReorderRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeupgradeUnitGroupsgroupIdactionreorder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeplanactioncontinue - errors', () => {
      it('should have a postUpgradeplanactioncontinue function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeplanactioncontinue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnitsaggregateInfo - errors', () => {
      it('should have a getUpgradeupgradeUnitsaggregateInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnitsaggregateInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeupgradeUnitGroupsgroupIdactionaddUpgradeUnits - errors', () => {
      it('should have a postUpgradeupgradeUnitGroupsgroupIdactionaddUpgradeUnits function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeupgradeUnitGroupsgroupIdactionaddUpgradeUnits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.postUpgradeupgradeUnitGroupsgroupIdactionaddUpgradeUnits(null, null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeupgradeUnitGroupsgroupIdactionaddUpgradeUnits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing UpgradeUnitList', (done) => {
        try {
          a.postUpgradeupgradeUnitGroupsgroupIdactionaddUpgradeUnits('fakeparam', null, (data, error) => {
            try {
              const displayE = 'UpgradeUnitList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeupgradeUnitGroupsgroupIdactionaddUpgradeUnits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnits - errors', () => {
      it('should have a getUpgradeupgradeUnits function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradestatusSummary - errors', () => {
      it('should have a getUpgradestatusSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradestatusSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnitGroupsgroupIdstatus - errors', () => {
      it('should have a getUpgradeupgradeUnitGroupsgroupIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnitGroupsgroupIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.getUpgradeupgradeUnitGroupsgroupIdstatus(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getUpgradeupgradeUnitGroupsgroupIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeplanactionupgradeSelectedUnits - errors', () => {
      it('should have a postUpgradeplanactionupgradeSelectedUnits function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeplanactionupgradeSelectedUnits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing UpgradeUnitList', (done) => {
        try {
          a.postUpgradeplanactionupgradeSelectedUnits(null, (data, error) => {
            try {
              const displayE = 'UpgradeUnitList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeplanactionupgradeSelectedUnits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeupgradeUnitGroupsStatus - errors', () => {
      it('should have a getUpgradeupgradeUnitGroupsStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeupgradeUnitGroupsStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradesummary - errors', () => {
      it('should have a getUpgradesummary function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradesummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradenodes - errors', () => {
      it('should have a getUpgradenodes function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradenodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeactionupgradeUc - errors', () => {
      it('should have a postUpgradeactionupgradeUc function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeactionupgradeUc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradehistory - errors', () => {
      it('should have a getUpgradehistory function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradehistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpgradeplancomponentTypesettings - errors', () => {
      it('should have a putUpgradeplancomponentTypesettings function', (done) => {
        try {
          assert.equal(true, typeof a.putUpgradeplancomponentTypesettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing componentType', (done) => {
        try {
          a.putUpgradeplancomponentTypesettings(null, null, (data, error) => {
            try {
              const displayE = 'componentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putUpgradeplancomponentTypesettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing UpgradePlanSettings', (done) => {
        try {
          a.putUpgradeplancomponentTypesettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'UpgradePlanSettings is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putUpgradeplancomponentTypesettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeplancomponentTypesettings - errors', () => {
      it('should have a getUpgradeplancomponentTypesettings function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeplancomponentTypesettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing componentType', (done) => {
        try {
          a.getUpgradeplancomponentTypesettings(null, (data, error) => {
            try {
              const displayE = 'componentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getUpgradeplancomponentTypesettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeplanactionreset - errors', () => {
      it('should have a postUpgradeplanactionreset function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeplanactionreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing componentType', (done) => {
        try {
          a.postUpgradeplanactionreset(null, (data, error) => {
            try {
              const displayE = 'componentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUpgradeplanactionreset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDhcpserversserverIdipPools - errors', () => {
      it('should have a postDhcpserversserverIdipPools function', (done) => {
        try {
          assert.equal(true, typeof a.postDhcpserversserverIdipPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.postDhcpserversserverIdipPools(null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postDhcpserversserverIdipPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpIpPool', (done) => {
        try {
          a.postDhcpserversserverIdipPools('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DhcpIpPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postDhcpserversserverIdipPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserversserverIdipPools - errors', () => {
      it('should have a getDhcpserversserverIdipPools function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserversserverIdipPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.getDhcpserversserverIdipPools(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdipPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDhcpserverProfiles - errors', () => {
      it('should have a postDhcpserverProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postDhcpserverProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpProfile', (done) => {
        try {
          a.postDhcpserverProfiles(null, (data, error) => {
            try {
              const displayE = 'DhcpProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postDhcpserverProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserverProfiles - errors', () => {
      it('should have a getDhcpserverProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserverProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionkeyPolicieskeyPolicyIdstats - errors', () => {
      it('should have a getNetworkEncryptionkeyPolicieskeyPolicyIdstats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionkeyPolicieskeyPolicyIdstats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypolicyid', (done) => {
        try {
          a.getNetworkEncryptionkeyPolicieskeyPolicyIdstats(null, null, (data, error) => {
            try {
              const displayE = 'keypolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionkeyPolicieskeyPolicyIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancerapplicationProfilesapplicationProfileId - errors', () => {
      it('should have a putLoadbalancerapplicationProfilesapplicationProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancerapplicationProfilesapplicationProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationprofileid', (done) => {
        try {
          a.putLoadbalancerapplicationProfilesapplicationProfileId(null, null, (data, error) => {
            try {
              const displayE = 'applicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerapplicationProfilesapplicationProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbAppProfile', (done) => {
        try {
          a.putLoadbalancerapplicationProfilesapplicationProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbAppProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerapplicationProfilesapplicationProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancerapplicationProfilesapplicationProfileId - errors', () => {
      it('should have a deleteLoadbalancerapplicationProfilesapplicationProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancerapplicationProfilesapplicationProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationprofileid', (done) => {
        try {
          a.deleteLoadbalancerapplicationProfilesapplicationProfileId(null, (data, error) => {
            try {
              const displayE = 'applicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancerapplicationProfilesapplicationProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerapplicationProfilesapplicationProfileId - errors', () => {
      it('should have a getLoadbalancerapplicationProfilesapplicationProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerapplicationProfilesapplicationProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationprofileid', (done) => {
        try {
          a.getLoadbalancerapplicationProfilesapplicationProfileId(null, (data, error) => {
            try {
              const displayE = 'applicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerapplicationProfilesapplicationProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsections - errors', () => {
      it('should have a postFirewallsections function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallSection', (done) => {
        try {
          a.postFirewallsections(null, null, null, (data, error) => {
            try {
              const displayE = 'FirewallSection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallsections - errors', () => {
      it('should have a getFirewallsections function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallsections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancerserverSslProfiles - errors', () => {
      it('should have a postLoadbalancerserverSslProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancerserverSslProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbServerSslProfile', (done) => {
        try {
          a.postLoadbalancerserverSslProfiles(null, (data, error) => {
            try {
              const displayE = 'LbServerSslProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerserverSslProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerserverSslProfiles - errors', () => {
      it('should have a getLoadbalancerserverSslProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerserverSslProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancerservicesserviceId - errors', () => {
      it('should have a putLoadbalancerservicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancerservicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.putLoadbalancerservicesserviceId(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerservicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbService', (done) => {
        try {
          a.putLoadbalancerservicesserviceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbService is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerservicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancerservicesserviceId - errors', () => {
      it('should have a deleteLoadbalancerservicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancerservicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.deleteLoadbalancerservicesserviceId(null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancerservicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceId - errors', () => {
      it('should have a getLoadbalancerservicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceId(null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancerrulesruleId - errors', () => {
      it('should have a putLoadbalancerrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancerrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.putLoadbalancerrulesruleId(null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbRule', (done) => {
        try {
          a.putLoadbalancerrulesruleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerrulesruleId - errors', () => {
      it('should have a getLoadbalancerrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getLoadbalancerrulesruleId(null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancerrulesruleId - errors', () => {
      it('should have a deleteLoadbalancerrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancerrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.deleteLoadbalancerrulesruleId(null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancerrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserversserverIdstatus - errors', () => {
      it('should have a getDhcpserversserverIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserversserverIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.getDhcpserversserverIdstatus(null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDhcpservers - errors', () => {
      it('should have a postDhcpservers function', (done) => {
        try {
          assert.equal(true, typeof a.postDhcpservers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalDhcpServer', (done) => {
        try {
          a.postDhcpservers(null, (data, error) => {
            try {
              const displayE = 'LogicalDhcpServer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postDhcpservers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpservers - errors', () => {
      it('should have a getDhcpservers function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpservers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDhcpserversserverIdipPoolspoolId - errors', () => {
      it('should have a putDhcpserversserverIdipPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.putDhcpserversserverIdipPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.putDhcpserversserverIdipPoolspoolId(null, null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserversserverIdipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.putDhcpserversserverIdipPoolspoolId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserversserverIdipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpIpPool', (done) => {
        try {
          a.putDhcpserversserverIdipPoolspoolId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'DhcpIpPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserversserverIdipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserversserverIdipPoolspoolId - errors', () => {
      it('should have a getDhcpserversserverIdipPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserversserverIdipPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.getDhcpserversserverIdipPoolspoolId(null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getDhcpserversserverIdipPoolspoolId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpserversserverIdipPoolspoolId - errors', () => {
      it('should have a deleteDhcpserversserverIdipPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcpserversserverIdipPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.deleteDhcpserversserverIdipPoolspoolId(null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteDhcpserversserverIdipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.deleteDhcpserversserverIdipPoolspoolId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteDhcpserversserverIdipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsectionssectionIdactionreviseWithRules - errors', () => {
      it('should have a postFirewallsectionssectionIdactionreviseWithRules function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsectionssectionIdactionreviseWithRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postFirewallsectionssectionIdactionreviseWithRules(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdactionreviseWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallSectionRuleList', (done) => {
        try {
          a.postFirewallsectionssectionIdactionreviseWithRules('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'FirewallSectionRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdactionreviseWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDhcpserversserverIdstaticBindings - errors', () => {
      it('should have a postDhcpserversserverIdstaticBindings function', (done) => {
        try {
          assert.equal(true, typeof a.postDhcpserversserverIdstaticBindings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.postDhcpserversserverIdstaticBindings(null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postDhcpserversserverIdstaticBindings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpStaticBinding', (done) => {
        try {
          a.postDhcpserversserverIdstaticBindings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DhcpStaticBinding is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postDhcpserversserverIdstaticBindings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserversserverIdstaticBindings - errors', () => {
      it('should have a getDhcpserversserverIdstaticBindings function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserversserverIdstaticBindings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.getDhcpserversserverIdstaticBindings(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdstaticBindings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsectionssectionIdrules - errors', () => {
      it('should have a postNetworkEncryptionsectionssectionIdrules function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsectionssectionIdrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrules(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneRule', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrules('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'DneRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrules('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionsectionssectionIdrules - errors', () => {
      it('should have a getNetworkEncryptionsectionssectionIdrules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionsectionssectionIdrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionIdrules(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionIdrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putFirewallstatuscontextType - errors', () => {
      it('should have a putFirewallstatuscontextType function', (done) => {
        try {
          assert.equal(true, typeof a.putFirewallstatuscontextType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contexttype', (done) => {
        try {
          a.putFirewallstatuscontextType(null, null, (data, error) => {
            try {
              const displayE = 'contexttype is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFirewallstatuscontextType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallStatus', (done) => {
        try {
          a.putFirewallstatuscontextType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'FirewallStatus is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFirewallstatuscontextType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallstatuscontextType - errors', () => {
      it('should have a getFirewallstatuscontextType function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallstatuscontextType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contexttype', (done) => {
        try {
          a.getFirewallstatuscontextType(null, (data, error) => {
            try {
              const displayE = 'contexttype is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallstatuscontextType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsectionssectionIdactionreviseWithRules - errors', () => {
      it('should have a postNetworkEncryptionsectionssectionIdactionreviseWithRules function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsectionssectionIdactionreviseWithRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionreviseWithRules(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionreviseWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneSectionRuleList', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionreviseWithRules('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'DneSectionRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionreviseWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionreviseWithRules('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionreviseWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancerpoolspoolId - errors', () => {
      it('should have a putLoadbalancerpoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancerpoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.putLoadbalancerpoolspoolId(null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerpoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbPool', (done) => {
        try {
          a.putLoadbalancerpoolspoolId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerpoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancerpoolspoolId - errors', () => {
      it('should have a postLoadbalancerpoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancerpoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.postLoadbalancerpoolspoolId(null, null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerpoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing PoolMemberSettingList', (done) => {
        try {
          a.postLoadbalancerpoolspoolId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'PoolMemberSettingList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerpoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.postLoadbalancerpoolspoolId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerpoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancerpoolspoolId - errors', () => {
      it('should have a deleteLoadbalancerpoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancerpoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.deleteLoadbalancerpoolspoolId(null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancerpoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerpoolspoolId - errors', () => {
      it('should have a getLoadbalancerpoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerpoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getLoadbalancerpoolspoolId(null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerpoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsectionssectionIdactionupdateWithRules - errors', () => {
      it('should have a postFirewallsectionssectionIdactionupdateWithRules function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsectionssectionIdactionupdateWithRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postFirewallsectionssectionIdactionupdateWithRules(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdactionupdateWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallSectionRuleList', (done) => {
        try {
          a.postFirewallsectionssectionIdactionupdateWithRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'FirewallSectionRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdactionupdateWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsectionssectionIdactionlistWithRules - errors', () => {
      it('should have a postFirewallsectionssectionIdactionlistWithRules function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsectionssectionIdactionlistWithRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postFirewallsectionssectionIdactionlistWithRules(null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdactionlistWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdpoolspoolIdstatus - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdpoolspoolIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdpoolspoolIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdpoolspoolIdstatus(null, null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdpoolspoolIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdpoolspoolIdstatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdpoolspoolIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionstatus - errors', () => {
      it('should have a getNetworkEncryptionstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDhcpserversserverId - errors', () => {
      it('should have a putDhcpserversserverId function', (done) => {
        try {
          assert.equal(true, typeof a.putDhcpserversserverId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.putDhcpserversserverId(null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserversserverId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LogicalDhcpServer', (done) => {
        try {
          a.putDhcpserversserverId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LogicalDhcpServer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserversserverId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpserversserverId - errors', () => {
      it('should have a deleteDhcpserversserverId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcpserversserverId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.deleteDhcpserversserverId(null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteDhcpserversserverId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserversserverId - errors', () => {
      it('should have a getDhcpserversserverId function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserversserverId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.getDhcpserversserverId(null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsectionssectionIdrulesactioncreateMultiple - errors', () => {
      it('should have a postFirewallsectionssectionIdrulesactioncreateMultiple function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsectionssectionIdrulesactioncreateMultiple === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postFirewallsectionssectionIdrulesactioncreateMultiple(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdrulesactioncreateMultiple', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallRuleList', (done) => {
        try {
          a.postFirewallsectionssectionIdrulesactioncreateMultiple('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'FirewallRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdrulesactioncreateMultiple', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetworkEncryptionkeyPolicieskeyPolicyId - errors', () => {
      it('should have a putNetworkEncryptionkeyPolicieskeyPolicyId function', (done) => {
        try {
          assert.equal(true, typeof a.putNetworkEncryptionkeyPolicieskeyPolicyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypolicyid', (done) => {
        try {
          a.putNetworkEncryptionkeyPolicieskeyPolicyId(null, null, (data, error) => {
            try {
              const displayE = 'keypolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionkeyPolicieskeyPolicyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneKeyPolicy', (done) => {
        try {
          a.putNetworkEncryptionkeyPolicieskeyPolicyId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DneKeyPolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionkeyPolicieskeyPolicyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkEncryptionkeyPolicieskeyPolicyId - errors', () => {
      it('should have a deleteNetworkEncryptionkeyPolicieskeyPolicyId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkEncryptionkeyPolicieskeyPolicyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypolicyid', (done) => {
        try {
          a.deleteNetworkEncryptionkeyPolicieskeyPolicyId(null, (data, error) => {
            try {
              const displayE = 'keypolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNetworkEncryptionkeyPolicieskeyPolicyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionkeyPolicieskeyPolicyId - errors', () => {
      it('should have a getNetworkEncryptionkeyPolicieskeyPolicyId function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionkeyPolicieskeyPolicyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypolicyid', (done) => {
        try {
          a.getNetworkEncryptionkeyPolicieskeyPolicyId(null, (data, error) => {
            try {
              const displayE = 'keypolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionkeyPolicieskeyPolicyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsectionsactioncreateWithRules - errors', () => {
      it('should have a postNetworkEncryptionsectionsactioncreateWithRules function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsectionsactioncreateWithRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneSectionRuleList', (done) => {
        try {
          a.postNetworkEncryptionsectionsactioncreateWithRules(null, null, null, (data, error) => {
            try {
              const displayE = 'DneSectionRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionsactioncreateWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.postNetworkEncryptionsectionsactioncreateWithRules('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionsactioncreateWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancerrules - errors', () => {
      it('should have a postLoadbalancerrules function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancerrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbRule', (done) => {
        try {
          a.postLoadbalancerrules(null, (data, error) => {
            try {
              const displayE = 'LbRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerrules - errors', () => {
      it('should have a getLoadbalancerrules function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetworkEncryptionsectionssectionId - errors', () => {
      it('should have a putNetworkEncryptionsectionssectionId function', (done) => {
        try {
          assert.equal(true, typeof a.putNetworkEncryptionsectionssectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.putNetworkEncryptionsectionssectionId(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneSection', (done) => {
        try {
          a.putNetworkEncryptionsectionssectionId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DneSection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionsectionssectionId - errors', () => {
      it('should have a getNetworkEncryptionsectionssectionId function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionsectionssectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionId(null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkEncryptionsectionssectionId - errors', () => {
      it('should have a deleteNetworkEncryptionsectionssectionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkEncryptionsectionssectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.deleteNetworkEncryptionsectionssectionId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNetworkEncryptionsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.deleteNetworkEncryptionsectionssectionId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNetworkEncryptionsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserversserverIdstatistics - errors', () => {
      it('should have a getDhcpserversserverIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserversserverIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.getDhcpserversserverIdstatistics(null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancerpools - errors', () => {
      it('should have a postLoadbalancerpools function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancerpools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbPool', (done) => {
        try {
          a.postLoadbalancerpools(null, (data, error) => {
            try {
              const displayE = 'LbPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerpools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerpools - errors', () => {
      it('should have a getLoadbalancerpools function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerpools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsections - errors', () => {
      it('should have a postNetworkEncryptionsections function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneSection', (done) => {
        try {
          a.postNetworkEncryptionsections(null, null, null, (data, error) => {
            try {
              const displayE = 'DneSection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.postNetworkEncryptionsections('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionsections - errors', () => {
      it('should have a getNetworkEncryptionsections function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionsections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallexcludelistactionremoveMember - errors', () => {
      it('should have a postFirewallexcludelistactionremoveMember function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallexcludelistactionremoveMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.postFirewallexcludelistactionremoveMember(null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallexcludelistactionremoveMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancerpersistenceProfiles - errors', () => {
      it('should have a postLoadbalancerpersistenceProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancerpersistenceProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbPersistenceProfile', (done) => {
        try {
          a.postLoadbalancerpersistenceProfiles(null, (data, error) => {
            try {
              const displayE = 'LbPersistenceProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerpersistenceProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerpersistenceProfiles - errors', () => {
      it('should have a getLoadbalancerpersistenceProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerpersistenceProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancerserverSslProfilesserverSslProfileId - errors', () => {
      it('should have a putLoadbalancerserverSslProfilesserverSslProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancerserverSslProfilesserverSslProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serversslprofileid', (done) => {
        try {
          a.putLoadbalancerserverSslProfilesserverSslProfileId(null, null, (data, error) => {
            try {
              const displayE = 'serversslprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerserverSslProfilesserverSslProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbServerSslProfile', (done) => {
        try {
          a.putLoadbalancerserverSslProfilesserverSslProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbServerSslProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerserverSslProfilesserverSslProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancerserverSslProfilesserverSslProfileId - errors', () => {
      it('should have a deleteLoadbalancerserverSslProfilesserverSslProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancerserverSslProfilesserverSslProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serversslprofileid', (done) => {
        try {
          a.deleteLoadbalancerserverSslProfilesserverSslProfileId(null, (data, error) => {
            try {
              const displayE = 'serversslprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancerserverSslProfilesserverSslProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerserverSslProfilesserverSslProfileId - errors', () => {
      it('should have a getLoadbalancerserverSslProfilesserverSslProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerserverSslProfilesserverSslProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serversslprofileid', (done) => {
        try {
          a.getLoadbalancerserverSslProfilesserverSslProfileId(null, (data, error) => {
            try {
              const displayE = 'serversslprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerserverSslProfilesserverSslProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putFirewallexcludelist - errors', () => {
      it('should have a putFirewallexcludelist function', (done) => {
        try {
          assert.equal(true, typeof a.putFirewallexcludelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ExcludeList', (done) => {
        try {
          a.putFirewallexcludelist(null, (data, error) => {
            try {
              const displayE = 'ExcludeList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFirewallexcludelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallexcludelist - errors', () => {
      it('should have a getFirewallexcludelist function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallexcludelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsectionssectionIdactionrevise - errors', () => {
      it('should have a postNetworkEncryptionsectionssectionIdactionrevise function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsectionssectionIdactionrevise === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionrevise(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneSection', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionrevise('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'DneSection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionrevise('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsectionsactioncreateWithRules - errors', () => {
      it('should have a postFirewallsectionsactioncreateWithRules function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsectionsactioncreateWithRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallSectionRuleList', (done) => {
        try {
          a.postFirewallsectionsactioncreateWithRules(null, null, null, (data, error) => {
            try {
              const displayE = 'FirewallSectionRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionsactioncreateWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise - errors', () => {
      it('should have a postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneRule', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'DneRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrulesruleIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putMdProxiesproxyId - errors', () => {
      it('should have a putMdProxiesproxyId function', (done) => {
        try {
          assert.equal(true, typeof a.putMdProxiesproxyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyid', (done) => {
        try {
          a.putMdProxiesproxyId(null, null, (data, error) => {
            try {
              const displayE = 'proxyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putMdProxiesproxyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MetadataProxy', (done) => {
        try {
          a.putMdProxiesproxyId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'MetadataProxy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putMdProxiesproxyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMdProxiesproxyId - errors', () => {
      it('should have a getMdProxiesproxyId function', (done) => {
        try {
          assert.equal(true, typeof a.getMdProxiesproxyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyid', (done) => {
        try {
          a.getMdProxiesproxyId(null, (data, error) => {
            try {
              const displayE = 'proxyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getMdProxiesproxyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMdProxiesproxyId - errors', () => {
      it('should have a deleteMdProxiesproxyId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMdProxiesproxyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyid', (done) => {
        try {
          a.deleteMdProxiesproxyId(null, (data, error) => {
            try {
              const displayE = 'proxyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteMdProxiesproxyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionkeyPolicies - errors', () => {
      it('should have a postNetworkEncryptionkeyPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionkeyPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneKeyPolicy', (done) => {
        try {
          a.postNetworkEncryptionkeyPolicies(null, (data, error) => {
            try {
              const displayE = 'DneKeyPolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionkeyPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionkeyPolicies - errors', () => {
      it('should have a getNetworkEncryptionkeyPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionkeyPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDhcpserversserverIdstaticBindingsbindingId - errors', () => {
      it('should have a putDhcpserversserverIdstaticBindingsbindingId function', (done) => {
        try {
          assert.equal(true, typeof a.putDhcpserversserverIdstaticBindingsbindingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.putDhcpserversserverIdstaticBindingsbindingId(null, null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserversserverIdstaticBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bindingid', (done) => {
        try {
          a.putDhcpserversserverIdstaticBindingsbindingId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bindingid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserversserverIdstaticBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpStaticBinding', (done) => {
        try {
          a.putDhcpserversserverIdstaticBindingsbindingId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'DhcpStaticBinding is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserversserverIdstaticBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpserversserverIdstaticBindingsbindingId - errors', () => {
      it('should have a deleteDhcpserversserverIdstaticBindingsbindingId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcpserversserverIdstaticBindingsbindingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.deleteDhcpserversserverIdstaticBindingsbindingId(null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteDhcpserversserverIdstaticBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bindingid', (done) => {
        try {
          a.deleteDhcpserversserverIdstaticBindingsbindingId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bindingid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteDhcpserversserverIdstaticBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserversserverIdstaticBindingsbindingId - errors', () => {
      it('should have a getDhcpserversserverIdstaticBindingsbindingId function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserversserverIdstaticBindingsbindingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.getDhcpserversserverIdstaticBindingsbindingId(null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdstaticBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bindingid', (done) => {
        try {
          a.getDhcpserversserverIdstaticBindingsbindingId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bindingid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdstaticBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetworkEncryptionkeyManagerskeyManagerId - errors', () => {
      it('should have a putNetworkEncryptionkeyManagerskeyManagerId function', (done) => {
        try {
          assert.equal(true, typeof a.putNetworkEncryptionkeyManagerskeyManagerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keymanagerid', (done) => {
        try {
          a.putNetworkEncryptionkeyManagerskeyManagerId(null, null, (data, error) => {
            try {
              const displayE = 'keymanagerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionkeyManagerskeyManagerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneKeyManager', (done) => {
        try {
          a.putNetworkEncryptionkeyManagerskeyManagerId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DneKeyManager is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionkeyManagerskeyManagerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionkeyManagerskeyManagerId - errors', () => {
      it('should have a getNetworkEncryptionkeyManagerskeyManagerId function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionkeyManagerskeyManagerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keymanagerid', (done) => {
        try {
          a.getNetworkEncryptionkeyManagerskeyManagerId(null, (data, error) => {
            try {
              const displayE = 'keymanagerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionkeyManagerskeyManagerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkEncryptionkeyManagerskeyManagerId - errors', () => {
      it('should have a deleteNetworkEncryptionkeyManagerskeyManagerId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkEncryptionkeyManagerskeyManagerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keymanagerid', (done) => {
        try {
          a.deleteNetworkEncryptionkeyManagerskeyManagerId(null, (data, error) => {
            try {
              const displayE = 'keymanagerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNetworkEncryptionkeyManagerskeyManagerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionkeyManagers - errors', () => {
      it('should have a postNetworkEncryptionkeyManagers function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionkeyManagers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneKeyManager', (done) => {
        try {
          a.postNetworkEncryptionkeyManagers(null, (data, error) => {
            try {
              const displayE = 'DneKeyManager is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionkeyManagers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionkeyManagers - errors', () => {
      it('should have a getNetworkEncryptionkeyManagers function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionkeyManagers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdvirtualServersstatus - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdvirtualServersstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdvirtualServersstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdvirtualServersstatus(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdvirtualServersstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDhcpserverProfilesprofileId - errors', () => {
      it('should have a putDhcpserverProfilesprofileId function', (done) => {
        try {
          assert.equal(true, typeof a.putDhcpserverProfilesprofileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileid', (done) => {
        try {
          a.putDhcpserverProfilesprofileId(null, null, (data, error) => {
            try {
              const displayE = 'profileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserverProfilesprofileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DhcpProfile', (done) => {
        try {
          a.putDhcpserverProfilesprofileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DhcpProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putDhcpserverProfilesprofileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserverProfilesprofileId - errors', () => {
      it('should have a getDhcpserverProfilesprofileId function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserverProfilesprofileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileid', (done) => {
        try {
          a.getDhcpserverProfilesprofileId(null, (data, error) => {
            try {
              const displayE = 'profileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserverProfilesprofileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpserverProfilesprofileId - errors', () => {
      it('should have a deleteDhcpserverProfilesprofileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcpserverProfilesprofileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileid', (done) => {
        try {
          a.deleteDhcpserverProfilesprofileId(null, (data, error) => {
            try {
              const displayE = 'profileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteDhcpserverProfilesprofileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsectionssectionIdactionlistWithRules - errors', () => {
      it('should have a postNetworkEncryptionsectionssectionIdactionlistWithRules function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsectionssectionIdactionlistWithRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionlistWithRules(null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionlistWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancersslciphersAndProtocols - errors', () => {
      it('should have a getLoadbalancersslciphersAndProtocols function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancersslciphersAndProtocols === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsectionssectionIdactionrevise - errors', () => {
      it('should have a postFirewallsectionssectionIdactionrevise function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsectionssectionIdactionrevise === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postFirewallsectionssectionIdactionrevise(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallSection', (done) => {
        try {
          a.postFirewallsectionssectionIdactionrevise('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'FirewallSection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallexcludelistactionaddMember - errors', () => {
      it('should have a postFirewallexcludelistactionaddMember function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallexcludelistactionaddMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ResourceReference', (done) => {
        try {
          a.postFirewallexcludelistactionaddMember(null, (data, error) => {
            try {
              const displayE = 'ResourceReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallexcludelistactionaddMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallsectionssectionIdrulesstats - errors', () => {
      it('should have a getFirewallsectionssectionIdrulesstats function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallsectionssectionIdrulesstats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getFirewallsectionssectionIdrulesstats(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallsectionssectionIdrulesstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancerapplicationProfiles - errors', () => {
      it('should have a postLoadbalancerapplicationProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancerapplicationProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbAppProfile', (done) => {
        try {
          a.postLoadbalancerapplicationProfiles(null, (data, error) => {
            try {
              const displayE = 'LbAppProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerapplicationProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerapplicationProfiles - errors', () => {
      it('should have a getLoadbalancerapplicationProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerapplicationProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancermonitorsmonitorId - errors', () => {
      it('should have a putLoadbalancermonitorsmonitorId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancermonitorsmonitorId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitorid', (done) => {
        try {
          a.putLoadbalancermonitorsmonitorId(null, null, (data, error) => {
            try {
              const displayE = 'monitorid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancermonitorsmonitorId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbMonitor', (done) => {
        try {
          a.putLoadbalancermonitorsmonitorId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbMonitor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancermonitorsmonitorId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancermonitorsmonitorId - errors', () => {
      it('should have a getLoadbalancermonitorsmonitorId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancermonitorsmonitorId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitorid', (done) => {
        try {
          a.getLoadbalancermonitorsmonitorId(null, (data, error) => {
            try {
              const displayE = 'monitorid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancermonitorsmonitorId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancermonitorsmonitorId - errors', () => {
      it('should have a deleteLoadbalancermonitorsmonitorId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancermonitorsmonitorId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitorid', (done) => {
        try {
          a.deleteLoadbalancermonitorsmonitorId(null, (data, error) => {
            try {
              const displayE = 'monitorid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancermonitorsmonitorId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionstatusactionupdateStatus - errors', () => {
      it('should have a postNetworkEncryptionstatusactionupdateStatus function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionstatusactionupdateStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing context', (done) => {
        try {
          a.postNetworkEncryptionstatusactionupdateStatus(null, null, (data, error) => {
            try {
              const displayE = 'context is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionstatusactionupdateStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancerclientSslProfilesclientSslProfileId - errors', () => {
      it('should have a putLoadbalancerclientSslProfilesclientSslProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancerclientSslProfilesclientSslProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientsslprofileid', (done) => {
        try {
          a.putLoadbalancerclientSslProfilesclientSslProfileId(null, null, (data, error) => {
            try {
              const displayE = 'clientsslprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerclientSslProfilesclientSslProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbClientSslProfile', (done) => {
        try {
          a.putLoadbalancerclientSslProfilesclientSslProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbClientSslProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerclientSslProfilesclientSslProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerclientSslProfilesclientSslProfileId - errors', () => {
      it('should have a getLoadbalancerclientSslProfilesclientSslProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerclientSslProfilesclientSslProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientsslprofileid', (done) => {
        try {
          a.getLoadbalancerclientSslProfilesclientSslProfileId(null, (data, error) => {
            try {
              const displayE = 'clientsslprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerclientSslProfilesclientSslProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancerclientSslProfilesclientSslProfileId - errors', () => {
      it('should have a deleteLoadbalancerclientSslProfilesclientSslProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancerclientSslProfilesclientSslProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientsslprofileid', (done) => {
        try {
          a.deleteLoadbalancerclientSslProfilesclientSslProfileId(null, (data, error) => {
            try {
              const displayE = 'clientsslprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancerclientSslProfilesclientSslProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetworkEncryptionsectionssectionIdrulesruleId - errors', () => {
      it('should have a putNetworkEncryptionsectionssectionIdrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.putNetworkEncryptionsectionssectionIdrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.putNetworkEncryptionsectionssectionIdrulesruleId(null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.putNetworkEncryptionsectionssectionIdrulesruleId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneRule', (done) => {
        try {
          a.putNetworkEncryptionsectionssectionIdrulesruleId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'DneRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkEncryptionsectionssectionIdrulesruleId - errors', () => {
      it('should have a deleteNetworkEncryptionsectionssectionIdrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkEncryptionsectionssectionIdrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.deleteNetworkEncryptionsectionssectionIdrulesruleId(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNetworkEncryptionsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.deleteNetworkEncryptionsectionssectionIdrulesruleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteNetworkEncryptionsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionsectionssectionIdrulesruleId - errors', () => {
      it('should have a getNetworkEncryptionsectionssectionIdrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionsectionssectionIdrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionIdrulesruleId(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionIdrulesruleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdpoolsstatistics - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdpoolsstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdpoolsstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdpoolsstatistics(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdpoolsstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatus - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatus(null, null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualserverid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualserverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatistics - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatistics(null, null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualserverid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatistics('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'virtualserverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdvirtualServersvirtualServerIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsectionssectionIdrulesruleIdactionrevise - errors', () => {
      it('should have a postFirewallsectionssectionIdrulesruleIdactionrevise function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsectionssectionIdrulesruleIdactionrevise === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postFirewallsectionssectionIdrulesruleIdactionrevise(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdrulesruleIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.postFirewallsectionssectionIdrulesruleIdactionrevise('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdrulesruleIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallRule', (done) => {
        try {
          a.postFirewallsectionssectionIdrulesruleIdactionrevise('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'FirewallRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdrulesruleIdactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionsectionssectionIdstate - errors', () => {
      it('should have a getNetworkEncryptionsectionssectionIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionsectionssectionIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionIdstate(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMdProxies - errors', () => {
      it('should have a postMdProxies function', (done) => {
        try {
          assert.equal(true, typeof a.postMdProxies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MetadataProxy', (done) => {
        try {
          a.postMdProxies(null, (data, error) => {
            try {
              const displayE = 'MetadataProxy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postMdProxies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMdProxies - errors', () => {
      it('should have a getMdProxies function', (done) => {
        try {
          assert.equal(true, typeof a.getMdProxies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallsectionssectionIdrules - errors', () => {
      it('should have a postFirewallsectionssectionIdrules function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallsectionssectionIdrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postFirewallsectionssectionIdrules(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallRule', (done) => {
        try {
          a.postFirewallsectionssectionIdrules('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'FirewallRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallsectionssectionIdrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallsectionssectionIdrules - errors', () => {
      it('should have a getFirewallsectionssectionIdrules function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallsectionssectionIdrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getFirewallsectionssectionIdrules(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallsectionssectionIdrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdvirtualServersstatistics - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdvirtualServersstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdvirtualServersstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdvirtualServersstatistics(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdvirtualServersstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionkeyPolicieskeyPolicyIdactionrotate - errors', () => {
      it('should have a postNetworkEncryptionkeyPolicieskeyPolicyIdactionrotate function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionkeyPolicieskeyPolicyIdactionrotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypolicyid', (done) => {
        try {
          a.postNetworkEncryptionkeyPolicieskeyPolicyIdactionrotate(null, (data, error) => {
            try {
              const displayE = 'keypolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionkeyPolicieskeyPolicyIdactionrotate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putFirewallsectionssectionId - errors', () => {
      it('should have a putFirewallsectionssectionId function', (done) => {
        try {
          assert.equal(true, typeof a.putFirewallsectionssectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.putFirewallsectionssectionId(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFirewallsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallSection', (done) => {
        try {
          a.putFirewallsectionssectionId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'FirewallSection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFirewallsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallsectionssectionId - errors', () => {
      it('should have a getFirewallsectionssectionId function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallsectionssectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getFirewallsectionssectionId(null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFirewallsectionssectionId - errors', () => {
      it('should have a deleteFirewallsectionssectionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFirewallsectionssectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.deleteFirewallsectionssectionId(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteFirewallsectionssectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putFirewallsectionssectionIdrulesruleId - errors', () => {
      it('should have a putFirewallsectionssectionIdrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.putFirewallsectionssectionIdrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.putFirewallsectionssectionIdrulesruleId(null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFirewallsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.putFirewallsectionssectionIdrulesruleId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFirewallsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FirewallRule', (done) => {
        try {
          a.putFirewallsectionssectionIdrulesruleId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'FirewallRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFirewallsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFirewallsectionssectionIdrulesruleId - errors', () => {
      it('should have a deleteFirewallsectionssectionIdrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFirewallsectionssectionIdrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.deleteFirewallsectionssectionIdrulesruleId(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteFirewallsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.deleteFirewallsectionssectionIdrulesruleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteFirewallsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallsectionssectionIdrulesruleId - errors', () => {
      it('should have a getFirewallsectionssectionIdrulesruleId function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallsectionssectionIdrulesruleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getFirewallsectionssectionIdrulesruleId(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getFirewallsectionssectionIdrulesruleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallsectionssectionIdrulesruleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMdProxiesproxyIdlogicalSwitchIdstatus - errors', () => {
      it('should have a getMdProxiesproxyIdlogicalSwitchIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getMdProxiesproxyIdlogicalSwitchIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyid', (done) => {
        try {
          a.getMdProxiesproxyIdlogicalSwitchIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'proxyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getMdProxiesproxyIdlogicalSwitchIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logicalswitchid', (done) => {
        try {
          a.getMdProxiesproxyIdlogicalSwitchIdstatus('fakeparam', null, (data, error) => {
            try {
              const displayE = 'logicalswitchid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getMdProxiesproxyIdlogicalSwitchIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallsectionssectionIdstate - errors', () => {
      it('should have a getFirewallsectionssectionIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallsectionssectionIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getFirewallsectionssectionIdstate(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallsectionssectionIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMdProxiesproxyIdstatistics - errors', () => {
      it('should have a getMdProxiesproxyIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getMdProxiesproxyIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyid', (done) => {
        try {
          a.getMdProxiesproxyIdstatistics(null, null, null, (data, error) => {
            try {
              const displayE = 'proxyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getMdProxiesproxyIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdstatistics - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdstatistics(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetworkEncryptionkeyPoliciesresetencryptType - errors', () => {
      it('should have a putNetworkEncryptionkeyPoliciesresetencryptType function', (done) => {
        try {
          assert.equal(true, typeof a.putNetworkEncryptionkeyPoliciesresetencryptType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encrypttype', (done) => {
        try {
          a.putNetworkEncryptionkeyPoliciesresetencryptType(null, (data, error) => {
            try {
              const displayE = 'encrypttype is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionkeyPoliciesresetencryptType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdpoolsstatus - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdpoolsstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdpoolsstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdpoolsstatus(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdpoolsstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallsectionssectionIdrulesruleIdstats - errors', () => {
      it('should have a getFirewallsectionssectionIdrulesruleIdstats function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallsectionssectionIdrulesruleIdstats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getFirewallsectionssectionIdrulesruleIdstats(null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallsectionssectionIdrulesruleIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getFirewallsectionssectionIdrulesruleIdstats('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallsectionssectionIdrulesruleIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallrulesruleIdstate - errors', () => {
      it('should have a getFirewallrulesruleIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallrulesruleIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getFirewallrulesruleIdstate(null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFirewallrulesruleIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallexcludelistactioncheckIfExists - errors', () => {
      it('should have a postFirewallexcludelistactioncheckIfExists function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallexcludelistactioncheckIfExists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.postFirewallexcludelistactioncheckIfExists(null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallexcludelistactioncheckIfExists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionkeyPolicieskeyPolicyIdactionrevoke - errors', () => {
      it('should have a postNetworkEncryptionkeyPolicieskeyPolicyIdactionrevoke function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionkeyPolicieskeyPolicyIdactionrevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypolicyid', (done) => {
        try {
          a.postNetworkEncryptionkeyPolicieskeyPolicyIdactionrevoke(null, (data, error) => {
            try {
              const displayE = 'keypolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionkeyPolicieskeyPolicyIdactionrevoke', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancerclientSslProfiles - errors', () => {
      it('should have a postLoadbalancerclientSslProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancerclientSslProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbClientSslProfile', (done) => {
        try {
          a.postLoadbalancerclientSslProfiles(null, (data, error) => {
            try {
              const displayE = 'LbClientSslProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerclientSslProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerclientSslProfiles - errors', () => {
      it('should have a getLoadbalancerclientSslProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerclientSslProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallstatsactionreset - errors', () => {
      it('should have a postFirewallstatsactionreset function', (done) => {
        try {
          assert.equal(true, typeof a.postFirewallstatsactionreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing category', (done) => {
        try {
          a.postFirewallstatsactionreset(null, (data, error) => {
            try {
              const displayE = 'category is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFirewallstatsactionreset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple - errors', () => {
      it('should have a postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple(null, null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneRuleList', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'DneRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdrulesactioncreateMultiple', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpserversserverIdleases - errors', () => {
      it('should have a getDhcpserversserverIdleases function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpserversserverIdleases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverid', (done) => {
        try {
          a.getDhcpserversserverIdleases(null, null, null, null, (data, error) => {
            try {
              const displayE = 'serverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getDhcpserversserverIdleases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancermonitors - errors', () => {
      it('should have a postLoadbalancermonitors function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancermonitors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbMonitor', (done) => {
        try {
          a.postLoadbalancermonitors(null, (data, error) => {
            try {
              const displayE = 'LbMonitor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancermonitors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancermonitors - errors', () => {
      it('should have a getLoadbalancermonitors function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancermonitors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancerservices - errors', () => {
      it('should have a postLoadbalancerservices function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancerservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbService', (done) => {
        try {
          a.postLoadbalancerservices(null, (data, error) => {
            try {
              const displayE = 'LbService is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancerservices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservices - errors', () => {
      it('should have a getLoadbalancerservices function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetworkEncryptionconfig - errors', () => {
      it('should have a putNetworkEncryptionconfig function', (done) => {
        try {
          assert.equal(true, typeof a.putNetworkEncryptionconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneGlobalConfig', (done) => {
        try {
          a.putNetworkEncryptionconfig(null, (data, error) => {
            try {
              const displayE = 'DneGlobalConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNetworkEncryptionconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionconfig - errors', () => {
      it('should have a getNetworkEncryptionconfig function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionsectionssectionIdrulesruleIdstate - errors', () => {
      it('should have a getNetworkEncryptionsectionssectionIdrulesruleIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionsectionssectionIdrulesruleIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionIdrulesruleIdstate(null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionIdrulesruleIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionIdrulesruleIdstate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionIdrulesruleIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdstatus - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallstatus - errors', () => {
      it('should have a getFirewallstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancervirtualServersvirtualServerId - errors', () => {
      it('should have a putLoadbalancervirtualServersvirtualServerId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancervirtualServersvirtualServerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualserverid', (done) => {
        try {
          a.putLoadbalancervirtualServersvirtualServerId(null, null, (data, error) => {
            try {
              const displayE = 'virtualserverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancervirtualServersvirtualServerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbVirtualServer', (done) => {
        try {
          a.putLoadbalancervirtualServersvirtualServerId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbVirtualServer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancervirtualServersvirtualServerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancervirtualServersvirtualServerId - errors', () => {
      it('should have a deleteLoadbalancervirtualServersvirtualServerId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancervirtualServersvirtualServerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualserverid', (done) => {
        try {
          a.deleteLoadbalancervirtualServersvirtualServerId(null, null, (data, error) => {
            try {
              const displayE = 'virtualserverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancervirtualServersvirtualServerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancervirtualServersvirtualServerId - errors', () => {
      it('should have a getLoadbalancervirtualServersvirtualServerId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancervirtualServersvirtualServerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualserverid', (done) => {
        try {
          a.getLoadbalancervirtualServersvirtualServerId(null, (data, error) => {
            try {
              const displayE = 'virtualserverid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancervirtualServersvirtualServerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkEncryptionsectionssectionIdactionupdateWithRules - errors', () => {
      it('should have a postNetworkEncryptionsectionssectionIdactionupdateWithRules function', (done) => {
        try {
          assert.equal(true, typeof a.postNetworkEncryptionsectionssectionIdactionupdateWithRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionupdateWithRules(null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionupdateWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DneSectionRuleList', (done) => {
        try {
          a.postNetworkEncryptionsectionssectionIdactionupdateWithRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'DneSectionRuleList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postNetworkEncryptionsectionssectionIdactionupdateWithRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionsectionssectionIdrulesruleIdstats - errors', () => {
      it('should have a getNetworkEncryptionsectionssectionIdrulesruleIdstats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionsectionssectionIdrulesruleIdstats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sectionid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionIdrulesruleIdstats(null, null, null, (data, error) => {
            try {
              const displayE = 'sectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionIdrulesruleIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleid', (done) => {
        try {
          a.getNetworkEncryptionsectionssectionIdrulesruleIdstats('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionsectionssectionIdrulesruleIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLoadbalancerpersistenceProfilespersistenceProfileId - errors', () => {
      it('should have a putLoadbalancerpersistenceProfilespersistenceProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putLoadbalancerpersistenceProfilespersistenceProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing persistenceprofileid', (done) => {
        try {
          a.putLoadbalancerpersistenceProfilespersistenceProfileId(null, null, (data, error) => {
            try {
              const displayE = 'persistenceprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerpersistenceProfilespersistenceProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbPersistenceProfile', (done) => {
        try {
          a.putLoadbalancerpersistenceProfilespersistenceProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'LbPersistenceProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLoadbalancerpersistenceProfilespersistenceProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerpersistenceProfilespersistenceProfileId - errors', () => {
      it('should have a getLoadbalancerpersistenceProfilespersistenceProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerpersistenceProfilespersistenceProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing persistenceprofileid', (done) => {
        try {
          a.getLoadbalancerpersistenceProfilespersistenceProfileId(null, (data, error) => {
            try {
              const displayE = 'persistenceprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerpersistenceProfilespersistenceProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoadbalancerpersistenceProfilespersistenceProfileId - errors', () => {
      it('should have a deleteLoadbalancerpersistenceProfilespersistenceProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoadbalancerpersistenceProfilespersistenceProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing persistenceprofileid', (done) => {
        try {
          a.deleteLoadbalancerpersistenceProfilespersistenceProfileId(null, (data, error) => {
            try {
              const displayE = 'persistenceprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLoadbalancerpersistenceProfilespersistenceProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEncryptionkeyPolicieskeypolicyIdrules - errors', () => {
      it('should have a getNetworkEncryptionkeyPolicieskeypolicyIdrules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEncryptionkeyPolicieskeypolicyIdrules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keypolicyid', (done) => {
        try {
          a.getNetworkEncryptionkeyPolicieskeypolicyIdrules(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'keypolicyid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNetworkEncryptionkeyPolicieskeypolicyIdrules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancerservicesserviceIdpoolspoolIdstatistics - errors', () => {
      it('should have a getLoadbalancerservicesserviceIdpoolspoolIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancerservicesserviceIdpoolspoolIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdpoolspoolIdstatistics(null, null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdpoolspoolIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getLoadbalancerservicesserviceIdpoolspoolIdstatistics('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLoadbalancerservicesserviceIdpoolspoolIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoadbalancervirtualServers - errors', () => {
      it('should have a postLoadbalancervirtualServers function', (done) => {
        try {
          assert.equal(true, typeof a.postLoadbalancervirtualServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LbVirtualServer', (done) => {
        try {
          a.postLoadbalancervirtualServers(null, (data, error) => {
            try {
              const displayE = 'LbVirtualServer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLoadbalancervirtualServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoadbalancervirtualServers - errors', () => {
      it('should have a getLoadbalancervirtualServers function', (done) => {
        try {
          assert.equal(true, typeof a.getLoadbalancervirtualServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEulacontent - errors', () => {
      it('should have a getEulacontent function', (done) => {
        try {
          assert.equal(true, typeof a.getEulacontent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLicense - errors', () => {
      it('should have a putLicense function', (done) => {
        try {
          assert.equal(true, typeof a.putLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing License', (done) => {
        try {
          a.putLicense(null, (data, error) => {
            try {
              const displayE = 'License is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicense - errors', () => {
      it('should have a getLicense function', (done) => {
        try {
          assert.equal(true, typeof a.getLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEulaacceptance - errors', () => {
      it('should have a getEulaacceptance function', (done) => {
        try {
          assert.equal(true, typeof a.getEulaacceptance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpgradeeulaaccept - errors', () => {
      it('should have a postUpgradeeulaaccept function', (done) => {
        try {
          assert.equal(true, typeof a.postUpgradeeulaaccept === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseslicensesUsage - errors', () => {
      it('should have a getLicenseslicensesUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseslicensesUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseslicenseKey - errors', () => {
      it('should have a getLicenseslicenseKey function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseslicenseKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licensekey', (done) => {
        try {
          a.getLicenseslicenseKey(null, (data, error) => {
            try {
              const displayE = 'licensekey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLicenseslicenseKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLicenseslicenseKey - errors', () => {
      it('should have a deleteLicenseslicenseKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLicenseslicenseKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licensekey', (done) => {
        try {
          a.deleteLicenseslicenseKey(null, (data, error) => {
            try {
              const displayE = 'licensekey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteLicenseslicenseKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEulaaccept - errors', () => {
      it('should have a postEulaaccept function', (done) => {
        try {
          assert.equal(true, typeof a.postEulaaccept === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseslicensesUsageformatcsv - errors', () => {
      it('should have a getLicenseslicensesUsageformatcsv function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseslicensesUsageformatcsv === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeeulacontent - errors', () => {
      it('should have a getUpgradeeulacontent function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeeulacontent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLicensesactiondelete - errors', () => {
      it('should have a postLicensesactiondelete function', (done) => {
        try {
          assert.equal(true, typeof a.postLicensesactiondelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing License', (done) => {
        try {
          a.postLicensesactiondelete(null, (data, error) => {
            try {
              const displayE = 'License is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLicensesactiondelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLicenses - errors', () => {
      it('should have a postLicenses function', (done) => {
        try {
          assert.equal(true, typeof a.postLicenses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing License', (done) => {
        try {
          a.postLicenses(null, (data, error) => {
            try {
              const displayE = 'License is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postLicenses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenses - errors', () => {
      it('should have a getLicenses function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUpgradeeulaacceptance - errors', () => {
      it('should have a getUpgradeeulaacceptance function', (done) => {
        try {
          assert.equal(true, typeof a.getUpgradeeulaacceptance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservicesnsserviceName - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservicesnsserviceName function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservicesnsserviceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservicesnsserviceName(null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservicesnsserviceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsservicename', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservicesnsserviceName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nsservicename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservicesnsserviceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxt - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxt function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxt(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxt', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradeploymentZonesdeploymentZoneIdenforcementPoints - errors', () => {
      it('should have a getInfradeploymentZonesdeploymentZoneIdenforcementPoints function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradeploymentZonesdeploymentZoneIdenforcementPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deploymentzoneid', (done) => {
        try {
          a.getInfradeploymentZonesdeploymentZoneIdenforcementPoints(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deploymentzoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradeploymentZonesdeploymentZoneIdenforcementPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSectionsfirewallSectionId - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSectionsfirewallSectionId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSectionsfirewallSectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSectionsfirewallSectionId(null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSectionsfirewallSectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallsectionid', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSectionsfirewallSectionId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'firewallsectionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSectionsfirewallSectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroups - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroups function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroups(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfraservicesserviceIdserviceEntriesserviceEntryId - errors', () => {
      it('should have a postInfraservicesserviceIdserviceEntriesserviceEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfraservicesserviceIdserviceEntriesserviceEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.postInfraservicesserviceIdserviceEntriesserviceEntryId(null, null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfraservicesserviceIdserviceEntriesserviceEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceentryid', (done) => {
        try {
          a.postInfraservicesserviceIdserviceEntriesserviceEntryId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfraservicesserviceIdserviceEntriesserviceEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ServiceEntry', (done) => {
        try {
          a.postInfraservicesserviceIdserviceEntriesserviceEntryId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ServiceEntry is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfraservicesserviceIdserviceEntriesserviceEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfraservicesserviceIdserviceEntriesserviceEntryId - errors', () => {
      it('should have a deleteInfraservicesserviceIdserviceEntriesserviceEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfraservicesserviceIdserviceEntriesserviceEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.deleteInfraservicesserviceIdserviceEntriesserviceEntryId(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfraservicesserviceIdserviceEntriesserviceEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceentryid', (done) => {
        try {
          a.deleteInfraservicesserviceIdserviceEntriesserviceEntryId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfraservicesserviceIdserviceEntriesserviceEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfraservicesserviceIdserviceEntriesserviceEntryId - errors', () => {
      it('should have a getInfraservicesserviceIdserviceEntriesserviceEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfraservicesserviceIdserviceEntriesserviceEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getInfraservicesserviceIdserviceEntriesserviceEntryId(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfraservicesserviceIdserviceEntriesserviceEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceentryid', (done) => {
        try {
          a.getInfraservicesserviceIdserviceEntriesserviceEntryId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfraservicesserviceIdserviceEntriesserviceEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroups - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroups function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroups(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplates - errors', () => {
      it('should have a getTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId - errors', () => {
      it('should have a postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId(null, null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domaindeploymentmapid', (done) => {
        try {
          a.postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domaindeploymentmapid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing DomainDeploymentMap', (done) => {
        try {
          a.postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'DomainDeploymentMap is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId - errors', () => {
      it('should have a getInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.getInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId(null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domaindeploymentmapid', (done) => {
        try {
          a.getInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domaindeploymentmapid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId - errors', () => {
      it('should have a deleteInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.deleteInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId(null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domaindeploymentmapid', (done) => {
        try {
          a.deleteInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domaindeploymentmapid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradomainsdomainIddomainDeploymentMapsdomainDeploymentMapId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradeploymentZones - errors', () => {
      it('should have a getInfradeploymentZones function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradeploymentZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomainsdomainIdcommunicationMapcommunicationEntries - errors', () => {
      it('should have a getInfradomainsdomainIdcommunicationMapcommunicationEntries function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomainsdomainIdcommunicationMapcommunicationEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.getInfradomainsdomainIdcommunicationMapcommunicationEntries(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIdcommunicationMapcommunicationEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfradomainsdomainIdcommunicationMapactionrevise - errors', () => {
      it('should have a postInfradomainsdomainIdcommunicationMapactionrevise function', (done) => {
        try {
          assert.equal(true, typeof a.postInfradomainsdomainIdcommunicationMapactionrevise === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.postInfradomainsdomainIdcommunicationMapactionrevise(null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdcommunicationMapactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CommunicationMap', (done) => {
        try {
          a.postInfradomainsdomainIdcommunicationMapactionrevise('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'CommunicationMap is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdcommunicationMapactionrevise', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxtmacSetName - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxtmacSetName function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxtmacSetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxtmacSetName(null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxtmacSetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing macsetname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxtmacSetName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'macsetname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxtmacSetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroupssecuritygroupName - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroupssecuritygroupName function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroupssecuritygroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroupssecuritygroupName(null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroupssecuritygroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securitygroupname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroupssecuritygroupName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'securitygroupname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamegroupssecuritygroupssecuritygroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfraservicesserviceId - errors', () => {
      it('should have a postInfraservicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfraservicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.postInfraservicesserviceId(null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfraservicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Service', (done) => {
        try {
          a.postInfraservicesserviceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'Service is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfraservicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfraservicesserviceId - errors', () => {
      it('should have a getInfraservicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfraservicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getInfraservicesserviceId(null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfraservicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfraservicesserviceId - errors', () => {
      it('should have a deleteInfraservicesserviceId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfraservicesserviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.deleteInfraservicesserviceId(null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfraservicesserviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId - errors', () => {
      it('should have a postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deploymentzoneid', (done) => {
        try {
          a.postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId(null, null, null, (data, error) => {
            try {
              const displayE = 'deploymentzoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointid', (done) => {
        try {
          a.postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing EnforcementPoint', (done) => {
        try {
          a.postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'EnforcementPoint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId - errors', () => {
      it('should have a getInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deploymentzoneid', (done) => {
        try {
          a.getInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId(null, null, (data, error) => {
            try {
              const displayE = 'deploymentzoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointid', (done) => {
        try {
          a.getInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'enforcementpointid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId - errors', () => {
      it('should have a deleteInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deploymentzoneid', (done) => {
        try {
          a.deleteInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId(null, null, (data, error) => {
            try {
              const displayE = 'deploymentzoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointid', (done) => {
        try {
          a.deleteInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'enforcementpointid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradeploymentZonesdeploymentZoneIdenforcementPointsenforcementpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfradomainsdomainIdgroupsgroupId - errors', () => {
      it('should have a postInfradomainsdomainIdgroupsgroupId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfradomainsdomainIdgroupsgroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.postInfradomainsdomainIdgroupsgroupId(null, null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdgroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.postInfradomainsdomainIdgroupsgroupId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdgroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Group', (done) => {
        try {
          a.postInfradomainsdomainIdgroupsgroupId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'Group is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdgroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfradomainsdomainIdgroupsgroupId - errors', () => {
      it('should have a deleteInfradomainsdomainIdgroupsgroupId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfradomainsdomainIdgroupsgroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.deleteInfradomainsdomainIdgroupsgroupId(null, null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradomainsdomainIdgroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.deleteInfradomainsdomainIdgroupsgroupId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradomainsdomainIdgroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomainsdomainIdgroupsgroupId - errors', () => {
      it('should have a getInfradomainsdomainIdgroupsgroupId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomainsdomainIdgroupsgroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.getInfradomainsdomainIdgroupsgroupId(null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIdgroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupid', (done) => {
        try {
          a.getInfradomainsdomainIdgroupsgroupId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIdgroupsgroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfracommunicationProfiles - errors', () => {
      it('should have a getInfracommunicationProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getInfracommunicationProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxtipSetName - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxtipSetName function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxtipSetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxtipSetName(null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxtipSetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsetname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxtipSetName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipsetname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNameipSetsipSetsNsxtipSetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntries - errors', () => {
      it('should have a getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntries function', (done) => {
        try {
          assert.equal(true, typeof a.getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileid', (done) => {
        try {
          a.getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntries(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'communicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfra - errors', () => {
      it('should have a postInfra function', (done) => {
        try {
          assert.equal(true, typeof a.postInfra === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Infra', (done) => {
        try {
          a.postInfra(null, (data, error) => {
            try {
              const displayE = 'Infra is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfra', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfra - errors', () => {
      it('should have a getInfra function', (done) => {
        try {
          assert.equal(true, typeof a.getInfra === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfracommunicationProfilescommunicationProfileId - errors', () => {
      it('should have a postInfracommunicationProfilescommunicationProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfracommunicationProfilescommunicationProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileid', (done) => {
        try {
          a.postInfracommunicationProfilescommunicationProfileId(null, null, (data, error) => {
            try {
              const displayE = 'communicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfracommunicationProfilescommunicationProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CommunicationProfile', (done) => {
        try {
          a.postInfracommunicationProfilescommunicationProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'CommunicationProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfracommunicationProfilescommunicationProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfracommunicationProfilescommunicationProfileId - errors', () => {
      it('should have a getInfracommunicationProfilescommunicationProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfracommunicationProfilescommunicationProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileid', (done) => {
        try {
          a.getInfracommunicationProfilescommunicationProfileId(null, (data, error) => {
            try {
              const displayE = 'communicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfracommunicationProfilescommunicationProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfracommunicationProfilescommunicationProfileId - errors', () => {
      it('should have a deleteInfracommunicationProfilescommunicationProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfracommunicationProfilescommunicationProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileid', (done) => {
        try {
          a.deleteInfracommunicationProfilescommunicationProfileId(null, (data, error) => {
            try {
              const displayE = 'communicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfracommunicationProfilescommunicationProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId - errors', () => {
      it('should have a postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileid', (done) => {
        try {
          a.postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId(null, null, null, (data, error) => {
            try {
              const displayE = 'communicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileentryid', (done) => {
        try {
          a.postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'communicationprofileentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CommunicationProfileEntry', (done) => {
        try {
          a.postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'CommunicationProfileEntry is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId - errors', () => {
      it('should have a deleteInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileid', (done) => {
        try {
          a.deleteInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId(null, null, (data, error) => {
            try {
              const displayE = 'communicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileentryid', (done) => {
        try {
          a.deleteInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'communicationprofileentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId - errors', () => {
      it('should have a getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileid', (done) => {
        try {
          a.getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId(null, null, (data, error) => {
            try {
              const displayE = 'communicationprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationprofileentryid', (done) => {
        try {
          a.getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'communicationprofileentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfracommunicationProfilescommunicationProfileIdcommunicationProfileEntriescommunicationProfileEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservices - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservices function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservices(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNameservicesnsservices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTemplatestemplateId - errors', () => {
      it('should have a postTemplatestemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.postTemplatestemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.postTemplatestemplateId(null, null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTemplatestemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing PolicyTemplate', (done) => {
        try {
          a.postTemplatestemplateId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'PolicyTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTemplatestemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplatestemplateId - errors', () => {
      it('should have a getTemplatestemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplatestemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.getTemplatestemplateId(null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTemplatestemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplatestemplateId - errors', () => {
      it('should have a deleteTemplatestemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplatestemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.deleteTemplatestemplateId(null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTemplatestemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfraservicesserviceIdserviceEntries - errors', () => {
      it('should have a getInfraservicesserviceIdserviceEntries function', (done) => {
        try {
          assert.equal(true, typeof a.getInfraservicesserviceIdserviceEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceid', (done) => {
        try {
          a.getInfraservicesserviceIdserviceEntries(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serviceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfraservicesserviceIdserviceEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSections - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSections function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSections(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamefirewallsfirewallSections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfraservices - errors', () => {
      it('should have a getInfraservices function', (done) => {
        try {
          assert.equal(true, typeof a.getInfraservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfradomainsdomainId - errors', () => {
      it('should have a postInfradomainsdomainId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfradomainsdomainId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.postInfradomainsdomainId(null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Domain', (done) => {
        try {
          a.postInfradomainsdomainId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'Domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomainsdomainId - errors', () => {
      it('should have a getInfradomainsdomainId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomainsdomainId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.getInfradomainsdomainId(null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfradomainsdomainId - errors', () => {
      it('should have a deleteInfradomainsdomainId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfradomainsdomainId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.deleteInfradomainsdomainId(null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradomainsdomainId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPoints - errors', () => {
      it('should have a getInfrarealizedStateenforcementPoints function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradeploymentZonesdeploymentZoneId - errors', () => {
      it('should have a getInfradeploymentZonesdeploymentZoneId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradeploymentZonesdeploymentZoneId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deploymentzoneid', (done) => {
        try {
          a.getInfradeploymentZonesdeploymentZoneId(null, (data, error) => {
            try {
              const displayE = 'deploymentzoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradeploymentZonesdeploymentZoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroupsnsgroupName - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroupsnsgroupName function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroupsnsgroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroupsnsgroupName(null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroupsnsgroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nsgroupname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroupsnsgroupName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nsgroupname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamegroupsnsgroupsnsgroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxt - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxt function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxt(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointNamemacSetsmacSetsNsxt', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomainsdomainIddomainDeploymentMaps - errors', () => {
      it('should have a getInfradomainsdomainIddomainDeploymentMaps function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomainsdomainIddomainDeploymentMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.getInfradomainsdomainIddomainDeploymentMaps(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIddomainDeploymentMaps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfrarealizedStateenforcementPointsenforcementPointName - errors', () => {
      it('should have a getInfrarealizedStateenforcementPointsenforcementPointName function', (done) => {
        try {
          assert.equal(true, typeof a.getInfrarealizedStateenforcementPointsenforcementPointName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enforcementpointname', (done) => {
        try {
          a.getInfrarealizedStateenforcementPointsenforcementPointName(null, (data, error) => {
            try {
              const displayE = 'enforcementpointname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfrarealizedStateenforcementPointsenforcementPointName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId - errors', () => {
      it('should have a postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId(null, null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationentryid', (done) => {
        try {
          a.postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'communicationentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CommunicationEntry', (done) => {
        try {
          a.postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'CommunicationEntry is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId - errors', () => {
      it('should have a deleteInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.deleteInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId(null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationentryid', (done) => {
        try {
          a.deleteInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'communicationentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId - errors', () => {
      it('should have a getInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.getInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId(null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communicationentryid', (done) => {
        try {
          a.getInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'communicationentryid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIdcommunicationMapcommunicationEntriescommunicationEntryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomains - errors', () => {
      it('should have a getInfradomains function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInfradomainsdomainIdcommunicationMap - errors', () => {
      it('should have a postInfradomainsdomainIdcommunicationMap function', (done) => {
        try {
          assert.equal(true, typeof a.postInfradomainsdomainIdcommunicationMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.postInfradomainsdomainIdcommunicationMap(null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdcommunicationMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CommunicationMap', (done) => {
        try {
          a.postInfradomainsdomainIdcommunicationMap('fakeparam', null, (data, error) => {
            try {
              const displayE = 'CommunicationMap is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postInfradomainsdomainIdcommunicationMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomainsdomainIdcommunicationMap - errors', () => {
      it('should have a getInfradomainsdomainIdcommunicationMap function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomainsdomainIdcommunicationMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.getInfradomainsdomainIdcommunicationMap(null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIdcommunicationMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTemplatestemplateIdactiondeploy - errors', () => {
      it('should have a postTemplatestemplateIdactiondeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postTemplatestemplateIdactiondeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.postTemplatestemplateIdactiondeploy(null, null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTemplatestemplateIdactiondeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing PolicyTemplateParameters', (done) => {
        try {
          a.postTemplatestemplateIdactiondeploy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'PolicyTemplateParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTemplatestemplateIdactiondeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfradomainsdomainIdgroups - errors', () => {
      it('should have a getInfradomainsdomainIdgroups function', (done) => {
        try {
          assert.equal(true, typeof a.getInfradomainsdomainIdgroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainid', (done) => {
        try {
          a.getInfradomainsdomainIdgroups(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'domainid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getInfradomainsdomainIdgroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaarolesrole - errors', () => {
      it('should have a getAaarolesrole function', (done) => {
        try {
          assert.equal(true, typeof a.getAaarolesrole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.getAaarolesrole(null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAaarolesrole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaauserInfo - errors', () => {
      it('should have a getAaauserInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getAaauserInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaavidmgroups - errors', () => {
      it('should have a getAaavidmgroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAaavidmgroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchString', (done) => {
        try {
          a.getAaavidmgroups('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'searchString is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAaavidmgroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaavidmusers - errors', () => {
      it('should have a getAaavidmusers function', (done) => {
        try {
          assert.equal(true, typeof a.getAaavidmusers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchString', (done) => {
        try {
          a.getAaavidmusers('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'searchString is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAaavidmusers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAaaroleBindingsbindingId - errors', () => {
      it('should have a putAaaroleBindingsbindingId function', (done) => {
        try {
          assert.equal(true, typeof a.putAaaroleBindingsbindingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bindingid', (done) => {
        try {
          a.putAaaroleBindingsbindingId(null, null, (data, error) => {
            try {
              const displayE = 'bindingid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putAaaroleBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RoleBinding', (done) => {
        try {
          a.putAaaroleBindingsbindingId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'RoleBinding is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putAaaroleBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAaaroleBindingsbindingId - errors', () => {
      it('should have a deleteAaaroleBindingsbindingId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAaaroleBindingsbindingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bindingid', (done) => {
        try {
          a.deleteAaaroleBindingsbindingId(null, (data, error) => {
            try {
              const displayE = 'bindingid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteAaaroleBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaaroleBindingsbindingId - errors', () => {
      it('should have a getAaaroleBindingsbindingId function', (done) => {
        try {
          assert.equal(true, typeof a.getAaaroleBindingsbindingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bindingid', (done) => {
        try {
          a.getAaaroleBindingsbindingId(null, (data, error) => {
            try {
              const displayE = 'bindingid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAaaroleBindingsbindingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAaaregistrationToken - errors', () => {
      it('should have a postAaaregistrationToken function', (done) => {
        try {
          assert.equal(true, typeof a.postAaaregistrationToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaaroles - errors', () => {
      it('should have a getAaaroles function', (done) => {
        try {
          assert.equal(true, typeof a.getAaaroles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAaavidmsearch - errors', () => {
      it('should have a postAaavidmsearch function', (done) => {
        try {
          assert.equal(true, typeof a.postAaavidmsearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchString', (done) => {
        try {
          a.postAaavidmsearch('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'searchString is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAaavidmsearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAaaregistrationTokentoken - errors', () => {
      it('should have a deleteAaaregistrationTokentoken function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAaaregistrationTokentoken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.deleteAaaregistrationTokentoken(null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteAaaregistrationTokentoken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaaregistrationTokentoken - errors', () => {
      it('should have a getAaaregistrationTokentoken function', (done) => {
        try {
          assert.equal(true, typeof a.getAaaregistrationTokentoken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.getAaaregistrationTokentoken(null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAaaregistrationTokentoken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAaaroleBindings - errors', () => {
      it('should have a postAaaroleBindings function', (done) => {
        try {
          assert.equal(true, typeof a.postAaaroleBindings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RoleBinding', (done) => {
        try {
          a.postAaaroleBindings(null, (data, error) => {
            try {
              const displayE = 'RoleBinding is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postAaaroleBindings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaaroleBindings - errors', () => {
      it('should have a getAaaroleBindings function', (done) => {
        try {
          assert.equal(true, typeof a.getAaaroleBindings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsvniPools - errors', () => {
      it('should have a getPoolsvniPools function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsvniPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsmacPools - errors', () => {
      it('should have a getPoolsmacPools function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsmacPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPoolsipSubnets - errors', () => {
      it('should have a postPoolsipSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.postPoolsipSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpBlockSubnet', (done) => {
        try {
          a.postPoolsipSubnets(null, (data, error) => {
            try {
              const displayE = 'IpBlockSubnet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipSubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsipSubnets - errors', () => {
      it('should have a getPoolsipSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsipSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsvtepLabelPools - errors', () => {
      it('should have a getPoolsvtepLabelPools function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsvtepLabelPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPoolsipBlocksblockId - errors', () => {
      it('should have a putPoolsipBlocksblockId function', (done) => {
        try {
          assert.equal(true, typeof a.putPoolsipBlocksblockId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing blockid', (done) => {
        try {
          a.putPoolsipBlocksblockId(null, null, (data, error) => {
            try {
              const displayE = 'blockid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putPoolsipBlocksblockId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpBlock', (done) => {
        try {
          a.putPoolsipBlocksblockId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'IpBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putPoolsipBlocksblockId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsipBlocksblockId - errors', () => {
      it('should have a getPoolsipBlocksblockId function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsipBlocksblockId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing blockid', (done) => {
        try {
          a.getPoolsipBlocksblockId(null, (data, error) => {
            try {
              const displayE = 'blockid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getPoolsipBlocksblockId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePoolsipBlocksblockId - errors', () => {
      it('should have a deletePoolsipBlocksblockId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePoolsipBlocksblockId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing blockid', (done) => {
        try {
          a.deletePoolsipBlocksblockId(null, (data, error) => {
            try {
              const displayE = 'blockid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deletePoolsipBlocksblockId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsmacPoolspoolId - errors', () => {
      it('should have a getPoolsmacPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsmacPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getPoolsmacPoolspoolId(null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getPoolsmacPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPoolsipBlocks - errors', () => {
      it('should have a postPoolsipBlocks function', (done) => {
        try {
          assert.equal(true, typeof a.postPoolsipBlocks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpBlock', (done) => {
        try {
          a.postPoolsipBlocks(null, (data, error) => {
            try {
              const displayE = 'IpBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipBlocks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsipBlocks - errors', () => {
      it('should have a getPoolsipBlocks function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsipBlocks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPoolsvniPoolspoolId - errors', () => {
      it('should have a putPoolsvniPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.putPoolsvniPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.putPoolsvniPoolspoolId(null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putPoolsvniPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing VniPool', (done) => {
        try {
          a.putPoolsvniPoolspoolId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'VniPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putPoolsvniPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsvniPoolspoolId - errors', () => {
      it('should have a getPoolsvniPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsvniPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getPoolsvniPoolspoolId(null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getPoolsvniPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPoolsipSubnetssubnetId - errors', () => {
      it('should have a postPoolsipSubnetssubnetId function', (done) => {
        try {
          assert.equal(true, typeof a.postPoolsipSubnetssubnetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetid', (done) => {
        try {
          a.postPoolsipSubnetssubnetId(null, null, null, (data, error) => {
            try {
              const displayE = 'subnetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipSubnetssubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AllocationIpAddress', (done) => {
        try {
          a.postPoolsipSubnetssubnetId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'AllocationIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipSubnetssubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.postPoolsipSubnetssubnetId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipSubnetssubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePoolsipSubnetssubnetId - errors', () => {
      it('should have a deletePoolsipSubnetssubnetId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePoolsipSubnetssubnetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetid', (done) => {
        try {
          a.deletePoolsipSubnetssubnetId(null, (data, error) => {
            try {
              const displayE = 'subnetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deletePoolsipSubnetssubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsipSubnetssubnetId - errors', () => {
      it('should have a getPoolsipSubnetssubnetId function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsipSubnetssubnetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetid', (done) => {
        try {
          a.getPoolsipSubnetssubnetId(null, (data, error) => {
            try {
              const displayE = 'subnetid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getPoolsipSubnetssubnetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPoolsipPools - errors', () => {
      it('should have a postPoolsipPools function', (done) => {
        try {
          assert.equal(true, typeof a.postPoolsipPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpPool', (done) => {
        try {
          a.postPoolsipPools(null, (data, error) => {
            try {
              const displayE = 'IpPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsipPools - errors', () => {
      it('should have a getPoolsipPools function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsipPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPoolsipPoolspoolId - errors', () => {
      it('should have a putPoolsipPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.putPoolsipPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.putPoolsipPoolspoolId(null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putPoolsipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpPool', (done) => {
        try {
          a.putPoolsipPoolspoolId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'IpPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putPoolsipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPoolsipPoolspoolId - errors', () => {
      it('should have a postPoolsipPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.postPoolsipPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.postPoolsipPoolspoolId(null, null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AllocationIpAddress', (done) => {
        try {
          a.postPoolsipPoolspoolId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'AllocationIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.postPoolsipPoolspoolId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postPoolsipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsipPoolspoolId - errors', () => {
      it('should have a getPoolsipPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsipPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getPoolsipPoolspoolId(null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getPoolsipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePoolsipPoolspoolId - errors', () => {
      it('should have a deletePoolsipPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePoolsipPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.deletePoolsipPoolspoolId(null, null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deletePoolsipPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsvtepLabelPoolspoolId - errors', () => {
      it('should have a getPoolsvtepLabelPoolspoolId function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsvtepLabelPoolspoolId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getPoolsvtepLabelPoolspoolId(null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getPoolsvtepLabelPoolspoolId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoolsipPoolspoolIdallocations - errors', () => {
      it('should have a getPoolsipPoolspoolIdallocations function', (done) => {
        try {
          assert.equal(true, typeof a.getPoolsipPoolspoolIdallocations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poolid', (done) => {
        try {
          a.getPoolsipPoolspoolIdallocations(null, (data, error) => {
            try {
              const displayE = 'poolid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getPoolsipPoolspoolIdallocations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCsmawsgatewayAmis - errors', () => {
      it('should have a postCsmawsgatewayAmis function', (done) => {
        try {
          assert.equal(true, typeof a.postCsmawsgatewayAmis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AwsGatewayAmiInfo', (done) => {
        try {
          a.postCsmawsgatewayAmis(null, (data, error) => {
            try {
              const displayE = 'AwsGatewayAmiInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postCsmawsgatewayAmis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsgatewayAmis - errors', () => {
      it('should have a getCsmawsgatewayAmis function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsgatewayAmis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawskeyPairs - errors', () => {
      it('should have a getCsmawskeyPairs function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawskeyPairs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getCsmawskeyPairs(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawskeyPairs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionId', (done) => {
        try {
          a.getCsmawskeyPairs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'regionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawskeyPairs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsgatewaysvpcIdstatus - errors', () => {
      it('should have a getCsmawsgatewaysvpcIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsgatewaysvpcIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcid', (done) => {
        try {
          a.getCsmawsgatewaysvpcIdstatus(null, (data, error) => {
            try {
              const displayE = 'vpcid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawsgatewaysvpcIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCsmawsgatewaysvpcId - errors', () => {
      it('should have a putCsmawsgatewaysvpcId function', (done) => {
        try {
          assert.equal(true, typeof a.putCsmawsgatewaysvpcId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcid', (done) => {
        try {
          a.putCsmawsgatewaysvpcId(null, null, (data, error) => {
            try {
              const displayE = 'vpcid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putCsmawsgatewaysvpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AwsGatewayDeployConfig', (done) => {
        try {
          a.putCsmawsgatewaysvpcId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'AwsGatewayDeployConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putCsmawsgatewaysvpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsgatewaysvpcId - errors', () => {
      it('should have a getCsmawsgatewaysvpcId function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsgatewaysvpcId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcid', (done) => {
        try {
          a.getCsmawsgatewaysvpcId(null, (data, error) => {
            try {
              const displayE = 'vpcid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawsgatewaysvpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsregionsregionId - errors', () => {
      it('should have a getCsmawsregionsregionId function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsregionsregionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionid', (done) => {
        try {
          a.getCsmawsregionsregionId(null, (data, error) => {
            try {
              const displayE = 'regionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawsregionsregionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCsmawsgatewaysactiondeploy - errors', () => {
      it('should have a postCsmawsgatewaysactiondeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postCsmawsgatewaysactiondeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AwsGatewayDeployConfig', (done) => {
        try {
          a.postCsmawsgatewaysactiondeploy(null, (data, error) => {
            try {
              const displayE = 'AwsGatewayDeployConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postCsmawsgatewaysactiondeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmcsmstatus - errors', () => {
      it('should have a getCsmcsmstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmcsmstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsvpcs - errors', () => {
      it('should have a getCsmawsvpcs function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsvpcs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCsmnsxManagerAccounts - errors', () => {
      it('should have a postCsmnsxManagerAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.postCsmnsxManagerAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NsxManagerAccount', (done) => {
        try {
          a.postCsmnsxManagerAccounts(null, (data, error) => {
            try {
              const displayE = 'NsxManagerAccount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postCsmnsxManagerAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmnsxManagerAccounts - errors', () => {
      it('should have a getCsmnsxManagerAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmnsxManagerAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsregions - errors', () => {
      it('should have a getCsmawsregions function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsregions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmvirtualMachinesvirtualMachineId - errors', () => {
      it('should have a getCsmvirtualMachinesvirtualMachineId function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmvirtualMachinesvirtualMachineId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing virtualmachineid', (done) => {
        try {
          a.getCsmvirtualMachinesvirtualMachineId(null, (data, error) => {
            try {
              const displayE = 'virtualmachineid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmvirtualMachinesvirtualMachineId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCsmawsgatewayAmisregionId - errors', () => {
      it('should have a putCsmawsgatewayAmisregionId function', (done) => {
        try {
          assert.equal(true, typeof a.putCsmawsgatewayAmisregionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionid', (done) => {
        try {
          a.putCsmawsgatewayAmisregionId(null, null, (data, error) => {
            try {
              const displayE = 'regionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putCsmawsgatewayAmisregionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AwsGatewayAmiInfo', (done) => {
        try {
          a.putCsmawsgatewayAmisregionId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'AwsGatewayAmiInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putCsmawsgatewayAmisregionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCsmawsgatewayAmisregionId - errors', () => {
      it('should have a deleteCsmawsgatewayAmisregionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCsmawsgatewayAmisregionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionid', (done) => {
        try {
          a.deleteCsmawsgatewayAmisregionId(null, (data, error) => {
            try {
              const displayE = 'regionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteCsmawsgatewayAmisregionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsgatewayAmisregionId - errors', () => {
      it('should have a getCsmawsgatewayAmisregionId function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsgatewayAmisregionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionid', (done) => {
        try {
          a.getCsmawsgatewayAmisregionId(null, (data, error) => {
            try {
              const displayE = 'regionid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawsgatewayAmisregionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCsmnsxManagerAccountsaccountId - errors', () => {
      it('should have a putCsmnsxManagerAccountsaccountId function', (done) => {
        try {
          assert.equal(true, typeof a.putCsmnsxManagerAccountsaccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountid', (done) => {
        try {
          a.putCsmnsxManagerAccountsaccountId(null, null, (data, error) => {
            try {
              const displayE = 'accountid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putCsmnsxManagerAccountsaccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NsxManagerAccount', (done) => {
        try {
          a.putCsmnsxManagerAccountsaccountId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'NsxManagerAccount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putCsmnsxManagerAccountsaccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCsmnsxManagerAccountsaccountId - errors', () => {
      it('should have a deleteCsmnsxManagerAccountsaccountId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCsmnsxManagerAccountsaccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountid', (done) => {
        try {
          a.deleteCsmnsxManagerAccountsaccountId(null, (data, error) => {
            try {
              const displayE = 'accountid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteCsmnsxManagerAccountsaccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmnsxManagerAccountsaccountId - errors', () => {
      it('should have a getCsmnsxManagerAccountsaccountId function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmnsxManagerAccountsaccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountid', (done) => {
        try {
          a.getCsmnsxManagerAccountsaccountId(null, (data, error) => {
            try {
              const displayE = 'accountid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmnsxManagerAccountsaccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCsmawsaccountsaccountIdactionsyncInventory - errors', () => {
      it('should have a postCsmawsaccountsaccountIdactionsyncInventory function', (done) => {
        try {
          assert.equal(true, typeof a.postCsmawsaccountsaccountIdactionsyncInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountid', (done) => {
        try {
          a.postCsmawsaccountsaccountIdactionsyncInventory(null, (data, error) => {
            try {
              const displayE = 'accountid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postCsmawsaccountsaccountIdactionsyncInventory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCsmawsaccountsaccountId - errors', () => {
      it('should have a putCsmawsaccountsaccountId function', (done) => {
        try {
          assert.equal(true, typeof a.putCsmawsaccountsaccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountid', (done) => {
        try {
          a.putCsmawsaccountsaccountId(null, null, (data, error) => {
            try {
              const displayE = 'accountid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putCsmawsaccountsaccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AwsAccount', (done) => {
        try {
          a.putCsmawsaccountsaccountId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'AwsAccount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putCsmawsaccountsaccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCsmawsaccountsaccountId - errors', () => {
      it('should have a deleteCsmawsaccountsaccountId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCsmawsaccountsaccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountid', (done) => {
        try {
          a.deleteCsmawsaccountsaccountId(null, (data, error) => {
            try {
              const displayE = 'accountid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteCsmawsaccountsaccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsaccountsaccountId - errors', () => {
      it('should have a getCsmawsaccountsaccountId function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsaccountsaccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountid', (done) => {
        try {
          a.getCsmawsaccountsaccountId(null, (data, error) => {
            try {
              const displayE = 'accountid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawsaccountsaccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCsmawsaccounts - errors', () => {
      it('should have a postCsmawsaccounts function', (done) => {
        try {
          assert.equal(true, typeof a.postCsmawsaccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AwsAccount', (done) => {
        try {
          a.postCsmawsaccounts(null, (data, error) => {
            try {
              const displayE = 'AwsAccount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postCsmawsaccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsaccounts - errors', () => {
      it('should have a getCsmawsaccounts function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsaccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmvirtualMachines - errors', () => {
      it('should have a getCsmvirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmvirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCsmawsgatewaysactionundeploy - errors', () => {
      it('should have a postCsmawsgatewaysactionundeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postCsmawsgatewaysactionundeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AwsGatewayUndeployConfig', (done) => {
        try {
          a.postCsmawsgatewaysactionundeploy(null, (data, error) => {
            try {
              const displayE = 'AwsGatewayUndeployConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postCsmawsgatewaysactionundeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsvpcsvpcId - errors', () => {
      it('should have a getCsmawsvpcsvpcId function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsvpcsvpcId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcid', (done) => {
        try {
          a.getCsmawsvpcsvpcId(null, (data, error) => {
            try {
              const displayE = 'vpcid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawsvpcsvpcId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawssubnets - errors', () => {
      it('should have a getCsmawssubnets function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawssubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getCsmawssubnets(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawssubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing availabilityZoneName', (done) => {
        try {
          a.getCsmawssubnets('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'availabilityZoneName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawssubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionName', (done) => {
        try {
          a.getCsmawssubnets('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'regionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawssubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.getCsmawssubnets('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawssubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsgateways - errors', () => {
      it('should have a getCsmawsgateways function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsgateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsmawsaccountsaccountIdstatus - errors', () => {
      it('should have a getCsmawsaccountsaccountIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getCsmawsaccountsaccountIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountid', (done) => {
        try {
          a.getCsmawsaccountsaccountIdstatus(null, (data, error) => {
            try {
              const displayE = 'accountid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getCsmawsaccountsaccountIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesnodeIdmodules - errors', () => {
      it('should have a getFabricnodesnodeIdmodules function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesnodeIdmodules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getFabricnodesnodeIdmodules(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdmodules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFabricvirtualMachinesactionupdateTags - errors', () => {
      it('should have a postFabricvirtualMachinesactionupdateTags function', (done) => {
        try {
          assert.equal(true, typeof a.postFabricvirtualMachinesactionupdateTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing VirtualMachineTagUpdate', (done) => {
        try {
          a.postFabricvirtualMachinesactionupdateTags(null, (data, error) => {
            try {
              const displayE = 'VirtualMachineTagUpdate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFabricvirtualMachinesactionupdateTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabriccomputeManagerscomputeManagerIdstatus - errors', () => {
      it('should have a getFabriccomputeManagerscomputeManagerIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getFabriccomputeManagerscomputeManagerIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing computemanagerid', (done) => {
        try {
          a.getFabriccomputeManagerscomputeManagerIdstatus(null, (data, error) => {
            try {
              const displayE = 'computemanagerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabriccomputeManagerscomputeManagerIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesstatus - errors', () => {
      it('should have a getFabricnodesstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ReadNodesStatusRequestParameters', (done) => {
        try {
          a.getFabricnodesstatus(null, (data, error) => {
            try {
              const displayE = 'ReadNodesStatusRequestParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabriccomputeCollections - errors', () => {
      it('should have a getFabriccomputeCollections function', (done) => {
        try {
          assert.equal(true, typeof a.getFabriccomputeCollections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFabricnodesnodeIdactionrestartInventorySync - errors', () => {
      it('should have a postFabricnodesnodeIdactionrestartInventorySync function', (done) => {
        try {
          assert.equal(true, typeof a.postFabricnodesnodeIdactionrestartInventorySync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postFabricnodesnodeIdactionrestartInventorySync(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFabricnodesnodeIdactionrestartInventorySync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFabriccomputeCollectionFabricTemplates - errors', () => {
      it('should have a postFabriccomputeCollectionFabricTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postFabriccomputeCollectionFabricTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ComputeCollectionFabricTemplate', (done) => {
        try {
          a.postFabriccomputeCollectionFabricTemplates(null, (data, error) => {
            try {
              const displayE = 'ComputeCollectionFabricTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFabriccomputeCollectionFabricTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabriccomputeCollectionFabricTemplates - errors', () => {
      it('should have a getFabriccomputeCollectionFabricTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getFabriccomputeCollectionFabricTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricvifs - errors', () => {
      it('should have a getFabricvifs function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricvifs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesnodeIdnetworkinterfacesinterfaceId - errors', () => {
      it('should have a getFabricnodesnodeIdnetworkinterfacesinterfaceId function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesnodeIdnetworkinterfacesinterfaceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getFabricnodesnodeIdnetworkinterfacesinterfaceId(null, null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdnetworkinterfacesinterfaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceid', (done) => {
        try {
          a.getFabricnodesnodeIdnetworkinterfacesinterfaceId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdnetworkinterfacesinterfaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricdiscoveredNodesnodeExtId - errors', () => {
      it('should have a getFabricdiscoveredNodesnodeExtId function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricdiscoveredNodesnodeExtId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeextid', (done) => {
        try {
          a.getFabricdiscoveredNodesnodeExtId(null, (data, error) => {
            try {
              const displayE = 'nodeextid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricdiscoveredNodesnodeExtId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricdiscoveredNodes - errors', () => {
      it('should have a getFabricdiscoveredNodes function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricdiscoveredNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabriccomputeCollectionsccExtId - errors', () => {
      it('should have a getFabriccomputeCollectionsccExtId function', (done) => {
        try {
          assert.equal(true, typeof a.getFabriccomputeCollectionsccExtId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ccextid', (done) => {
        try {
          a.getFabriccomputeCollectionsccExtId(null, (data, error) => {
            try {
              const displayE = 'ccextid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabriccomputeCollectionsccExtId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricvirtualMachines - errors', () => {
      it('should have a getFabricvirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricvirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesnodeIdnetworkinterfaces - errors', () => {
      it('should have a getFabricnodesnodeIdnetworkinterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesnodeIdnetworkinterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getFabricnodesnodeIdnetworkinterfaces(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdnetworkinterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFabricnodesnodeIdactionupgradeInfra - errors', () => {
      it('should have a postFabricnodesnodeIdactionupgradeInfra function', (done) => {
        try {
          assert.equal(true, typeof a.postFabricnodesnodeIdactionupgradeInfra === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postFabricnodesnodeIdactionupgradeInfra(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFabricnodesnodeIdactionupgradeInfra', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFabriccomputeManagers - errors', () => {
      it('should have a postFabriccomputeManagers function', (done) => {
        try {
          assert.equal(true, typeof a.postFabriccomputeManagers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ComputeManager', (done) => {
        try {
          a.postFabriccomputeManagers(null, (data, error) => {
            try {
              const displayE = 'ComputeManager is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFabriccomputeManagers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabriccomputeManagers - errors', () => {
      it('should have a getFabriccomputeManagers function', (done) => {
        try {
          assert.equal(true, typeof a.getFabriccomputeManagers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesnodeIdstate - errors', () => {
      it('should have a getFabricnodesnodeIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesnodeIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getFabricnodesnodeIdstate(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFabricdiscoveredNodesnodeExtIdactionhostprep - errors', () => {
      it('should have a postFabricdiscoveredNodesnodeExtIdactionhostprep function', (done) => {
        try {
          assert.equal(true, typeof a.postFabricdiscoveredNodesnodeExtIdactionhostprep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeextid', (done) => {
        try {
          a.postFabricdiscoveredNodesnodeExtIdactionhostprep(null, (data, error) => {
            try {
              const displayE = 'nodeextid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFabricdiscoveredNodesnodeExtIdactionhostprep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabriccomputeManagerscomputeManagerIdstate - errors', () => {
      it('should have a getFabriccomputeManagerscomputeManagerIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getFabriccomputeManagerscomputeManagerIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing computemanagerid', (done) => {
        try {
          a.getFabriccomputeManagerscomputeManagerIdstate(null, (data, error) => {
            try {
              const displayE = 'computemanagerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabriccomputeManagerscomputeManagerIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesnodeIdcapabilities - errors', () => {
      it('should have a getFabricnodesnodeIdcapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesnodeIdcapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getFabricnodesnodeIdcapabilities(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdcapabilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFabricnodes - errors', () => {
      it('should have a postFabricnodes function', (done) => {
        try {
          assert.equal(true, typeof a.postFabricnodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Node', (done) => {
        try {
          a.postFabricnodes(null, (data, error) => {
            try {
              const displayE = 'Node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFabricnodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodes - errors', () => {
      it('should have a getFabricnodes function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesnodeIdstatus - errors', () => {
      it('should have a getFabricnodesnodeIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesnodeIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getFabricnodesnodeIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putFabriccomputeCollectionFabricTemplatesfabricTemplateId - errors', () => {
      it('should have a putFabriccomputeCollectionFabricTemplatesfabricTemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.putFabriccomputeCollectionFabricTemplatesfabricTemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabrictemplateid', (done) => {
        try {
          a.putFabriccomputeCollectionFabricTemplatesfabricTemplateId(null, null, (data, error) => {
            try {
              const displayE = 'fabrictemplateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFabriccomputeCollectionFabricTemplatesfabricTemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ComputeCollectionFabricTemplate', (done) => {
        try {
          a.putFabriccomputeCollectionFabricTemplatesfabricTemplateId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ComputeCollectionFabricTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFabriccomputeCollectionFabricTemplatesfabricTemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabriccomputeCollectionFabricTemplatesfabricTemplateId - errors', () => {
      it('should have a getFabriccomputeCollectionFabricTemplatesfabricTemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.getFabriccomputeCollectionFabricTemplatesfabricTemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabrictemplateid', (done) => {
        try {
          a.getFabriccomputeCollectionFabricTemplatesfabricTemplateId(null, (data, error) => {
            try {
              const displayE = 'fabrictemplateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabriccomputeCollectionFabricTemplatesfabricTemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFabriccomputeCollectionFabricTemplatesfabricTemplateId - errors', () => {
      it('should have a deleteFabriccomputeCollectionFabricTemplatesfabricTemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFabriccomputeCollectionFabricTemplatesfabricTemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fabrictemplateid', (done) => {
        try {
          a.deleteFabriccomputeCollectionFabricTemplatesfabricTemplateId(null, (data, error) => {
            try {
              const displayE = 'fabrictemplateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteFabriccomputeCollectionFabricTemplatesfabricTemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putFabriccomputeManagerscomputeManagerId - errors', () => {
      it('should have a putFabriccomputeManagerscomputeManagerId function', (done) => {
        try {
          assert.equal(true, typeof a.putFabriccomputeManagerscomputeManagerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing computemanagerid', (done) => {
        try {
          a.putFabriccomputeManagerscomputeManagerId(null, null, (data, error) => {
            try {
              const displayE = 'computemanagerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFabriccomputeManagerscomputeManagerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ComputeManager', (done) => {
        try {
          a.putFabriccomputeManagerscomputeManagerId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ComputeManager is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFabriccomputeManagerscomputeManagerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFabriccomputeManagerscomputeManagerId - errors', () => {
      it('should have a deleteFabriccomputeManagerscomputeManagerId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFabriccomputeManagerscomputeManagerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing computemanagerid', (done) => {
        try {
          a.deleteFabriccomputeManagerscomputeManagerId(null, (data, error) => {
            try {
              const displayE = 'computemanagerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteFabriccomputeManagerscomputeManagerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabriccomputeManagerscomputeManagerId - errors', () => {
      it('should have a getFabriccomputeManagerscomputeManagerId function', (done) => {
        try {
          assert.equal(true, typeof a.getFabriccomputeManagerscomputeManagerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing computemanagerid', (done) => {
        try {
          a.getFabriccomputeManagerscomputeManagerId(null, (data, error) => {
            try {
              const displayE = 'computemanagerid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabriccomputeManagerscomputeManagerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putFabricnodesnodeId - errors', () => {
      it('should have a putFabricnodesnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.putFabricnodesnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.putFabricnodesnodeId(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFabricnodesnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing Node', (done) => {
        try {
          a.putFabricnodesnodeId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'Node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putFabricnodesnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFabricnodesnodeId - errors', () => {
      it('should have a postFabricnodesnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.postFabricnodesnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.postFabricnodesnodeId(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postFabricnodesnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesnodeId - errors', () => {
      it('should have a getFabricnodesnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getFabricnodesnodeId(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFabricnodesnodeId - errors', () => {
      it('should have a deleteFabricnodesnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFabricnodesnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.deleteFabricnodesnodeId(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteFabricnodesnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFabricnodesnodeIdnetworkinterfacesinterfaceIdstats - errors', () => {
      it('should have a getFabricnodesnodeIdnetworkinterfacesinterfaceIdstats function', (done) => {
        try {
          assert.equal(true, typeof a.getFabricnodesnodeIdnetworkinterfacesinterfaceIdstats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getFabricnodesnodeIdnetworkinterfacesinterfaceIdstats(null, null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdnetworkinterfacesinterfaceIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceid', (done) => {
        try {
          a.getFabricnodesnodeIdnetworkinterfacesinterfaceIdstats('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getFabricnodesnodeIdnetworkinterfacesinterfaceIdstats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTransportNodestransportnodeId - errors', () => {
      it('should have a putTransportNodestransportnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.putTransportNodestransportnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportnodeid', (done) => {
        try {
          a.putTransportNodestransportnodeId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'transportnodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putTransportNodestransportnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransportNode', (done) => {
        try {
          a.putTransportNodestransportnodeId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'TransportNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putTransportNodestransportnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransportNodestransportnodeId - errors', () => {
      it('should have a postTransportNodestransportnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.postTransportNodestransportnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportnodeid', (done) => {
        try {
          a.postTransportNodestransportnodeId(null, null, (data, error) => {
            try {
              const displayE = 'transportnodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTransportNodestransportnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportNodestransportnodeId - errors', () => {
      it('should have a deleteTransportNodestransportnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransportNodestransportnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportnodeid', (done) => {
        try {
          a.deleteTransportNodestransportnodeId(null, (data, error) => {
            try {
              const displayE = 'transportnodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTransportNodestransportnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodestransportnodeId - errors', () => {
      it('should have a getTransportNodestransportnodeId function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodestransportnodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportnodeid', (done) => {
        try {
          a.getTransportNodestransportnodeId(null, (data, error) => {
            try {
              const displayE = 'transportnodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodestransportnodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postComputeCollectionTransportNodeTemplates - errors', () => {
      it('should have a postComputeCollectionTransportNodeTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postComputeCollectionTransportNodeTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ComputeCollectionTransportNodeTemplate', (done) => {
        try {
          a.postComputeCollectionTransportNodeTemplates(null, (data, error) => {
            try {
              const displayE = 'ComputeCollectionTransportNodeTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postComputeCollectionTransportNodeTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComputeCollectionTransportNodeTemplates - errors', () => {
      it('should have a getComputeCollectionTransportNodeTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getComputeCollectionTransportNodeTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postBridgeEndpoints - errors', () => {
      it('should have a postBridgeEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.postBridgeEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BridgeEndpoint', (done) => {
        try {
          a.postBridgeEndpoints(null, (data, error) => {
            try {
              const displayE = 'BridgeEndpoint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postBridgeEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeEndpoints - errors', () => {
      it('should have a getBridgeEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZoneszoneIdsummary - errors', () => {
      it('should have a getTransportZoneszoneIdsummary function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZoneszoneIdsummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.getTransportZoneszoneIdsummary(null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportZoneszoneIdsummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putEdgeClustersedgeClusterId - errors', () => {
      it('should have a putEdgeClustersedgeClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.putEdgeClustersedgeClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeclusterid', (done) => {
        try {
          a.putEdgeClustersedgeClusterId(null, null, (data, error) => {
            try {
              const displayE = 'edgeclusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putEdgeClustersedgeClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing EdgeCluster', (done) => {
        try {
          a.putEdgeClustersedgeClusterId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'EdgeCluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putEdgeClustersedgeClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEdgeClustersedgeClusterId - errors', () => {
      it('should have a deleteEdgeClustersedgeClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEdgeClustersedgeClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeclusterid', (done) => {
        try {
          a.deleteEdgeClustersedgeClusterId(null, (data, error) => {
            try {
              const displayE = 'edgeclusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteEdgeClustersedgeClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEdgeClustersedgeClusterId - errors', () => {
      it('should have a getEdgeClustersedgeClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.getEdgeClustersedgeClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeclusterid', (done) => {
        try {
          a.getEdgeClustersedgeClusterId(null, (data, error) => {
            try {
              const displayE = 'edgeclusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getEdgeClustersedgeClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putHostSwitchProfileshostSwitchProfileId - errors', () => {
      it('should have a putHostSwitchProfileshostSwitchProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putHostSwitchProfileshostSwitchProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostswitchprofileid', (done) => {
        try {
          a.putHostSwitchProfileshostSwitchProfileId(null, null, (data, error) => {
            try {
              const displayE = 'hostswitchprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putHostSwitchProfileshostSwitchProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BaseHostSwitchProfile', (done) => {
        try {
          a.putHostSwitchProfileshostSwitchProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'BaseHostSwitchProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putHostSwitchProfileshostSwitchProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostSwitchProfileshostSwitchProfileId - errors', () => {
      it('should have a getHostSwitchProfileshostSwitchProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getHostSwitchProfileshostSwitchProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostswitchprofileid', (done) => {
        try {
          a.getHostSwitchProfileshostSwitchProfileId(null, (data, error) => {
            try {
              const displayE = 'hostswitchprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getHostSwitchProfileshostSwitchProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostSwitchProfileshostSwitchProfileId - errors', () => {
      it('should have a deleteHostSwitchProfileshostSwitchProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHostSwitchProfileshostSwitchProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostswitchprofileid', (done) => {
        try {
          a.deleteHostSwitchProfileshostSwitchProfileId(null, (data, error) => {
            try {
              const displayE = 'hostswitchprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteHostSwitchProfileshostSwitchProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeEndpointsendpointIdstatus - errors', () => {
      it('should have a getBridgeEndpointsendpointIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeEndpointsendpointIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointid', (done) => {
        try {
          a.getBridgeEndpointsendpointIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'endpointid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getBridgeEndpointsendpointIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putBridgeEndpointsbridgeendpointId - errors', () => {
      it('should have a putBridgeEndpointsbridgeendpointId function', (done) => {
        try {
          assert.equal(true, typeof a.putBridgeEndpointsbridgeendpointId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bridgeendpointid', (done) => {
        try {
          a.putBridgeEndpointsbridgeendpointId(null, null, (data, error) => {
            try {
              const displayE = 'bridgeendpointid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putBridgeEndpointsbridgeendpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BridgeEndpoint', (done) => {
        try {
          a.putBridgeEndpointsbridgeendpointId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'BridgeEndpoint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putBridgeEndpointsbridgeendpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeEndpointsbridgeendpointId - errors', () => {
      it('should have a getBridgeEndpointsbridgeendpointId function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeEndpointsbridgeendpointId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bridgeendpointid', (done) => {
        try {
          a.getBridgeEndpointsbridgeendpointId(null, (data, error) => {
            try {
              const displayE = 'bridgeendpointid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getBridgeEndpointsbridgeendpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBridgeEndpointsbridgeendpointId - errors', () => {
      it('should have a deleteBridgeEndpointsbridgeendpointId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBridgeEndpointsbridgeendpointId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bridgeendpointid', (done) => {
        try {
          a.deleteBridgeEndpointsbridgeendpointId(null, (data, error) => {
            try {
              const displayE = 'bridgeendpointid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteBridgeEndpointsbridgeendpointId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgeClusters - errors', () => {
      it('should have a postEdgeClusters function', (done) => {
        try {
          assert.equal(true, typeof a.postEdgeClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing EdgeCluster', (done) => {
        try {
          a.postEdgeClusters(null, (data, error) => {
            try {
              const displayE = 'EdgeCluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postEdgeClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEdgeClusters - errors', () => {
      it('should have a getEdgeClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getEdgeClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransportNodestransportnodeIdactionresyncHostConfig - errors', () => {
      it('should have a postTransportNodestransportnodeIdactionresyncHostConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postTransportNodestransportnodeIdactionresyncHostConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportnodeid', (done) => {
        try {
          a.postTransportNodestransportnodeIdactionresyncHostConfig(null, (data, error) => {
            try {
              const displayE = 'transportnodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTransportNodestransportnodeIdactionresyncHostConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postBridgeClusters - errors', () => {
      it('should have a postBridgeClusters function', (done) => {
        try {
          assert.equal(true, typeof a.postBridgeClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BridgeCluster', (done) => {
        try {
          a.postBridgeClusters(null, (data, error) => {
            try {
              const displayE = 'BridgeCluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postBridgeClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeClusters - errors', () => {
      it('should have a getBridgeClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putClusterProfilesclusterProfileId - errors', () => {
      it('should have a putClusterProfilesclusterProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putClusterProfilesclusterProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterprofileid', (done) => {
        try {
          a.putClusterProfilesclusterProfileId(null, null, (data, error) => {
            try {
              const displayE = 'clusterprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putClusterProfilesclusterProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ClusterProfile', (done) => {
        try {
          a.putClusterProfilesclusterProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ClusterProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putClusterProfilesclusterProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClusterProfilesclusterProfileId - errors', () => {
      it('should have a deleteClusterProfilesclusterProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClusterProfilesclusterProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterprofileid', (done) => {
        try {
          a.deleteClusterProfilesclusterProfileId(null, (data, error) => {
            try {
              const displayE = 'clusterprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteClusterProfilesclusterProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterProfilesclusterProfileId - errors', () => {
      it('should have a getClusterProfilesclusterProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterProfilesclusterProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterprofileid', (done) => {
        try {
          a.getClusterProfilesclusterProfileId(null, (data, error) => {
            try {
              const displayE = 'clusterprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getClusterProfilesclusterProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLldptransportNodesnodeIdinterfacesinterfaceName - errors', () => {
      it('should have a getLldptransportNodesnodeIdinterfacesinterfaceName function', (done) => {
        try {
          assert.equal(true, typeof a.getLldptransportNodesnodeIdinterfacesinterfaceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getLldptransportNodesnodeIdinterfacesinterfaceName(null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLldptransportNodesnodeIdinterfacesinterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfacename', (done) => {
        try {
          a.getLldptransportNodesnodeIdinterfacesinterfaceName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfacename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLldptransportNodesnodeIdinterfacesinterfaceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgeClustersedgeClusterIdactionreplaceTransportNode - errors', () => {
      it('should have a postEdgeClustersedgeClusterIdactionreplaceTransportNode function', (done) => {
        try {
          assert.equal(true, typeof a.postEdgeClustersedgeClusterIdactionreplaceTransportNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeclusterid', (done) => {
        try {
          a.postEdgeClustersedgeClusterIdactionreplaceTransportNode(null, null, (data, error) => {
            try {
              const displayE = 'edgeclusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postEdgeClustersedgeClusterIdactionreplaceTransportNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing EdgeClusterMemberTransportNode', (done) => {
        try {
          a.postEdgeClustersedgeClusterIdactionreplaceTransportNode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'EdgeClusterMemberTransportNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postEdgeClustersedgeClusterIdactionreplaceTransportNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeClustersclusterIdstatus - errors', () => {
      it('should have a getBridgeClustersclusterIdstatus function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeClustersclusterIdstatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterid', (done) => {
        try {
          a.getBridgeClustersclusterIdstatus(null, null, (data, error) => {
            try {
              const displayE = 'clusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getBridgeClustersclusterIdstatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComputeCollectionTransportNodeTemplatestemplateIdstate - errors', () => {
      it('should have a getComputeCollectionTransportNodeTemplatestemplateIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getComputeCollectionTransportNodeTemplatestemplateIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.getComputeCollectionTransportNodeTemplatestemplateIdstate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getComputeCollectionTransportNodeTemplatestemplateIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postHostSwitchProfiles - errors', () => {
      it('should have a postHostSwitchProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postHostSwitchProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BaseHostSwitchProfile', (done) => {
        try {
          a.postHostSwitchProfiles(null, (data, error) => {
            try {
              const displayE = 'BaseHostSwitchProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postHostSwitchProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostSwitchProfiles - errors', () => {
      it('should have a getHostSwitchProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getHostSwitchProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransportZones - errors', () => {
      it('should have a postTransportZones function', (done) => {
        try {
          assert.equal(true, typeof a.postTransportZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransportZone', (done) => {
        try {
          a.postTransportZones(null, (data, error) => {
            try {
              const displayE = 'TransportZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTransportZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZones - errors', () => {
      it('should have a getTransportZones function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTransportZoneszoneId - errors', () => {
      it('should have a putTransportZoneszoneId function', (done) => {
        try {
          assert.equal(true, typeof a.putTransportZoneszoneId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.putTransportZoneszoneId(null, null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putTransportZoneszoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransportZone', (done) => {
        try {
          a.putTransportZoneszoneId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'TransportZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putTransportZoneszoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportZoneszoneId - errors', () => {
      it('should have a getTransportZoneszoneId function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportZoneszoneId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.getTransportZoneszoneId(null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportZoneszoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportZoneszoneId - errors', () => {
      it('should have a deleteTransportZoneszoneId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransportZoneszoneId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneid', (done) => {
        try {
          a.deleteTransportZoneszoneId(null, (data, error) => {
            try {
              const displayE = 'zoneid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTransportZoneszoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodestransportnodeIdstate - errors', () => {
      it('should have a getTransportNodestransportnodeIdstate function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodestransportnodeIdstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportnodeid', (done) => {
        try {
          a.getTransportNodestransportnodeIdstate(null, (data, error) => {
            try {
              const displayE = 'transportnodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodestransportnodeIdstate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransportNodes - errors', () => {
      it('should have a postTransportNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postTransportNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransportNode', (done) => {
        try {
          a.postTransportNodes(null, (data, error) => {
            try {
              const displayE = 'TransportNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTransportNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodes - errors', () => {
      it('should have a getTransportNodes function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClusterProfiles - errors', () => {
      it('should have a postClusterProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postClusterProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ClusterProfile', (done) => {
        try {
          a.postClusterProfiles(null, (data, error) => {
            try {
              const displayE = 'ClusterProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postClusterProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterProfiles - errors', () => {
      it('should have a getClusterProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getClusterProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLldptransportNodesnodeIdinterfaces - errors', () => {
      it('should have a getLldptransportNodesnodeIdinterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getLldptransportNodesnodeIdinterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getLldptransportNodesnodeIdinterfaces(null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getLldptransportNodesnodeIdinterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putBridgeClustersbridgeclusterId - errors', () => {
      it('should have a putBridgeClustersbridgeclusterId function', (done) => {
        try {
          assert.equal(true, typeof a.putBridgeClustersbridgeclusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bridgeclusterid', (done) => {
        try {
          a.putBridgeClustersbridgeclusterId(null, null, (data, error) => {
            try {
              const displayE = 'bridgeclusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putBridgeClustersbridgeclusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BridgeCluster', (done) => {
        try {
          a.putBridgeClustersbridgeclusterId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'BridgeCluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putBridgeClustersbridgeclusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBridgeClustersbridgeclusterId - errors', () => {
      it('should have a deleteBridgeClustersbridgeclusterId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBridgeClustersbridgeclusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bridgeclusterid', (done) => {
        try {
          a.deleteBridgeClustersbridgeclusterId(null, (data, error) => {
            try {
              const displayE = 'bridgeclusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteBridgeClustersbridgeclusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeClustersbridgeclusterId - errors', () => {
      it('should have a getBridgeClustersbridgeclusterId function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeClustersbridgeclusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bridgeclusterid', (done) => {
        try {
          a.getBridgeClustersbridgeclusterId(null, (data, error) => {
            try {
              const displayE = 'bridgeclusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getBridgeClustersbridgeclusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putComputeCollectionTransportNodeTemplatestemplateId - errors', () => {
      it('should have a putComputeCollectionTransportNodeTemplatestemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.putComputeCollectionTransportNodeTemplatestemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.putComputeCollectionTransportNodeTemplatestemplateId(null, null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putComputeCollectionTransportNodeTemplatestemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ComputeCollectionTransportNodeTemplate', (done) => {
        try {
          a.putComputeCollectionTransportNodeTemplatestemplateId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ComputeCollectionTransportNodeTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putComputeCollectionTransportNodeTemplatestemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComputeCollectionTransportNodeTemplatestemplateId - errors', () => {
      it('should have a getComputeCollectionTransportNodeTemplatestemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.getComputeCollectionTransportNodeTemplatestemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.getComputeCollectionTransportNodeTemplatestemplateId(null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getComputeCollectionTransportNodeTemplatestemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteComputeCollectionTransportNodeTemplatestemplateId - errors', () => {
      it('should have a deleteComputeCollectionTransportNodeTemplatestemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteComputeCollectionTransportNodeTemplatestemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.deleteComputeCollectionTransportNodeTemplatestemplateId(null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteComputeCollectionTransportNodeTemplatestemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBridgeEndpointsendpointIdstatistics - errors', () => {
      it('should have a getBridgeEndpointsendpointIdstatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getBridgeEndpointsendpointIdstatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointid', (done) => {
        try {
          a.getBridgeEndpointsendpointIdstatistics(null, null, (data, error) => {
            try {
              const displayE = 'endpointid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getBridgeEndpointsendpointIdstatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTransportzoneProfilestransportzoneProfileId - errors', () => {
      it('should have a putTransportzoneProfilestransportzoneProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.putTransportzoneProfilestransportzoneProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportzoneprofileid', (done) => {
        try {
          a.putTransportzoneProfilestransportzoneProfileId(null, null, (data, error) => {
            try {
              const displayE = 'transportzoneprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putTransportzoneProfilestransportzoneProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransportZoneProfile', (done) => {
        try {
          a.putTransportzoneProfilestransportzoneProfileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'TransportZoneProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putTransportzoneProfilestransportzoneProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportzoneProfilestransportzoneProfileId - errors', () => {
      it('should have a deleteTransportzoneProfilestransportzoneProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransportzoneProfilestransportzoneProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportzoneprofileid', (done) => {
        try {
          a.deleteTransportzoneProfilestransportzoneProfileId(null, (data, error) => {
            try {
              const displayE = 'transportzoneprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteTransportzoneProfilestransportzoneProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportzoneProfilestransportzoneProfileId - errors', () => {
      it('should have a getTransportzoneProfilestransportzoneProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportzoneProfilestransportzoneProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportzoneprofileid', (done) => {
        try {
          a.getTransportzoneProfilestransportzoneProfileId(null, (data, error) => {
            try {
              const displayE = 'transportzoneprofileid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportzoneProfilestransportzoneProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodesstate - errors', () => {
      it('should have a getTransportNodesstate function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodesstate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransportzoneProfiles - errors', () => {
      it('should have a postTransportzoneProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.postTransportzoneProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransportZoneProfile', (done) => {
        try {
          a.postTransportzoneProfiles(null, (data, error) => {
            try {
              const displayE = 'TransportZoneProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postTransportzoneProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportzoneProfiles - errors', () => {
      it('should have a getTransportzoneProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportzoneProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpfixconfigsconfigId - errors', () => {
      it('should have a putIpfixconfigsconfigId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpfixconfigsconfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configid', (done) => {
        try {
          a.putIpfixconfigsconfigId(null, null, (data, error) => {
            try {
              const displayE = 'configid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putIpfixconfigsconfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpfixConfig', (done) => {
        try {
          a.putIpfixconfigsconfigId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'IpfixConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putIpfixconfigsconfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpfixconfigsconfigId - errors', () => {
      it('should have a deleteIpfixconfigsconfigId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpfixconfigsconfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configid', (done) => {
        try {
          a.deleteIpfixconfigsconfigId(null, (data, error) => {
            try {
              const displayE = 'configid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteIpfixconfigsconfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpfixconfigsconfigId - errors', () => {
      it('should have a getIpfixconfigsconfigId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpfixconfigsconfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configid', (done) => {
        try {
          a.getIpfixconfigsconfigId(null, (data, error) => {
            try {
              const displayE = 'configid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getIpfixconfigsconfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpfixconfigs - errors', () => {
      it('should have a postIpfixconfigs function', (done) => {
        try {
          assert.equal(true, typeof a.postIpfixconfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpfixConfig', (done) => {
        try {
          a.postIpfixconfigs(null, (data, error) => {
            try {
              const displayE = 'IpfixConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postIpfixconfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpfixconfigs - errors', () => {
      it('should have a getIpfixconfigs function', (done) => {
        try {
          assert.equal(true, typeof a.getIpfixconfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpfixcollectorconfigs - errors', () => {
      it('should have a postIpfixcollectorconfigs function', (done) => {
        try {
          assert.equal(true, typeof a.postIpfixcollectorconfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpfixCollectorConfig', (done) => {
        try {
          a.postIpfixcollectorconfigs(null, (data, error) => {
            try {
              const displayE = 'IpfixCollectorConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postIpfixcollectorconfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpfixcollectorconfigs - errors', () => {
      it('should have a getIpfixcollectorconfigs function', (done) => {
        try {
          assert.equal(true, typeof a.getIpfixcollectorconfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpfixcollectorconfigscollectorConfigId - errors', () => {
      it('should have a putIpfixcollectorconfigscollectorConfigId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpfixcollectorconfigscollectorConfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectorconfigid', (done) => {
        try {
          a.putIpfixcollectorconfigscollectorConfigId(null, null, (data, error) => {
            try {
              const displayE = 'collectorconfigid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putIpfixcollectorconfigscollectorConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing IpfixCollectorConfig', (done) => {
        try {
          a.putIpfixcollectorconfigscollectorConfigId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'IpfixCollectorConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putIpfixcollectorconfigscollectorConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpfixcollectorconfigscollectorConfigId - errors', () => {
      it('should have a deleteIpfixcollectorconfigscollectorConfigId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpfixcollectorconfigscollectorConfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectorconfigid', (done) => {
        try {
          a.deleteIpfixcollectorconfigscollectorConfigId(null, (data, error) => {
            try {
              const displayE = 'collectorconfigid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteIpfixcollectorconfigscollectorConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpfixcollectorconfigscollectorConfigId - errors', () => {
      it('should have a getIpfixcollectorconfigscollectorConfigId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpfixcollectorconfigscollectorConfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectorconfigid', (done) => {
        try {
          a.getIpfixcollectorconfigscollectorConfigId(null, (data, error) => {
            try {
              const displayE = 'collectorconfigid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getIpfixcollectorconfigscollectorConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNormalizations - errors', () => {
      it('should have a getNormalizations function', (done) => {
        try {
          assert.equal(true, typeof a.getNormalizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing preferredNormalizationType', (done) => {
        try {
          a.getNormalizations('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'preferredNormalizationType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNormalizations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.getNormalizations('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNormalizations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.getNormalizations('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getNormalizations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealizationStateBarriercurrent - errors', () => {
      it('should have a getRealizationStateBarriercurrent function', (done) => {
        try {
          assert.equal(true, typeof a.getRealizationStateBarriercurrent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRealizationStateBarriercurrentactionincrement - errors', () => {
      it('should have a postRealizationStateBarriercurrentactionincrement function', (done) => {
        try {
          assert.equal(true, typeof a.postRealizationStateBarriercurrentactionincrement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRealizationStateBarrierconfig - errors', () => {
      it('should have a putRealizationStateBarrierconfig function', (done) => {
        try {
          assert.equal(true, typeof a.putRealizationStateBarrierconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing RealizationStateBarrierConfig', (done) => {
        try {
          a.putRealizationStateBarrierconfig(null, (data, error) => {
            try {
              const displayE = 'RealizationStateBarrierConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putRealizationStateBarrierconfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRealizationStateBarrierconfig - errors', () => {
      it('should have a getRealizationStateBarrierconfig function', (done) => {
        try {
          assert.equal(true, typeof a.getRealizationStateBarrierconfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodesnodeIdtunnels - errors', () => {
      it('should have a getTransportNodesnodeIdtunnels function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodesnodeIdtunnels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getTransportNodesnodeIdtunnels(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodesnodeIdtunnels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportNodesnodeIdtunnelstunnelName - errors', () => {
      it('should have a getTransportNodesnodeIdtunnelstunnelName function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportNodesnodeIdtunnelstunnelName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeid', (done) => {
        try {
          a.getTransportNodesnodeIdtunnelstunnelName(null, null, null, (data, error) => {
            try {
              const displayE = 'nodeid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodesnodeIdtunnelstunnelName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelname', (done) => {
        try {
          a.getTransportNodesnodeIdtunnelstunnelName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tunnelname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTransportNodesnodeIdtunnelstunnelName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssociations - errors', () => {
      it('should have a getAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associatedResourceType', (done) => {
        try {
          a.getAssociations(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'associatedResourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.getAssociations('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.getAssociations('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUiViewsdashboardwidgetconfigurationswidgetconfigurationId - errors', () => {
      it('should have a putUiViewsdashboardwidgetconfigurationswidgetconfigurationId function', (done) => {
        try {
          assert.equal(true, typeof a.putUiViewsdashboardwidgetconfigurationswidgetconfigurationId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing widgetconfigurationid', (done) => {
        try {
          a.putUiViewsdashboardwidgetconfigurationswidgetconfigurationId(null, null, (data, error) => {
            try {
              const displayE = 'widgetconfigurationid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putUiViewsdashboardwidgetconfigurationswidgetconfigurationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing WidgetConfiguration', (done) => {
        try {
          a.putUiViewsdashboardwidgetconfigurationswidgetconfigurationId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'WidgetConfiguration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putUiViewsdashboardwidgetconfigurationswidgetconfigurationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUiViewsdashboardwidgetconfigurationswidgetconfigurationId - errors', () => {
      it('should have a getUiViewsdashboardwidgetconfigurationswidgetconfigurationId function', (done) => {
        try {
          assert.equal(true, typeof a.getUiViewsdashboardwidgetconfigurationswidgetconfigurationId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing widgetconfigurationid', (done) => {
        try {
          a.getUiViewsdashboardwidgetconfigurationswidgetconfigurationId(null, (data, error) => {
            try {
              const displayE = 'widgetconfigurationid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getUiViewsdashboardwidgetconfigurationswidgetconfigurationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUiViewsdashboardwidgetconfigurationswidgetconfigurationId - errors', () => {
      it('should have a deleteUiViewsdashboardwidgetconfigurationswidgetconfigurationId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUiViewsdashboardwidgetconfigurationswidgetconfigurationId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing widgetconfigurationid', (done) => {
        try {
          a.deleteUiViewsdashboardwidgetconfigurationswidgetconfigurationId(null, (data, error) => {
            try {
              const displayE = 'widgetconfigurationid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-deleteUiViewsdashboardwidgetconfigurationswidgetconfigurationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUiViewsdashboardwidgetconfigurations - errors', () => {
      it('should have a postUiViewsdashboardwidgetconfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.postUiViewsdashboardwidgetconfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing WidgetConfiguration', (done) => {
        try {
          a.postUiViewsdashboardwidgetconfigurations(null, (data, error) => {
            try {
              const displayE = 'WidgetConfiguration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postUiViewsdashboardwidgetconfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUiViewsdashboardwidgetconfigurations - errors', () => {
      it('should have a getUiViewsdashboardwidgetconfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.getUiViewsdashboardwidgetconfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getErrorResolvererrorId - errors', () => {
      it('should have a getErrorResolvererrorId function', (done) => {
        try {
          assert.equal(true, typeof a.getErrorResolvererrorId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing errorId', (done) => {
        try {
          a.getErrorResolvererrorId(null, (data, error) => {
            try {
              const displayE = 'errorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getErrorResolvererrorId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getErrorResolver - errors', () => {
      it('should have a getErrorResolver function', (done) => {
        try {
          assert.equal(true, typeof a.getErrorResolver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postErrorResolveractionresolveError - errors', () => {
      it('should have a postErrorResolveractionresolveError function', (done) => {
        try {
          assert.equal(true, typeof a.postErrorResolveractionresolveError === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ErrorResolverMetadataList', (done) => {
        try {
          a.postErrorResolveractionresolveError(null, (data, error) => {
            try {
              const displayE = 'ErrorResolverMetadataList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postErrorResolveractionresolveError', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNodeaaaauthPolicy - errors', () => {
      it('should have a putNodeaaaauthPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.putNodeaaaauthPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing AuthenticationPolicyProperties', (done) => {
        try {
          a.putNodeaaaauthPolicy(null, (data, error) => {
            try {
              const displayE = 'AuthenticationPolicyProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-putNodeaaaauthPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeaaaauthPolicy - errors', () => {
      it('should have a getNodeaaaauthPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeaaaauthPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskstaskId - errors', () => {
      it('should have a getTaskstaskId function', (done) => {
        try {
          assert.equal(true, typeof a.getTaskstaskId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskid', (done) => {
        try {
          a.getTaskstaskId(null, (data, error) => {
            try {
              const displayE = 'taskid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTaskstaskId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postBatch - errors', () => {
      it('should have a postBatch function', (done) => {
        try {
          assert.equal(true, typeof a.postBatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing BatchRequest', (done) => {
        try {
          a.postBatch(null, null, (data, error) => {
            try {
              const displayE = 'BatchRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-postBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasks - errors', () => {
      it('should have a getTasks function', (done) => {
        try {
          assert.equal(true, typeof a.getTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskstaskIdresponse - errors', () => {
      it('should have a getTaskstaskIdresponse function', (done) => {
        try {
          assert.equal(true, typeof a.getTaskstaskIdresponse === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskid', (done) => {
        try {
          a.getTaskstaskIdresponse(null, (data, error) => {
            try {
              const displayE = 'taskid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_nsx_t-adapter-getTaskstaskIdresponse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
